
EATOP Git Repository rules:

Git repository for the final, consolidated EATOP code is : https://code.google.com/a/eclipselabs.org/p/eclipse-auto-iwg.eatop


Contributions by single companies/institutes are collected in separate Git repositories following the naming convention


https://code.google.com/a/eclipselabs.org/p/eclipse-auto-iwg.eatop-<ContributorShortName>/


E.g. https://code.google.com/a/eclipselabs.org/p/eclipse-auto-iwg.eatop-conti


Existing Git repositories:

Main repository: https://code.google.com/a/eclipselabs.org/p/eclipse-auto-iwg.eatop
Continental repository: https://code.google.com/a/eclipselabs.org/p/eclipse-auto-iwg.eatop-conti