/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */
package eaadapter.provider;


import eaadapter.EAConnector;
import eaadapter.EaadapterFactory;
import eaadapter.EaadapterPackage;

import eaadapter.abstracthierachy.AbstracthierachyPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link eaadapter.EAConnector} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EAConnectorItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "<copyright>\r\nCopyright (c) Continental AG and others.\r\nAll rights reserved. This program and the accompanying materials are made navailable under the terms of the Eclipse Public License \r\nwhich accompanies this distribution, and is navailable at http://www.eclipse.org/org/documents/epl-v10.php\r\n\r\nContributors:\r\n\tContinental AG, 2012 Matthias Nick - Initial API and implementation\r\n</copyright>";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAConnectorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addNotesPropertyDescriptor(object);
			addGuidPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addStereotypePropertyDescriptor(object);
			addStereotypeExPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addConnectorIDPropertyDescriptor(object);
			addDirectionPropertyDescriptor(object);
			addSubtypePropertyDescriptor(object);
			addIsLeafPropertyDescriptor(object);
			addIsRootPropertyDescriptor(object);
			addIsSpecPropertyDescriptor(object);
			addVirtualInheritancePropertyDescriptor(object);
			addTransitionEventPropertyDescriptor(object);
			addTransitionActionPropertyDescriptor(object);
			addTransitionGuardPropertyDescriptor(object);
			addRouteStylePropertyDescriptor(object);
			addSequenceNoPropertyDescriptor(object);
			addClientPropertyDescriptor(object);
			addSupplierPropertyDescriptor(object);
			addClientIDPropertyDescriptor(object);
			addSupplierIDPropertyDescriptor(object);
			addEventFlagsPropertyDescriptor(object);
			addStyleExPropertyDescriptor(object);
			addDerivedPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EANamedElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EANamedElement_name_feature", "_UI_EANamedElement_type"),
				 AbstracthierachyPackage.Literals.EA_NAMED_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Notes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNotesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EANamedElement_notes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EANamedElement_notes_feature", "_UI_EANamedElement_type"),
				 AbstracthierachyPackage.Literals.EA_NAMED_ELEMENT__NOTES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Guid feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGuidPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EANamedElement_guid_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EANamedElement_guid_feature", "_UI_EANamedElement_type"),
				 AbstracthierachyPackage.Literals.EA_NAMED_ELEMENT__GUID,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EANamedElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EANamedElement_id_feature", "_UI_EANamedElement_type"),
				 AbstracthierachyPackage.Literals.EA_NAMED_ELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stereotype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStereotypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAStereotypedElement_stereotype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAStereotypedElement_stereotype_feature", "_UI_EAStereotypedElement_type"),
				 AbstracthierachyPackage.Literals.EA_STEREOTYPED_ELEMENT__STEREOTYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stereotype Ex feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStereotypeExPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAStereotypedElement_stereotypeEx_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAStereotypedElement_stereotypeEx_feature", "_UI_EAStereotypedElement_type"),
				 AbstracthierachyPackage.Literals.EA_STEREOTYPED_ELEMENT__STEREOTYPE_EX,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EATypedElement_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EATypedElement_type_feature", "_UI_EATypedElement_type"),
				 AbstracthierachyPackage.Literals.EA_TYPED_ELEMENT__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Connector ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConnectorIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_connectorID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_connectorID_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__CONNECTOR_ID,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Direction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_direction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_direction_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__DIRECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Subtype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSubtypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_subtype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_subtype_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__SUBTYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Leaf feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsLeafPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_isLeaf_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_isLeaf_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__IS_LEAF,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Root feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsRootPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_isRoot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_isRoot_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__IS_ROOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Spec feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsSpecPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_isSpec_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_isSpec_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__IS_SPEC,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Virtual Inheritance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVirtualInheritancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_virtualInheritance_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_virtualInheritance_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__VIRTUAL_INHERITANCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transition Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransitionEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_transitionEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_transitionEvent_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__TRANSITION_EVENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transition Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransitionActionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_transitionAction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_transitionAction_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__TRANSITION_ACTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transition Guard feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransitionGuardPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_transitionGuard_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_transitionGuard_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__TRANSITION_GUARD,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Route Style feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRouteStylePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_routeStyle_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_routeStyle_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__ROUTE_STYLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sequence No feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSequenceNoPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_sequenceNo_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_sequenceNo_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__SEQUENCE_NO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Client feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClientPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_client_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_client_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__CLIENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Supplier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSupplierPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_supplier_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_supplier_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__SUPPLIER,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Client ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClientIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_clientID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_clientID_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__CLIENT_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Supplier ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSupplierIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_supplierID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_supplierID_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__SUPPLIER_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Event Flags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventFlagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_eventFlags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_eventFlags_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__EVENT_FLAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Style Ex feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStyleExPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_styleEx_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_styleEx_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__STYLE_EX,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Derived feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EAConnector_derived_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EAConnector_derived_feature", "_UI_EAConnector_type"),
				 EaadapterPackage.Literals.EA_CONNECTOR__DERIVED,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(EaadapterPackage.Literals.EA_CONNECTOR__TAGGED_VALUES);
			childrenFeatures.add(EaadapterPackage.Literals.EA_CONNECTOR__CONSTRAINTS);
			childrenFeatures.add(EaadapterPackage.Literals.EA_CONNECTOR__CLIENT_END);
			childrenFeatures.add(EaadapterPackage.Literals.EA_CONNECTOR__SUPPLIER_END);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EAConnector.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	@Override
	public Object getImage(Object object) {
		EAConnector element = (EAConnector) object;
		
		Object defaultImage = overlayImage(object, getResourceLocator().getImage("full/obj16/EAConnector"));
		Object myImage = null;
		
		if (element.getType().equals("Association"))
			myImage = overlayImage(object, getResourceLocator().getImage("full/custom16/EAConnector"));
		else if (element.getType().equals("Generalization"))
			myImage = overlayImage(object, getResourceLocator().getImage("full/custom16/EAConnectorGeneralization"));
		else if (element.getType().equals("Aggregation"))
			myImage = overlayImage(object, getResourceLocator().getImage("full/custom16/EAConnectorAggregation"));
		else
			myImage = overlayImage(object, getResourceLocator().getImage("full/custom16/EAConnector"));			
			
		if (myImage != null)
			return myImage;
		return defaultImage;		
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	@Override
	public String getText(Object object) {
		EAConnector element = (EAConnector) object;
		String client = element.getClient().getName();
		if (element.getClientEnd().getRole().length() > 0)
			client += "." + element.getClientEnd().getRole();
		
		String target = element.getSupplier().getName();
		if (element.getSupplierEnd().getRole().length() > 0)
			target += "." + element.getSupplierEnd().getRole();
		
		String type = element.getType();
		
		String stereotype = "";
		if (element.getStereotype().length() > 0) {
			if (element.getStereotypeEx().length() > element.getStereotype().length())
				stereotype = "<<" + element.getStereotypeEx() + ">> ";
			else
				stereotype = "<<" + element.getStereotype() + ">> ";
			
		}
		
		String direction = createDirection(element);
			
		String derived = "";
		if (element.getClientEnd().isDerived() == true || element.getSupplierEnd().isDerived() == true)
			derived = " / ";
		
		return "(" + type + ") " + derived + stereotype + client + direction + target;
	}
	
	private String createDirection(EAConnector element) {
		String direction = "";
		if (element.getDirection() == null) {
			direction = "<null>";
		} else if (element.getDirection().equals("Unspecified")) {
			direction = "-unspecified-";
		} else if (element.getDirection().equals("Bi-Directional")) {
			direction = "<->";
		} else if (element.getDirection().equals("Source -> Destination")) {
			direction = "->";
		} else if (element.getDirection().equals("Destination -> Source")) {
			direction = "<-";
		} else {
			direction = "direction unknown value";
		}
		return " " + direction + " ";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EAConnector.class)) {
			case EaadapterPackage.EA_CONNECTOR__NAME:
			case EaadapterPackage.EA_CONNECTOR__NOTES:
			case EaadapterPackage.EA_CONNECTOR__GUID:
			case EaadapterPackage.EA_CONNECTOR__ID:
			case EaadapterPackage.EA_CONNECTOR__STEREOTYPE:
			case EaadapterPackage.EA_CONNECTOR__STEREOTYPE_EX:
			case EaadapterPackage.EA_CONNECTOR__TYPE:
			case EaadapterPackage.EA_CONNECTOR__CONNECTOR_ID:
			case EaadapterPackage.EA_CONNECTOR__DIRECTION:
			case EaadapterPackage.EA_CONNECTOR__SUBTYPE:
			case EaadapterPackage.EA_CONNECTOR__IS_LEAF:
			case EaadapterPackage.EA_CONNECTOR__IS_ROOT:
			case EaadapterPackage.EA_CONNECTOR__IS_SPEC:
			case EaadapterPackage.EA_CONNECTOR__VIRTUAL_INHERITANCE:
			case EaadapterPackage.EA_CONNECTOR__TRANSITION_EVENT:
			case EaadapterPackage.EA_CONNECTOR__TRANSITION_ACTION:
			case EaadapterPackage.EA_CONNECTOR__TRANSITION_GUARD:
			case EaadapterPackage.EA_CONNECTOR__ROUTE_STYLE:
			case EaadapterPackage.EA_CONNECTOR__SEQUENCE_NO:
			case EaadapterPackage.EA_CONNECTOR__EA_LINK:
			case EaadapterPackage.EA_CONNECTOR__CLIENT_ID:
			case EaadapterPackage.EA_CONNECTOR__SUPPLIER_ID:
			case EaadapterPackage.EA_CONNECTOR__EVENT_FLAGS:
			case EaadapterPackage.EA_CONNECTOR__STYLE_EX:
			case EaadapterPackage.EA_CONNECTOR__DERIVED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case EaadapterPackage.EA_CONNECTOR__TAGGED_VALUES:
			case EaadapterPackage.EA_CONNECTOR__CONSTRAINTS:
			case EaadapterPackage.EA_CONNECTOR__CLIENT_END:
			case EaadapterPackage.EA_CONNECTOR__SUPPLIER_END:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(EaadapterPackage.Literals.EA_CONNECTOR__TAGGED_VALUES,
				 EaadapterFactory.eINSTANCE.createEAConnectorTag()));

		newChildDescriptors.add
			(createChildParameter
				(EaadapterPackage.Literals.EA_CONNECTOR__CONSTRAINTS,
				 EaadapterFactory.eINSTANCE.createEAConnectorConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(EaadapterPackage.Literals.EA_CONNECTOR__CLIENT_END,
				 EaadapterFactory.eINSTANCE.createEAConnectorEnd()));

		newChildDescriptors.add
			(createChildParameter
				(EaadapterPackage.Literals.EA_CONNECTOR__SUPPLIER_END,
				 EaadapterFactory.eINSTANCE.createEAConnectorEnd()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == EaadapterPackage.Literals.EA_CONNECTOR__CLIENT_END ||
			childFeature == EaadapterPackage.Literals.EA_CONNECTOR__SUPPLIER_END;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EaEditPlugin.INSTANCE;
	}

}
