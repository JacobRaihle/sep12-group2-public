/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package eaadapter.ea2ecore;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EPackage;

import eaadapter.EARepository;


public class EA2Ecore {
	
	protected EARepository repository;
	protected EPackage model;
	private List<PostProcessingTemplate> postProcessings;
	private EcoreMapperTemplate ecoreBuilder;
	private Logger logger;
	private boolean enableLoggingMapper = false;
	private boolean enableLoggingPostProcessing = false;
		
	public EA2Ecore(EARepository r) {
		this.repository = r;
		postProcessings = new LinkedList<PostProcessingTemplate>();
		logger = new LoggerImpl();
	}

	public void generateEcore() throws Exception {
		ecoreBuilder = new EcoreMapperImpl(repository);
		ecoreBuilder.setLogger(logger);
		
		if (enableLoggingMapper)
			ecoreBuilder.setLoggingEnabled();
		else
			ecoreBuilder.setLoggingDisabled();
		
		model = ecoreBuilder.execute();
		
		
					
		for (PostProcessingTemplate p : postProcessings) {
			p.setModel(model);
			p.setRepository(repository);
			p.setLogger(logger);
			if (enableLoggingPostProcessing)
				p.setLoggingEnabled();
			else
				p.setLoggingDisabled();
			this.model = p.invoke();
		}
	}
	
	public EPackage getEcore() {
		return model;
	}

	public List<PostProcessingTemplate> getPostProcessings() {
		return postProcessings;
	}

	public void setPostProcessings(List<PostProcessingTemplate> postProcessings) {
		this.postProcessings = postProcessings;
	}

	public boolean isEnableLoggingMapper() {
		return enableLoggingMapper;
	}

	public void setEnableLoggingMapper(boolean enableLoggingMapper) {
		this.enableLoggingMapper = enableLoggingMapper;
	}

	public boolean isEnableLoggingPostProcessing() {
		return enableLoggingPostProcessing;
	}

	public void setEnableLoggingPostProcessing(boolean enableLoggingPostProcessing) {
		this.enableLoggingPostProcessing = enableLoggingPostProcessing;
	}
	
	
	
}


/*
 * Problems:
 * 
 * 1)
 * EASTADL - Infrastructure - Datatypes:
 * some Elements (EnumerationValueType) have attributes which reference EABoolean, EAString
 * is it okay just to use Boolean and String ?!
 * 
 * 4)
 * Problem stereotypes
 * EAElement hat "stereotypes" als String. Ich br�uchte aber die Stereotypes als ID
 * findStereotypeByString() ist ganz schlecht!
 */
