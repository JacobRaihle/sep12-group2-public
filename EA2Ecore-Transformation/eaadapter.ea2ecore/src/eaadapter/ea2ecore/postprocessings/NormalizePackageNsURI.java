/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package eaadapter.ea2ecore.postprocessings;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import eaadapter.ea2ecore.PostProcessingTemplate;


public class NormalizePackageNsURI extends PostProcessingTemplate {

	private static String prefix = "http://continental-corporation.com";
	
	@Override
	public void execute() {	
		TreeIterator<EObject> iter = model.eAllContents();
		while (iter.hasNext()) {
			EObject element = (EObject) iter.next();
			if (element instanceof EPackage) {
				createNsURIForEPackage((EPackage) element);
			}
		}
	}

	private void createNsURIForEPackage(EPackage element) {
		String packagePath = createPathOfEPackage(element);
		String nsUri = prefix + packagePath;
		element.setNsURI(nsUri);
	}
}
