package eaadapter.ea2ecore.postprocessings;
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import eaadapter.EAConnector;
import eaadapter.datatypes.T_ConnectorAggregation;
import eaadapter.ea2ecore.PostProcessingTemplate;


public class MakeReferencesNavigable extends PostProcessingTemplate {

	@Override
	public void execute() {
		EList<EReference> allReferences = getAllReferences();
		for (EReference reference : allReferences) {
			/**
			 * only create an opposite reference if there is none
			 */
			if (reference.getEOpposite() == null) {
				EAConnector eaConnector = getEAConnectorForEReference(reference);
				if (eaConnector != null && reference.isContainment() == false) {
					createOppositeReference(reference, eaConnector);
				} else if (eaConnector != null && reference.isContainment() == true) {
					createOppositeReferenceForContainment(reference, eaConnector);
				}
			}
		}
		
	}
	
	private void createOppositeReferenceForContainment(EReference ref, EAConnector eaConnector) {
		EClass client = ref.getEContainingClass();
		EClass supplier = ref.getEReferenceType();

		logger.info("Creating opposite association for containment reference from " + supplier.getName() + " to " + client.getName() + " with multiplicity [0..1]");
		
		EReference opposite = ecoreFactory.createEReference();
		String oppositeName;
		if (eaConnector.getClientEnd().getRole().length() > 0 && eaConnector.getClientEnd().getAggregation() == T_ConnectorAggregation.COMPOSITE)
			oppositeName = eaConnector.getClientEnd().getRole();
		else
			oppositeName = ref.getEContainingClass().getName() + "_" + ref.getName();
		opposite.setName(oppositeName);
		opposite.setDerived(eaConnector.isDerived());
		opposite.setLowerBound(0);
		opposite.setUpperBound(1);
		opposite.setEType(client);
		supplier.getEStructuralFeatures().add(opposite);
		
		opposite.setEOpposite(ref);
		ref.setEOpposite(opposite);			
	}

	private void createOppositeReference(EReference ref, EAConnector eaConnector) {
		EClass client = ref.getEContainingClass();
		EClass supplier = ref.getEReferenceType();

		logger.info("Creating opposite association from " + supplier.getName() + " to " + client.getName() + " with multiplicity [0..*]");
		
		EReference opposite = ecoreFactory.createEReference();
		String oppositeName;
		if (eaConnector.getClientEnd().getRole().length() > 0)
			oppositeName = eaConnector.getClientEnd().getRole();
		else
			oppositeName = eaConnector.getClient().getName() + "_" + eaConnector.getSupplierEnd().getRole();
		opposite.setName(oppositeName);
		opposite.setDerived(eaConnector.isDerived());
		opposite.setLowerBound(0);
		opposite.setUpperBound(-1);
		opposite.setEType(client);
		supplier.getEStructuralFeatures().add(opposite);
		
		opposite.setEOpposite(ref);
		ref.setEOpposite(opposite);		
	}

	private EList<EReference> getAllReferences() {
		EList<EReference> references = new BasicEList<EReference>();
		TreeIterator<EObject> iter = model.eAllContents();
		while (iter.hasNext()) {
			EObject element = iter.next();
			if (element instanceof EReference) {
				EReference ref = (EReference) element;
					references.add(ref);
			}
		}
		return references;
	}
}
