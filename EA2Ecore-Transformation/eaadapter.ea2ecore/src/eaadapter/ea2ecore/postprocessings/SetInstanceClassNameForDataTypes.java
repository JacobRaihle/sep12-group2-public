/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package eaadapter.ea2ecore.postprocessings;

import java.math.BigInteger;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.activation.MimeType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;

import eaadapter.ea2ecore.PostProcessingTemplate;


public class SetInstanceClassNameForDataTypes extends PostProcessingTemplate {

	private static Map<String, Class<?>> mapping;	
	static {
		mapping = new HashMap<String, Class<?>>();
		mapping.put("String", String.class);
		mapping.put("Integer", Integer.class);
		mapping.put("Float", Double.class);
		mapping.put("Numerical", String.class);
		mapping.put("UnlimitedInteger", BigInteger.class);
		mapping.put("UnlimitedNatural", BigInteger.class);
		mapping.put("PositiveInteger", Long.class);
		mapping.put("Boolean", Boolean.class);
		mapping.put("DateTime", XMLGregorianCalendar.class);
		mapping.put("NameToken", String.class);
		mapping.put("ExtIdClassEnum", String.class);
		mapping.put("RegularExpression", String.class);
		mapping.put("Identifier", String.class);
		mapping.put("Address", String.class);
		mapping.put("BaseTypeEncodingString", String.class);
		mapping.put("DisplayFormatString", String.class);
		mapping.put("MimeTypeString", MimeType.class);
		mapping.put("UriString", URI.class);
		mapping.put("IsString", String.class);
		mapping.put("NumericalValue", String.class);
		mapping.put("NativeDeclarationString", String.class);
		mapping.put("Ref", String.class);
		mapping.put("RevisionLabelString", String.class);
		mapping.put("VerbatimString", String.class);
		mapping.put("Superscript", String.class);
		mapping.put("MixedContentForPlainText", String.class);
		mapping.put("AxisIndexType", String.class);
		mapping.put("RecordLayoutIteratorPoint", String.class);
		mapping.put("DiagRequirementIdString", String.class);
		mapping.put("NameTokens", String.class);
		mapping.put("CIdentifier", String.class);
		mapping.put("CseCodeType", Integer.class);
		mapping.put("AsamRecordLayoutSemantics", String.class);
		mapping.put("TableSeparatorString", String.class);
		mapping.put("ViewTokens", String.class);
		mapping.put("AlignmentType", String.class);
		mapping.put("CategoryString", String.class);
		mapping.put("SectionInitializationPolicyType", String.class);
		mapping.put("ReferrableSubtypesEnum", String.class);
		mapping.put("TimeValue", Double.class);
	}	
	
	@Override
	public void execute() {

		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext()) {
			EObject element = iterator.next();
			
			if (element instanceof EEnum)
				continue;
			
			if (element instanceof EDataType) {
				EDataType dataType = (EDataType) element;
				String dataTypeName = dataType.getName();
				if (dataTypeName != null) {
					Class<?> javaClass = mapping.get(dataTypeName);
					if (javaClass != null) {
						dataType.setInstanceClassName(javaClass.getName());
						logger.info("Setting datatype " + dataType.getName() + "\t --> \t" + javaClass.getName() + " for " + ((ENamedElement)element).getName());
					} else {
						logger.error("No mapping for datatype " + dataTypeName + " available! The instanceClassName for " + ((ENamedElement)element).getName() + " could not set!");
					}
				}
			}
		}
	}
}
