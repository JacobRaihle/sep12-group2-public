package eaadapter.ea2ecore.postprocessings;
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

import org.eclipse.emf.ecore.EPackage;

import eaadapter.ea2ecore.PostProcessingTemplate;


public class DeletePackageGeneratedEcore extends PostProcessingTemplate {
	
	@Override
	public void execute() {
		EPackage generatedPackage = model;
		EPackage domainModel = model.getESubpackages().get(0);
				
		if (!generatedPackage.getName().equalsIgnoreCase("Generated Ecore")) {
			logger.error("could not remove 'Generated Ecore' package because it is not the root element.");
			return;
		}
		
		generatedPackage.setName(domainModel.getName());
		generatedPackage.setNsPrefix(domainModel.getNsPrefix());
		generatedPackage.setNsURI(domainModel.getNsURI());
		generatedPackage.getEAnnotations().addAll(domainModel.getEAnnotations());
		
		generatedPackage.getESubpackages().addAll(domainModel.getESubpackages());
		generatedPackage.getESubpackages().remove(domainModel);		
	}
}
