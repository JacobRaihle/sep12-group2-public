/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package eaadapter.ea2ecore.postprocessings;

import java.math.BigInteger;
import java.net.URI;

import javax.activation.MimeType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;

import eaadapter.ea2ecore.PostProcessingTemplate;

public class SetAttributesDefaultValue extends PostProcessingTemplate {

	private static final String BLANK = ""; 

	@Override
	public void execute() {
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext()) {
			EObject element = iterator.next();

			if (element instanceof EAttribute) {
				EAttribute attribute = (EAttribute) element;
				String instanceClassName = getInstanceClassNameForAttribute(attribute);
				if (instanceClassName != null) {
					if (String.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, BLANK);
					} else if (Boolean.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "false");
					} else if ("boolean".equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "false");
					} else if (Integer.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0"); 
					} else if ("int".equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0");										
					} else if (Float.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0.0");
					} else if ("float".equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0.0");
					} else if (Long.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0");	
					} else if ("long".equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0");											
					} else if (Double.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0.0");
					} else if ("double".equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0.0");										
					} else if (BigInteger.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0"); 
					} else if (XMLGregorianCalendar.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "0001-01-01T00:00:00"); 
					} else if (MimeType.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, "application/*");						
					} else if (URI.class.getName().equals(instanceClassName)) {
						setDefaultValueForAttribute(attribute, BLANK);
					} else if (EEnum.class.getName().equals(instanceClassName)) {
						EList<EEnumLiteral> literals = ((EEnum) attribute.getEType()).getELiterals();
						if (literals.size() > 0) {
							setDefaultValueForAttribute(attribute, literals.get(0).getLiteral());
						}
					}
				}
			}
		}		
	}
	
	private void setDefaultValueForAttribute(EAttribute attribute, String defaultValue) {
		attribute.setDefaultValueLiteral(defaultValue);
		logger.info("Setting defaultValue of " + attribute.getName() + " to '" + attribute.getDefaultValueLiteral() + "'");
	}

	private String getInstanceClassNameForAttribute(EAttribute attribute) {
		EClassifier classifer = attribute.getEType();
		if (classifer instanceof EEnum) {
			return EEnum.class.getName();
		} else if (classifer instanceof EDataType) {
			EDataType dataType = (EDataType) classifer;
			return dataType.getInstanceClassName();
		}
		return null;
	}
	
}
