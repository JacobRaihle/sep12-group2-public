/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package eaadapter.ea2ecore.postprocessings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EClass;


import eaadapter.ea2ecore.PostProcessingTemplate;

public class NameChanger extends PostProcessingTemplate {

	private static final  Map<CharSequence, CharSequence> characters = new HashMap<CharSequence, CharSequence>();
	static {
		characters.put(" ", "_");
		characters.put("-", "_");
		characters.put(".", "_dot_");
		characters.put(":", "_colon_");
		characters.put("/", "_slash_");
	}
	
	private int counter = 0;
	
	@Override
	public void execute() {			
		logger.info("NameChanger started!");
		EPackage rootElement = (EPackage) EcoreUtil.getRootContainer(model);
		List<ENamedElement> toBeDeletedElements = null;
		toBeDeletedElements = new ArrayList<ENamedElement>();
		
		if(!isNameWellFormed(rootElement)) {
			rootElement.setName(createWellFormedName(rootElement));
			rootElement.setNsPrefix(createWellFormedName(rootElement));
		}
		
		for (TreeIterator<EObject> iter = EcoreUtil.getAllProperContents(rootElement,true); iter.hasNext();) {
			EObject element = iter.next();
			if (element instanceof ENamedElement) {
				ENamedElement e = (ENamedElement) element;
				
				if (!isNameWellFormed(e)) {
					if (e instanceof EClass && !isNameSet(e)){
						// A class with no name may be a note, a border or something else that is not relevant
						toBeDeletedElements.add(e);
					} else {
						String newName = createWellFormedName(e);
						System.out.println("Warning: new name set from " + e.getName() + " --> " + newName);
						e.setName(newName);
						if (e instanceof EPackage) {
							((EPackage) e).setNsPrefix(newName);
						}
					}
				}
			}
		}

//	Normally no the list of toBeDeletedElements should be empty as these elements are 
		System.out.println("Number of useless classes that were transfered: " + toBeDeletedElements.size());
//		if (toBeDeletedElements != null) {
//			System.out.println("Warning: " + toBeDeletedElements.size() + " EClasses with no names are deleted");
//			Iterator it = toBeDeletedElements.iterator();
//			while (it.hasNext()) {
//				EClass element = (EClass) it.next();
//				EPackage parent = element.getEPackage();
//				parent.getEClassifiers().remove(element);
// to be implemented - delete EObject
				
//			}
//		}
	}
	
	private String createWellFormedName(ENamedElement element) {
		String name = element.getName();
		name = name.trim();
//		Either name is not set		
		if (!isNameSet(element)) {
			return "NameWasNotSet_" + counter++;
		}
//		Or characterset is not correct
		Iterator it = characters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			name = name.replace((CharSequence) entry.getKey(), (CharSequence) entry.getValue());
		}	
		return name;
	}
	

	private boolean isNameSet(ENamedElement element) {
		String name = element.getName();

		if (name.length() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	private boolean isNameWellFormed(ENamedElement element) {
		String name = element.getName();
// 		Either name is not set		
		if (!isNameSet(element)) {
			return false;
		}
//		Or character set not correct
		Iterator it = characters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			if (name.contains((CharSequence) entry.getKey()))
				return false;
		}
		return true;
	}
}
