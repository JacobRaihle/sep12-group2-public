package eaadapter.ea2ecore.postprocessings;
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import eaadapter.ea2ecore.PostProcessingTemplate;


public class SetSuperTypesForStereotypedElements extends PostProcessingTemplate {

	@Override
	public void execute() {
		
		TreeIterator<EObject> iterator = model.eAllContents();
		while (iterator.hasNext()) {
			EObject element = iterator.next();
			if (element instanceof EClass) {
				EClass cls = (EClass) element;
				if (cls.getEAnnotation("Stereotype") == null)
					continue;
				
				if (cls.getEAnnotation("Stereotype").getDetails().get("Stereotype") == null)
					continue;
				
				String stereotype = cls.getEAnnotation("Stereotype").getDetails().get("Stereotype");
				
				/**
				 * Special Case: Typo in EAST-ADL MM
				 */
				if (stereotype.equalsIgnoreCase("atpStructuredElement"))
					stereotype = "atpStructureElement";
				
				EClass stereotypeClass = getEClassForStereotype(stereotype);
					
				if (stereotypeClass == null) {
					EPackage p = cls.getEPackage();
					logger.error("Stereotype '" + stereotype + "' not found for " + cls.getName() + " " + createPathOfEPackage(p));					
				} else {
					cls.getESuperTypes().add(stereotypeClass);
					logger.info("added '" + stereotype + "' to class " + cls.getName() );
				}				
			}
		}
	}
	
	private EClass getEClassForStereotype(String stereotype) {
		EClassifier e = findEClassifierByName(stereotype);
		if (e instanceof EClass) {
			return (EClass) e;
		}
		return null;
	}
}
