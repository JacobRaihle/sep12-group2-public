/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package eaadapter.ea2ecore.postprocessings;

import org.eclipse.emf.ecore.EReference;

import eaadapter.ea2ecore.PostProcessingTemplate;


public class FixInvalidEOppositeReferences extends PostProcessingTemplate {

	
	@Override
	public void execute() {			
		/**
		 * Refernce 
		 * 'DomainModel/EAST-ADL/Structure/HardwareModeling/HardwareComponentPrototype/HardwareComponentType_part' 
		 * must have lowerBound set to 0. Otherwise, the Ecore can not be validated successfully.
		 * 
		 */
		EReference connector = (EReference) findENamedElementByName("HardwareComponentType_part");
		if (connector == null)
			logger.error("HardwareComponentType_part not found!");
		else
			connector.setLowerBound(0);
	}
	

}
