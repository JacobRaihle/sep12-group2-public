/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */
package eaadapter.impl;

import java.io.File;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.sparx.Diagram;
import org.sparx.Repository;

import eaadapter.EAAttribute;
import eaadapter.EAAttributeTag;
import eaadapter.EAConnector;
import eaadapter.EAConnectorConstraint;
import eaadapter.EAConnectorEnd;
import eaadapter.EAConnectorTag;
import eaadapter.EAConstraint;
import eaadapter.EAElement;
import eaadapter.EAMethod;
import eaadapter.EAMethodTag;
import eaadapter.EAPackage;
import eaadapter.EAParameter;
import eaadapter.EARepository;
import eaadapter.EARoleTag;
import eaadapter.EATaggedValue;
import eaadapter.EaadapterFactory;
import eaadapter.EaadapterPackage;
import eaadapter.abstracthierachy.EANamedElement;
import eaadapter.datatypes.T_ConnectorAggregation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EA Repository</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link eaadapter.impl.EARepositoryImpl#getEaLink <em>Ea Link</em>}</li>
 *   <li>{@link eaadapter.impl.EARepositoryImpl#getFile <em>File</em>}</li>
 *   <li>{@link eaadapter.impl.EARepositoryImpl#getModels <em>Models</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EARepositoryImpl extends EObjectImpl implements EARepository {
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "<copyright>\r\nCopyright (c) Continental AG and others.\r\nAll rights reserved. This program and the accompanying materials are made navailable under the terms of the Eclipse Public License \r\nwhich accompanies this distribution, and is navailable at http://www.eclipse.org/org/documents/epl-v10.php\r\n\r\nContributors:\r\n\tContinental AG, 2012 Matthias Nick - Initial API and implementation\r\n</copyright>";

	/**
	 * The default value of the '{@link #getEaLink() <em>Ea Link</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEaLink()
	 * @generated
	 * @ordered
	 */
	protected static final Repository EA_LINK_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEaLink() <em>Ea Link</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEaLink()
	 * @generated
	 * @ordered
	 */
	protected Repository eaLink = EA_LINK_EDEFAULT;

	/**
	 * The default value of the '{@link #getFile() <em>File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected static final File FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFile() <em>File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected File file = FILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModels() <em>Models</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModels()
	 * @generated
	 * @ordered
	 */
	protected EList<EAPackage> models;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EARepositoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EaadapterPackage.Literals.EA_REPOSITORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EAPackage> getModels() {
		if (models == null) {
			models = new EObjectContainmentEList<EAPackage>(EAPackage.class, this, EaadapterPackage.EA_REPOSITORY__MODELS);
		}
		return models;
	}
	
	public void addModel(EAPackage p) {
		this.getModels().add(p);
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Repository getEaLink() {
		return eaLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEaLink(Repository newEaLink) {
		Repository oldEaLink = eaLink;
		eaLink = newEaLink;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EaadapterPackage.EA_REPOSITORY__EA_LINK, oldEaLink, eaLink));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public File getFile() {
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFile(File newFile) {
		File oldFile = file;
		file = newFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EaadapterPackage.EA_REPOSITORY__FILE, oldFile, file));
	}

	private void openFile() throws Exception {
		if (this.getFile() == null)
			throw new Exception("EAP-file is not set");
		
		if (!this.getFile().exists())
			throw new Exception(file.getAbsolutePath() + " does not exist!");
		
		this.eaLink = new org.sparx.Repository();
		eaLink.OpenFile(this.getFile().getAbsolutePath());		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	public void load() throws Exception {
		openFile();
		clearModel();

		
		//initialize models
		for (org.sparx.Package modelPackage : eaLink.GetModels()) {
			this.getModels().add(createEAPackageFromSparxPackage(modelPackage));
		}
				
		for (EAPackage model : this.getModels()) {
			
			//initialize packages
			createSubpackagesForPackage(model);
			
			//initialize elements
			createElementsForPackage(model);
			
			/* Now that all packages and elements are
			 * initialized, the connectors can be
			 * initialized.
			 * Note: Before the connectors can be set,
			 * all of the Elements must be present, e.g.
			 * Element A in Package A references Element Z
			 * in Package Z --> All elements and packages
			 * must be present before.
			 */
			
			//initialize connectors
			createConnectorsForPackage(model);			
		}		
		

		eaLink.CloseFile();
	}
	

	private void clearModel() {
		if (this.getModels().size() > 0) {
			this.getModels().clear();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	public EANamedElement getEANamedElementByGUID(String guid) {
		for (TreeIterator<EANamedElement> iter = EcoreUtil.getAllContents(this.getModels()); iter.hasNext();) {
			EANamedElement element = (EANamedElement) iter.next();
			if (element.getGuid().equals(guid)) {
				return element;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	public EAElement getEAElementByGUID(String guid) {
		EANamedElement result = getEANamedElementByGUID(guid);
		if (result instanceof EAElement) {
			return (EAElement) result;
		} else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	public EList<EANamedElement> getEANamedElementByID(long id) {
		EList<EANamedElement> result = new BasicEList<EANamedElement>();
		for (TreeIterator<EANamedElement> iter = EcoreUtil.getAllContents(this.getModels()); iter.hasNext();) {
			EANamedElement element = (EANamedElement) iter.next();
			if (element.getId() == id) {
				result.add(element);
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT mnick
	 */
	public EList<EAElement> getEAElementByID(long id) {
		EList<EAElement> result = new BasicEList<EAElement>();
		EList<EANamedElement> allElements = getEANamedElementByID(id);
		for (EANamedElement element : allElements) {
			if (element instanceof EAElement)
				result.add((EAElement) element);
		}
		return result;
	}

	private void createConnectorsForPackage(EAPackage pkg) {
		for (TreeIterator<EANamedElement> iter = EcoreUtil.getAllContents(this.getModels()); iter.hasNext();) {
			EANamedElement element = (EANamedElement) iter.next();
			if (element instanceof EAElement) {
				EAElement eaElement = (EAElement) element;
				for (org.sparx.Connector c : eaElement.getEaLink().GetConnectors()) {
					
					//only add the connector if its 'source' is the current element				
					EAConnector eaConnector = createEAConnectorFromSparxConnector(c);
					if (eaConnector.getClientID() == eaElement.getId()) {					
						eaElement.getConnectors().add(eaConnector);
					}
				}
			}
		}
	}
	
	private EAConnectorEnd createEAConnectorEndFromSparxConnectorEnd(org.sparx.ConnectorEnd ce) {
		EAConnectorEnd connEnd = EaadapterFactory.eINSTANCE.createEAConnectorEnd();
		
		switch (ce.GetAggregation()) {
		case 0:
			connEnd.setAggregation(T_ConnectorAggregation.NONE);
			break;
		case 1:
			connEnd.setAggregation(T_ConnectorAggregation.SHARED);
			break;
		case 2:
			connEnd.setAggregation(T_ConnectorAggregation.COMPOSITE);
			break;
		default:
			connEnd.setAggregation(T_ConnectorAggregation.NONE);
			break;			
		}
		
		connEnd.setCardinality(ce.GetCardinality());
		connEnd.setConstraint(ce.GetConstraint());
		connEnd.setContainment(ce.GetContainment());
		connEnd.setDerived(ce.GetDerived());
		connEnd.setEaLink(ce);
		connEnd.setEnd(ce.GetEnd());
		connEnd.setIsChangable(ce.GetIsChangeable());
		connEnd.setIsNavigable(ce.GetIsNavigable());
		connEnd.setOrdering(ce.GetOrdering());
		connEnd.setQualifier(ce.GetQualifier());
		connEnd.setRole(ce.GetRole());
		connEnd.setRoleNote(ce.GetRoleNote());
		connEnd.setRoleType(ce.GetRoleType());
		connEnd.setVisibility(ce.GetVisibility());
		
		for (org.sparx.RoleTag rt : ce.GetTaggedValues()) {
			EARoleTag roleTag = EaadapterFactory.eINSTANCE.createEARoleTag();
			roleTag.setBaseClass(rt.GetBaseClass());
			roleTag.setEaLink(rt);
			roleTag.setElementGUID(rt.GetElementGUID());
			roleTag.setPropertyGUID(rt.GetPropertyGUID());
			roleTag.setTag(rt.GetTag());
			roleTag.setValue(rt.GetValue());
			connEnd.getTaggedValues().add(roleTag);
		}
		
		return connEnd;
	}
	
	private EAConnectorConstraint createEAConnectorConstraintFromSparxConnectorConstraint(org.sparx.ConnectorConstraint cc) {
		EAConnectorConstraint eaCC  = EaadapterFactory.eINSTANCE.createEAConnectorConstraint();
		eaCC.setConnectorID(cc.GetConnectorID());
		eaCC.setEaLink(cc);
		eaCC.setName(cc.GetName());
		eaCC.setNotes(cc.GetNotes());
		eaCC.setType(cc.GetType());
		return eaCC;
	}
	
	private EAConnector createEAConnectorFromSparxConnector(org.sparx.Connector c) {
		EAConnector eaConnector = EaadapterFactory.eINSTANCE.createEAConnector();
		eaConnector.setClientEnd(createEAConnectorEndFromSparxConnectorEnd(c.GetClientEnd()));
		eaConnector.setConnectorID(c.GetConnectorID());
		
		//create constraints
		eaConnector.setClientID(c.GetClientID());
		for (org.sparx.ConnectorConstraint cc : c.GetConstraints()) {
			eaConnector.getConstraints().add(createEAConnectorConstraintFromSparxConnectorConstraint(cc));
		}
		
		eaConnector.setDirection(c.GetDirection());
		eaConnector.setEaLink(c);
		eaConnector.setEventFlags(c.GetEventFlags());
		eaConnector.setGuid(c.GetConnectorGUID());
		eaConnector.setId(c.GetConnectorID());
		eaConnector.setIsLeaf(c.GetIsLeaf());
		eaConnector.setIsRoot(c.GetIsRoot());
		eaConnector.setIsSpec(c.GetIsSpec());
		eaConnector.setName(c.GetName());
		eaConnector.setNotes(c.GetNotes());
		eaConnector.setRouteStyle(c.GetRouteStyle());
		eaConnector.setSequenceNo(c.GetSequenceNo());
		eaConnector.setStereotype(c.GetStereotype());
		eaConnector.setStereotypeEx(c.GetStereotypeEx());
		eaConnector.setStyleEx(c.GetStyleEx());
		eaConnector.setSubtype(c.GetSubtype());
		
		/*
		 * The connector's supplier is identified via its supplierID.
		 * However, we don't know if this ID is unique (Sparx-Support says: "it is unique within the model, but not within the project")
		 * Therefore the getEAElementByID() method must return exactly 1 Element!
		 * Otherwise we have a Problem.
		 */
		EList<EAElement> supplierList = this.getEAElementByID(c.GetSupplierID());
		if (supplierList.size() == 1) {
			System.out.println("found supplier: " + supplierList.get(0).getName());
			eaConnector.setSupplier(supplierList.get(0));
		} else {
			System.err.println("ERROR, could not find SUPPLIER / too much supplier for connector. Size: " + supplierList.size());
		}
		
		eaConnector.setSupplierEnd(createEAConnectorEndFromSparxConnectorEnd(c.GetSupplierEnd()));
		eaConnector.setSupplierID(c.GetSupplierID());
		eaConnector.setTransitionAction(c.GetTransitionAction());
		eaConnector.setTransitionEvent(c.GetTransitionEvent());
		eaConnector.setTransitionGuard(c.GetTransitionGuard());
		eaConnector.setType(c.GetType());
		eaConnector.setVirtualInheritance(c.GetVirtualInheritance());
		
		//Add tagged values
		for (org.sparx.ConnectorTag cTag : c.GetTaggedValues()) {
			EAConnectorTag eaCTag = EaadapterFactory.eINSTANCE.createEAConnectorTag();
			eaCTag.setEaLink(cTag);
			eaCTag.setGuid(cTag.GetTagGUID());
			eaCTag.setName(cTag.GetName());
			eaCTag.setNotes(cTag.GetNotes());
			eaCTag.setId(cTag.GetTagID());
			eaCTag.setValue(cTag.GetValue());
			eaConnector.getTaggedValues().add(eaCTag);
		}
		return eaConnector;
	}
	
	private void createElementsForPackage(EAPackage root) {
		for (EAPackage currentPackage : root.getPackages()) {
			for (org.sparx.Element e : currentPackage.getEaLink().GetElements()) {
				EAElement eaElement = createEAElementFromSparxElement(e);
				currentPackage.getElements().add(eaElement);
				System.out.println("added element " + eaElement.getName() + " in " + root.getName());				
			}
			createElementsForPackage(currentPackage);
		}
		
	}
	
	
	private EAElement createEAElementFromSparxElement(org.sparx.Element e) {
		EAElement eaElement = EaadapterFactory.eINSTANCE.createEAElement();
				
		if (e.GetAbstract().equals("1"))
			eaElement.setIsAbstract(true);
		else
			eaElement.setIsAbstract(false);
		
		eaElement.setAuthor(e.GetAuthor());
		eaElement.setClassifierName(e.GetClassifierName());
		eaElement.setClassifierID(e.GetClassfierID());
		eaElement.setComplexity(e.GetComplexity());
		eaElement.setDifficulty(e.GetDifficulty());
		eaElement.setEaLink(e);
		eaElement.setExtensionPoints(e.GetExtensionPoints());
		eaElement.setGenfile(e.GetGenfile());
		eaElement.setGenlinks(e.GetGenlinks());
		eaElement.setGentype(e.GetGentype());
		eaElement.setGuid(e.GetElementGUID());
		eaElement.setId(e.GetElementID());
		eaElement.setIsActive(e.GetIsActive());
		eaElement.setIsLocked(e.GetIsLeaf());
		eaElement.setMultiplicity(e.GetMultiplicity());
		eaElement.setMetaType(e.GetMetaType());
		eaElement.setName(e.GetName());
		eaElement.setNotes(e.GetNotes());
		eaElement.setPackageID(e.GetPackageID());
		eaElement.setParentID(e.GetParentID());
		eaElement.setPhase(e.GetPhase());
		eaElement.setPriority(e.GetPriority());
		eaElement.setPropertyType(e.GetPropertyType());
		eaElement.setStatus(e.GetStatus());
		eaElement.setStereotype(e.GetStereotype());
		eaElement.setStereotypeEx(e.GetStereotypeEx());
		eaElement.setSubtype(e.GetSubtype());
		eaElement.setTablespace(e.GetTablespace());
		eaElement.setTablespace(e.GetTag());
		eaElement.setVersion(e.GetVersion());
		eaElement.setVisibility(e.GetVisibility());
		
		for (org.sparx.TaggedValue tv : e.GetTaggedValues()) {
			EATaggedValue eaTv = EaadapterFactory.eINSTANCE.createEATaggedValue();
			eaTv.setEaLink(tv);
			eaTv.setId(tv.GetElementID());
			eaTv.setGuid(tv.GetPropertyGUID());
			eaTv.setName(tv.GetName());
			eaTv.setNotes(tv.GetNotes());
			eaTv.setValue(tv.GetValue());
			eaElement.getTaggedValues().add(eaTv);
		}
	
		//create constraints here
		for (org.sparx.Constraint constr : e.GetConstraints()) {
			EAConstraint eaConstraint = EaadapterFactory.eINSTANCE.createEAConstraint();
			eaConstraint.setName(constr.GetName());
			eaConstraint.setNotes(constr.GetNotes());
			eaConstraint.setStatus(constr.GetStatus());
			eaConstraint.setType(constr.GetType());
			eaElement.getConstraints().add(eaConstraint);
		}
		
		//create attributes here
		for (org.sparx.Attribute attr : e.GetAttributes()) {
			EAAttribute newAttrib = createEAAttributeFromSparxAttribute(attr);
			eaElement.getAttributes().add(newAttrib);
			System.out.println("added attribute " + newAttrib.getName() + " in Element " + newAttrib.getElement().getName() );
		}
		
		//create methods here
		for (org.sparx.Method mthd : e.GetMethods()) {
			EAMethod newMethod = createEAMethodFromSparxMethod(mthd);
			eaElement.getMethods().add(newMethod);
			System.out.println("added method " + newMethod.getName() + " in Element " + newMethod.getElement().getName() );
		}
		
		return eaElement;
	}
	
	private EAAttribute createEAAttributeFromSparxAttribute(org.sparx.Attribute a) {
			EAAttribute eaAttribute = EaadapterFactory.eINSTANCE.createEAAttribute();
			eaAttribute.setAllowDuplicates(a.GetAllowDuplicates());
			eaAttribute.setContainer(a.GetContainer());
			eaAttribute.setContainment(a.GetContainment());
			eaAttribute.setClassifierID(a.GetClassifierID());
			eaAttribute.setDefault(a.GetDefault());
			eaAttribute.setEaLink(a);
			eaAttribute.setGuid(a.GetAttributeGUID());
			eaAttribute.setId(a.GetAttributeID());
			eaAttribute.setCollection(a.GetIsCollection());
			eaAttribute.setIsConst(a.GetIsConst());
			eaAttribute.setDerived(a.GetIsDerived());
			eaAttribute.setOrdered(a.GetIsOrdered());
			eaAttribute.setIsStatic(a.GetIsStatic());
			eaAttribute.setLength(a.GetLength());
			eaAttribute.setLowerBound(a.GetLowerBound());
			eaAttribute.setName(a.GetName());
			eaAttribute.setNotes(a.GetNotes());
			eaAttribute.setPrecision(a.GetPrecision());
			eaAttribute.setScale(a.GetScale());
			eaAttribute.setStereotype(a.GetStereotype());
			eaAttribute.setStereotypeEx(a.GetStereotypeEx());
			eaAttribute.setType(a.GetType());
			eaAttribute.setUpperBound(a.GetUpperBound());
			eaAttribute.setVisibility(a.GetVisibility());
			
			for (org.sparx.AttributeTag attrTag : a.GetTaggedValues()) {
				EAAttributeTag eaAttribTag = EaadapterFactory.eINSTANCE.createEAAttributeTag();
				eaAttribTag.setEaLink(attrTag);
				eaAttribTag.setGuid(attrTag.GetTagGUID());
				eaAttribTag.setId(attrTag.GetTagID());
				eaAttribTag.setName(attrTag.GetName());
				eaAttribTag.setNotes(attrTag.GetNotes());
				eaAttribTag.setValue(attrTag.GetValue());
				eaAttribute.getTaggedValues().add(eaAttribTag);
			}
			return eaAttribute;
	}	
	
	private EAMethod createEAMethodFromSparxMethod(org.sparx.Method m) {
		EAMethod eaMethod = EaadapterFactory.eINSTANCE.createEAMethod();
		eaMethod.setCode(m.GetCode());
		eaMethod.setConcurrency(m.GetConcurrency());
		eaMethod.setEaLink(m);
		eaMethod.setGuid(m.GetMethodGUID());
		eaMethod.setId(m.GetMethodID());
		eaMethod.setIsAbstract(m.GetAbstract());
		eaMethod.setIsConst(m.GetIsConst());
		eaMethod.setIsLeaf(m.GetIsLeaf());
		eaMethod.setIsPure(m.GetIsPure());
		eaMethod.setIsQuery(m.GetIsQuery());
		eaMethod.setIsRoot(m.GetIsRoot());
		eaMethod.setIsStatic(m.GetIsStatic());
		eaMethod.setIsSynchronized(m.GetIsSynchronized());
		eaMethod.setName(m.GetName());
		eaMethod.setNotes(m.GetNotes());
		eaMethod.setReturnIsArray(m.GetReturnIsArray());
		eaMethod.setReturnType(m.GetReturnType());
		eaMethod.setStereotype(m.GetStereotype());
		eaMethod.setStereotypeEx(m.GetStereotypeEx());
		try {
			eaMethod.setClassifierID(Integer.valueOf(m.GetClassifierID()));
		} catch (NumberFormatException e) {
			System.err.println(m.GetName() + ": " + m.GetClassifierID() + " can't be casted to a number!");
		}
		eaMethod.setThrows(m.GetThrows());
		eaMethod.setType("");

		//create Parameters
		for (org.sparx.Parameter par : m.GetParameters()) {
			EAParameter eaPar = createEAParameterFromSparxParameter(par);
			eaMethod.getParameters().add(eaPar);
		}
		
		//create Tagged Values
		for (org.sparx.MethodTag mTag : m.GetTaggedValues()) {
			EAMethodTag eaMtag = EaadapterFactory.eINSTANCE.createEAMethodTag();
			eaMtag.setEaLink(mTag);
			eaMtag.setGuid(mTag.GetTagGUID());
			eaMethod.setId(mTag.GetTagID());
			eaMtag.setName(mTag.GetName());
			eaMtag.setNotes(mTag.GetNotes());
			eaMtag.setValue(mTag.GetValue());
			eaMethod.getTaggedValues().add(eaMtag);
		}
		return eaMethod;
	}
	
	private EAParameter createEAParameterFromSparxParameter(org.sparx.Parameter p) {
		EAParameter eaPar = EaadapterFactory.eINSTANCE.createEAParameter();
		try {
			eaPar.setClassifierID(Integer.valueOf(p.GetClassifierID()));
		} catch (NumberFormatException e) {
			System.err.println(p.GetName() + ": " + p.GetClassifierID() + " can't be casted to a number!");
		}
		eaPar.setDefault(p.GetDefault());
		eaPar.setEaLink(p);
		eaPar.setGuid(p.GetParameterGUID());
		eaPar.setId(p.GetOperationID()); /** TODO: don't know if this is the right one ???? */
		eaPar.setIsConst(p.GetIsConst());
		eaPar.setKind(p.GetKind());
		eaPar.setName(p.GetName());
		eaPar.setNotes(p.GetNotes());
		eaPar.setPosition(p.GetPosition());
		eaPar.setStereotype(p.GetStereotype());
		eaPar.setType(p.GetType());
		return eaPar;
	}
	
	private void createSubpackagesForPackage(EAPackage root) {
		for (org.sparx.Package pkg : root.getEaLink().GetPackages()) {
			EAPackage newPackage = createEAPackageFromSparxPackage(pkg);
			root.getPackages().add(newPackage);
			System.out.println("added: " + newPackage.getName());
			createSubpackagesForPackage(newPackage);
		}		
	}
	
	private EAPackage createEAPackageFromSparxPackage(org.sparx.Package pkg) {
		EAPackage eaPackage = EaadapterFactory.eINSTANCE.createEAPackage();
		eaPackage.setEaLink(pkg);
		eaPackage.setCodePath(pkg.GetCodePath());
		eaPackage.setFlags(pkg.GetFlags());
		eaPackage.setName(pkg.GetName());
		eaPackage.setNotes(pkg.GetNotes());
		eaPackage.setStereotype(pkg.GetStereotypeEx());
		eaPackage.setVersion(pkg.GetVersion());
		eaPackage.setGuid(pkg.GetPackageGUID());
		eaPackage.setId(pkg.GetPackageID());
		return eaPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EaadapterPackage.EA_REPOSITORY__MODELS:
				return ((InternalEList<?>)getModels()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EaadapterPackage.EA_REPOSITORY__EA_LINK:
				return getEaLink();
			case EaadapterPackage.EA_REPOSITORY__FILE:
				return getFile();
			case EaadapterPackage.EA_REPOSITORY__MODELS:
				return getModels();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EaadapterPackage.EA_REPOSITORY__EA_LINK:
				setEaLink((Repository)newValue);
				return;
			case EaadapterPackage.EA_REPOSITORY__FILE:
				setFile((File)newValue);
				return;
			case EaadapterPackage.EA_REPOSITORY__MODELS:
				getModels().clear();
				getModels().addAll((Collection<? extends EAPackage>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EaadapterPackage.EA_REPOSITORY__EA_LINK:
				setEaLink(EA_LINK_EDEFAULT);
				return;
			case EaadapterPackage.EA_REPOSITORY__FILE:
				setFile(FILE_EDEFAULT);
				return;
			case EaadapterPackage.EA_REPOSITORY__MODELS:
				getModels().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EaadapterPackage.EA_REPOSITORY__EA_LINK:
				return EA_LINK_EDEFAULT == null ? eaLink != null : !EA_LINK_EDEFAULT.equals(eaLink);
			case EaadapterPackage.EA_REPOSITORY__FILE:
				return FILE_EDEFAULT == null ? file != null : !FILE_EDEFAULT.equals(file);
			case EaadapterPackage.EA_REPOSITORY__MODELS:
				return models != null && !models.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (eaLink: ");
		result.append(eaLink);
		result.append(", file: ");
		result.append(file);
		result.append(')');
		return result.toString();
	}

} //EARepositoryImpl
