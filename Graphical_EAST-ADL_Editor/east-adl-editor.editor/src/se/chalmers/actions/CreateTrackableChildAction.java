package se.chalmers.actions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.action.CreateChildAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;

/**
 * A class that extends the EMF CreateChildAction class and adds functionality for tracking
 * the latest used children
 */
public class CreateTrackableChildAction extends CreateChildAction {
	
	/**
	 * This contains class name of the node that was selected when the action was created.
	 */
	private String childSelection;
	
	/**
	 * This hashmap contains the latest used child actions grouped by their "childSelection" (its parent)
	 */
	private static HashMap<String, LinkedHashMap<String, CreateTrackableChildAction>> latest = new HashMap<String, LinkedHashMap<String, CreateTrackableChildAction>>();
	
	/**
	 * This method returns the hashmap with the latest used child actions
	 */
	public static HashMap<String, LinkedHashMap<String, CreateTrackableChildAction>> getLatestChildActions() {
		return latest;
	}

	/**
	 * This constructor stores the class name of the selected node and calls the super constructor
	 */
	public CreateTrackableChildAction(IEditorPart editorPart, ISelection selection,
			Object descriptor) {
		super(editorPart, selection, descriptor);
		this.childSelection = ((IStructuredSelection)selection).getFirstElement().getClass().toString();
	}
	
	/**
	 * This method is run when the action is triggered. It calls the super run() method and then stores the
	 * triggered action in the latest list.
	 */
	public void run() {
		super.run();
		if (latest.get(this.childSelection) == null) {
			latest.put(this.childSelection, new LinkedHashMap<String, CreateTrackableChildAction>(5));
		}
		
		// Is the list full? Remove the oldest item
		if (latest.get(this.childSelection).size() >= 5 && !latest.get(this.childSelection).containsKey(this.getText())) {
			Iterator<String> oldest = latest.get(this.childSelection).keySet().iterator();
			latest.get(this.childSelection).remove(oldest.next());
		}
		
		// Check if the child is already in the latest used list and remove it if it is.
		if (latest.get(this.childSelection).containsKey(this.getText())) {
			latest.get(this.childSelection).remove(this.getText());
		}
		
		latest.get(this.childSelection).put(this.getText(), this);
	}
}