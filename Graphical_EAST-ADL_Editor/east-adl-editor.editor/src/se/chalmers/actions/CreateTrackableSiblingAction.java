package se.chalmers.actions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.action.CreateSiblingAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;

/**
 * A class that extends the EMF CreateSiblingAction class and adds functionality for tracking
 * the latest used siblings
 */
public class CreateTrackableSiblingAction extends CreateSiblingAction {
	
	/**
	 * This contains class name of the node that was selected when the action was created.
	 */
	private String siblingSelection;
	
	/**
	 * This hashmap contains the latest used sibling actions grouped by their "siblingSelection"
	 */
	private static HashMap<String, LinkedHashMap<String, CreateTrackableSiblingAction>> latest = new HashMap<String, LinkedHashMap<String, CreateTrackableSiblingAction>>();
	
	/**
	 * This method returns the hashmap with the latest used child actions
	 */
	public static HashMap<String, LinkedHashMap<String, CreateTrackableSiblingAction>> getLatestSiblingActions() {
		return latest;
	} 

	/**
	 * This constructor stores the class name of the selected node and calls the super constructor
	 */
	public CreateTrackableSiblingAction(IEditorPart editorPart,
			ISelection selection, Object descriptor) {
		super(editorPart, selection, descriptor);
		this.siblingSelection = ((IStructuredSelection)selection).getFirstElement().getClass().toString();
	}

	/**
	 * This method is run when the action is triggered. It calls the super run() method and then stores the
	 * triggered action in the latest list.
	 */
	public void run() {
		super.run();
		if (latest.get(this.siblingSelection) == null) {
			latest.put(this.siblingSelection, new LinkedHashMap<String, CreateTrackableSiblingAction>(5));
		}
		
		// Is the list full? Remove the oldest item
		if (latest.get(this.siblingSelection).size() >= 5 && !latest.get(this.siblingSelection).containsKey(this.getText())) {
			Iterator<String> oldest = latest.get(this.siblingSelection).keySet().iterator();
			latest.get(this.siblingSelection).remove(oldest.next());
		}
		
		// Check if the sibling is already in the latest used list and remove it if it is.
		if (latest.get(this.siblingSelection).containsKey(this.getText())) {
			latest.get(this.siblingSelection).remove(this.getText());
		}
		
		latest.get(this.siblingSelection).put(this.getText(), this);
	}
}
