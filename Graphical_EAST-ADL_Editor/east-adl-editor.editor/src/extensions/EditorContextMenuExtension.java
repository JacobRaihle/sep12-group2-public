package extensions;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;

public interface EditorContextMenuExtension {
	/**
	 * Adds menu items to an EAST-ADL editor context menu
	 * @param menu the menu to add items to
	 * @param context the selection that the menu is being opened for
	 */
	public void contributeToMenu(IMenuManager menu, TreePath[] selections);
	
}
