package extensions;

import org.eclipse.jface.action.IToolBarManager;

public interface EditorToolbarExtension {

	void contributeToToolbar(IToolBarManager toolBarManager);

}
