package extensions;

import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.Bundle;

public class ExampleToolbarExtension implements EditorToolbarExtension {

	@Override
	public void contributeToToolbar(IToolBarManager toolBarManager) {
		toolBarManager.insertAfter("elements-additions", new Action() {
			@Override
			public void run() {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						JOptionPane.showMessageDialog(null, "Hello, world");
					}
				});
			}
			
			@Override
			public String getToolTipText() {
				return "Hello, world";
			}
			
			@Override
			public ImageDescriptor getImageDescriptor() {
				Bundle bundle = Platform.getBundle("east-adl-editor.editor");
				URL fullPathString = bundle.getEntry("icons/east-adl.gif");
				return ImageDescriptor.createFromURL(fullPathString);
			}
		});
	}
}
