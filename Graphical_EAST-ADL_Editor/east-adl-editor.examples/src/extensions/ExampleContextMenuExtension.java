package extensions;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;

public class ExampleContextMenuExtension implements EditorContextMenuExtension {

	public void contributeToMenu(IMenuManager menu, final TreePath[] selections) {
		
		MenuManager newMenu = new MenuManager("New sub menu");
		newMenu.add(new Action() {
			@Override
			public void run() {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						StringBuilder message = new StringBuilder();
						if (selections.length == 0) {
							message.append("You have not made a selection.");
						} else if (selections.length == 1) {
							message.append("You have selected the item ");
							message.append(selections[0].getLastSegment().toString());
						} else {
							message.append("You have selected the items");
							for (TreePath selection : selections) {
								message.append("\n  ");
								message.append(selection.getLastSegment().toString());
							}
						}
						JOptionPane.showMessageDialog(null, message.toString());
					}
				});
			}
			
			@Override
			public String getText() {
				return "Hello, world";
			}
			
			@Override
			public String getToolTipText() {
				return "Hello, world";
			}
		});
		menu.insertAfter("additions", newMenu);
	}
}
