package ExtensionPoints;

public interface ITreeEditorLabelProvider {
	/**
	 * Called when the label for an item in the tree view is being fetched.
	 * @param itemProvider The item provider for the tree item
	 * @param treeItem     The tree item that should be described.
	 * @param currentText  The current label of the item. If several extensions are connected they will receive the output of the previous.
	 * @return The new label to display for the item
	 */
	public String getTextForTreeItem(Object itemProvider, Object treeItem, String currentText);
}
