package ExtensionPoints;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

public class ExtensionPointInvoker {

	// Calls the extension connected to the tree item description extension point
	public static String invokeTreeExtensionPoint(Object itemProvider, Object treeItem, String text)
	{
		// Call the extensions
		IConfigurationElement[] config =
			Platform.getExtensionRegistry().getConfigurationElementsFor("east-adl-editor.edit.se.chalmers.ieee.TreeItemText");
		
		for (IConfigurationElement e : config) {
			try {
				Object extensionInstance = e.createExecutableExtension("class");
				if (extensionInstance instanceof ITreeEditorLabelProvider)
					text = ((ITreeEditorLabelProvider) extensionInstance).getTextForTreeItem(extensionInstance, treeItem, text);
			} catch (CoreException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		return text;
	}
}
