package se.chalmers.ieee.eastadleditor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CopyCommand.Helper;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionTypeItemProvider;
import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

public class PrototypeList extends ChildList
{
	public PrototypeList(AdapterFactory adapterFactory, EObject function) {
		super(adapterFactory, function);
	}

	@Override
	protected String getChildMethodName() {
		return "getPart";
	}
	
	@Override
	public Collection<?> getNewChildDescriptors(Object object,
			EditingDomain editingDomain, Object sibling) {
		ArrayList descriptors = new ArrayList(1);
		if (getContainer() instanceof DesignFunctionType) {
			descriptors.add(createChildParameter(
					FunctionModelingPackage.Literals.DESIGN_FUNCTION_TYPE__PART,
					FunctionModelingFactory.eINSTANCE
							.createDesignFunctionPrototype()));
		} else if (getContainer() instanceof AnalysisFunctionType){
			descriptors.add(createChildParameter(
					FunctionModelingPackage.Literals.ANALYSIS_FUNCTION_TYPE__PART,
					FunctionModelingFactory.eINSTANCE
							.createAnalysisFunctionPrototype()));
			
		}
		return descriptors;
	}

	@Override
	public String getText(Object object) {
		return "Parts";
	}

	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Parts"));
	}
	
}
