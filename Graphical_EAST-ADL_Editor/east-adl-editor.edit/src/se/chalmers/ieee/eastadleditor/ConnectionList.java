package se.chalmers.ieee.eastadleditor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;

public class ConnectionList extends ChildList {
	public ConnectionList(AdapterFactory adapterFactory, EObject function) {
		super(adapterFactory, function);
	}

	
	
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FunctionModelingPackage.Literals.FUNCTION_TYPE__CONNECTOR);
		}
		return childrenFeatures;
	}
	
	@Override
	public Collection<?> getNewChildDescriptors(Object object,
			EditingDomain editingDomain, Object sibling) {
		ArrayList<Object> descriptors = new ArrayList<Object>(1);
		descriptors.add(createChildParameter
			(FunctionModelingPackage.Literals.FUNCTION_TYPE__CONNECTOR,
			 FunctionModelingFactory.eINSTANCE.createFunctionConnector()));
		return descriptors;
	}
	
	@Override
	public String getText(Object object) {
		return "Connections";
	}
	
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Connections"));
	}

	@Override
	protected String getChildMethodName() {
		return "getConnector";
	}
}
