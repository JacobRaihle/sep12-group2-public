package se.chalmers.ieee.eastadleditor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

public abstract class ChildList 
extends ItemProviderAdapter 
implements 
  IEditingDomainItemProvider, 
  IStructuredItemContentProvider, 
  ITreeItemContentProvider, 
  IItemLabelProvider, 
  IItemPropertySource 
{
	private final EObject parent;

	protected EObject getContainer() {
		return parent;
	}
	
	public ChildList(AdapterFactory adapterFactory, EObject parent) {
		super(adapterFactory);
		this.parent = parent;
		parent.eAdapters().add(this);
	}

	/**
	 * Providers the name of the method to (reflectively) invoke on the container to get the list of children included in this virtual child.
	 * If getChildren is overridden, this method need not return anything useful.
	 */
	protected abstract String getChildMethodName();
	
	@Override
	public Collection<?> getChildren(Object object) {
		Class<?> klass = getContainer().getClass();
		Method method;
		try {
			method = klass.getMethod(getChildMethodName());
			return (Collection<?>) method.invoke(getContainer());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		//return getContainer().getClass().getM  //getConnector();
	}
	
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
		}
		return childrenFeatures;
	}
	
	@Override
	public Object getParent(Object object) {
		return parent;
	}
	
	@Override
	public abstract Collection<?> getNewChildDescriptors(Object object,
			EditingDomain editingDomain, Object sibling);
	
	@Override
	public abstract String getText(Object object);
	
	@Override
	public Command createCommand(Object object, EditingDomain domain,
			Class<? extends Command> commandClass,
			CommandParameter commandParameter) {
		commandParameter.setOwner(parent);
		return super.createCommand(parent, domain, commandClass, commandParameter);
	}
	
	@Override
	public abstract Object getImage(Object object);
	
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}
}
