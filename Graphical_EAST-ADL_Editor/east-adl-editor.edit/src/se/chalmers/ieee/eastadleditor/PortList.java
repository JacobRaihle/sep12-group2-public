package se.chalmers.ieee.eastadleditor;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;

public class PortList extends ChildList {
	public PortList(AdapterFactory adapterFactory, EObject function) {
		super(adapterFactory, function);
	}
	
	@Override
	protected String getChildMethodName() {
		return "getPort";
	}
	
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT);
		}
		return childrenFeatures;
	}
	
	@Override
	public Collection<?> getNewChildDescriptors(Object object,
			EditingDomain editingDomain, Object sibling) {
		ArrayList<Object> descriptors = new ArrayList<Object>(7);
		
		descriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionPowerPort()));

		descriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionInFlowPort()));

		descriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionOutFlowPort()));

		descriptors
				.add(createChildParameter(
						FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
						FunctionModelingFactory.eINSTANCE
								.createFunctionInOutFlowPort()));

		descriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionClientPort()));

		descriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionServerPort()));

		descriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionPortProxy()));
		
		return descriptors;
	}
	
	@Override
	public String getText(Object object) {
		return "Ports";
	}
	
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Ports"));
	}
}
