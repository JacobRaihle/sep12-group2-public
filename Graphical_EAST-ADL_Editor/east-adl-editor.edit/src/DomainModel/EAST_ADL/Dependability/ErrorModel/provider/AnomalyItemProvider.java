/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.ErrorModel.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.EAST_ADL.Dependability.ErrorModel.Anomaly;
import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelPackage;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Dependability.ErrorModel.Anomaly} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AnomalyItemProvider extends EAElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnomalyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpContextElementPropertyDescriptor(object);
			addAtpInstanceRef_atpTargetPropertyDescriptor(object);
			addAtpTypePropertyDescriptor(object);
			addGenericDescriptionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addFaultFailure_anomaly_anomalyPropertyDescriptor(object);
			addParameterCondition_representAnomalyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Context Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpContextElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_CONTEXT_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpTarget_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AtpPrototype_atpType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AtpPrototype_atpType_feature",
						"_UI_AtpPrototype_type"),
				AbstractStructurePackage.Literals.ATP_PROTOTYPE__ATP_TYPE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Generic Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGenericDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Anomaly_genericDescription_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Anomaly_genericDescription_feature",
						"_UI_Anomaly_type"),
				ErrorModelPackage.Literals.ANOMALY__GENERIC_DESCRIPTION, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Anomaly_type_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Anomaly_type_feature", "_UI_Anomaly_type"),
				ErrorModelPackage.Literals.ANOMALY__TYPE, true, false, true,
				null, null, null));
	}

	/**
	 * This adds a property descriptor for the Fault Failure anomaly anomaly feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailure_anomaly_anomalyPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Anomaly_FaultFailure_anomaly_anomaly_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Anomaly_FaultFailure_anomaly_anomaly_feature",
								"_UI_Anomaly_type"),
						ErrorModelPackage.Literals.ANOMALY__FAULT_FAILURE_ANOMALY_ANOMALY,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Parameter Condition represent Anomaly feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterCondition_representAnomalyPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Anomaly_ParameterCondition_representAnomaly_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Anomaly_ParameterCondition_representAnomaly_feature",
								"_UI_Anomaly_type"),
						ErrorModelPackage.Literals.ANOMALY__PARAMETER_CONDITION_REPRESENT_ANOMALY,
						true, false, true, null, null, null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Anomaly) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Anomaly_type")
				: getString("_UI_Anomaly_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Anomaly.class)) {
		case ErrorModelPackage.ANOMALY__GENERIC_DESCRIPTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
