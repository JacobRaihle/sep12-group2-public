/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.ErrorModel.provider;

import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelPackage;
import DomainModel.EAST_ADL.Dependability.ErrorModel.FaultFailurePort;

import DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef._instanceRefFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Dependability.ErrorModel.FaultFailurePort} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FaultFailurePortItemProvider extends AnomalyItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultFailurePortItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFaultFailurePropagationLink_fromPort_faultFailurePortPropertyDescriptor(object);
			addFaultFailurePropagationLink_toPort_faultFailurePortPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Fault Failure Propagation Link from Port fault Failure Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailurePropagationLink_fromPort_faultFailurePortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FaultFailurePort_FaultFailurePropagationLink_fromPort_faultFailurePort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FaultFailurePort_FaultFailurePropagationLink_fromPort_faultFailurePort_feature",
								"_UI_FaultFailurePort_type"),
						ErrorModelPackage.Literals.FAULT_FAILURE_PORT__FAULT_FAILURE_PROPAGATION_LINK_FROM_PORT_FAULT_FAILURE_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Fault Failure Propagation Link to Port fault Failure Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailurePropagationLink_toPort_faultFailurePortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FaultFailurePort_FaultFailurePropagationLink_toPort_faultFailurePort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FaultFailurePort_FaultFailurePropagationLink_toPort_faultFailurePort_feature",
								"_UI_FaultFailurePort_type"),
						ErrorModelPackage.Literals.FAULT_FAILURE_PORT__FAULT_FAILURE_PROPAGATION_LINK_TO_PORT_FAULT_FAILURE_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ErrorModelPackage.Literals.FAULT_FAILURE_PORT__FUNCTION_TARGET);
			childrenFeatures
					.add(ErrorModelPackage.Literals.FAULT_FAILURE_PORT__HW_TARGET);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FaultFailurePort) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_FaultFailurePort_type")
				: getString("_UI_FaultFailurePort_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FaultFailurePort.class)) {
		case ErrorModelPackage.FAULT_FAILURE_PORT__FUNCTION_TARGET:
		case ErrorModelPackage.FAULT_FAILURE_PORT__HW_TARGET:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ErrorModelPackage.Literals.FAULT_FAILURE_PORT__FUNCTION_TARGET,
				_instanceRefFactory.eINSTANCE
						.createFaultFailurePort_functionTarget()));

		newChildDescriptors
				.add(createChildParameter(
						ErrorModelPackage.Literals.FAULT_FAILURE_PORT__HW_TARGET,
						_instanceRefFactory.eINSTANCE
								.createFaultFailurePort_hwTarget()));
	}

}
