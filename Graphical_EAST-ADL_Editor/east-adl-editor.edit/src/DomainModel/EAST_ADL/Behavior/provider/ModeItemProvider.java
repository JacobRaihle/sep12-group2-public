/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Behavior.provider;

import DomainModel.EAST_ADL.Behavior.BehaviorPackage;
import DomainModel.EAST_ADL.Behavior.Mode;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Behavior.Mode} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModeItemProvider extends EAElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addConditionPropertyDescriptor(object);
			addFunctionBehavior_modePropertyDescriptor(object);
			addFunctionTrigger_modePropertyDescriptor(object);
			addRequirement_modePropertyDescriptor(object);
			addTimingConstraint_modePropertyDescriptor(object);
			addHazardousEvent_operatingModePropertyDescriptor(object);
			addSafetyGoal_safeModesPropertyDescriptor(object);
			addGenericConstraint_modePropertyDescriptor(object);
			addBehaviorConstraint_constrainedModePropertyDescriptor(object);
			addState_representModePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_condition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_condition_feature", "_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__CONDITION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Behavior mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionBehavior_modePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_FunctionBehavior_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_FunctionBehavior_mode_feature",
						"_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__FUNCTION_BEHAVIOR_MODE, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Trigger mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionTrigger_modePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_FunctionTrigger_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_FunctionTrigger_mode_feature",
						"_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__FUNCTION_TRIGGER_MODE, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Requirement mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequirement_modePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_Requirement_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_Requirement_mode_feature", "_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__REQUIREMENT_MODE, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Timing Constraint mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTimingConstraint_modePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_TimingConstraint_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_TimingConstraint_mode_feature",
						"_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__TIMING_CONSTRAINT_MODE, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Hazardous Event operating Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHazardousEvent_operatingModePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_HazardousEvent_operatingMode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_HazardousEvent_operatingMode_feature",
						"_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__HAZARDOUS_EVENT_OPERATING_MODE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Safety Goal safe Modes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSafetyGoal_safeModesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_SafetyGoal_safeModes_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_SafetyGoal_safeModes_feature",
						"_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__SAFETY_GOAL_SAFE_MODES, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Generic Constraint mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGenericConstraint_modePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Mode_GenericConstraint_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Mode_GenericConstraint_mode_feature",
						"_UI_Mode_type"),
				BehaviorPackage.Literals.MODE__GENERIC_CONSTRAINT_MODE, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Behavior Constraint constrained Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBehaviorConstraint_constrainedModePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Mode_BehaviorConstraint_constrainedMode_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Mode_BehaviorConstraint_constrainedMode_feature",
								"_UI_Mode_type"),
						BehaviorPackage.Literals.MODE__BEHAVIOR_CONSTRAINT_CONSTRAINED_MODE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the State represent Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addState_representModePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Mode_State_representMode_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Mode_State_representMode_feature",
								"_UI_Mode_type"),
						BehaviorPackage.Literals.MODE__STATE_REPRESENT_MODE,
						true, false, true, null, null, null));
	}

	/**
	 * This returns Mode.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Mode"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Mode) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Mode_type")
				: getString("_UI_Mode_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Mode.class)) {
		case BehaviorPackage.MODE__CONDITION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
