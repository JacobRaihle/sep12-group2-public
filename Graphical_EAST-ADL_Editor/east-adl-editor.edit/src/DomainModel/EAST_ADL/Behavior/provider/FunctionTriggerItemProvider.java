/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Behavior.provider;

import DomainModel.EAST_ADL.Behavior.BehaviorPackage;
import DomainModel.EAST_ADL.Behavior.FunctionTrigger;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Behavior.FunctionTrigger} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionTriggerItemProvider extends EAElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionTriggerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTriggerConditionPropertyDescriptor(object);
			addTriggerPolicyPropertyDescriptor(object);
			addFunctionPropertyDescriptor(object);
			addModePropertyDescriptor(object);
			addFunctionPrototypePropertyDescriptor(object);
			addPortPropertyDescriptor(object);
			addBehaviorConstraint_constrainedFunctionTriggerPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Trigger Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTriggerConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionTrigger_triggerCondition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionTrigger_triggerCondition_feature",
						"_UI_FunctionTrigger_type"),
				BehaviorPackage.Literals.FUNCTION_TRIGGER__TRIGGER_CONDITION,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Trigger Policy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTriggerPolicyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionTrigger_triggerPolicy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionTrigger_triggerPolicy_feature",
						"_UI_FunctionTrigger_type"),
				BehaviorPackage.Literals.FUNCTION_TRIGGER__TRIGGER_POLICY,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Function feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionTrigger_function_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionTrigger_function_feature",
						"_UI_FunctionTrigger_type"),
				BehaviorPackage.Literals.FUNCTION_TRIGGER__FUNCTION, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionTrigger_mode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionTrigger_mode_feature",
						"_UI_FunctionTrigger_type"),
				BehaviorPackage.Literals.FUNCTION_TRIGGER__MODE, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionPrototypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionTrigger_functionPrototype_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionTrigger_functionPrototype_feature",
						"_UI_FunctionTrigger_type"),
				BehaviorPackage.Literals.FUNCTION_TRIGGER__FUNCTION_PROTOTYPE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPortPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionTrigger_port_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionTrigger_port_feature",
						"_UI_FunctionTrigger_type"),
				BehaviorPackage.Literals.FUNCTION_TRIGGER__PORT, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Behavior Constraint constrained Function Trigger feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBehaviorConstraint_constrainedFunctionTriggerPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionTrigger_BehaviorConstraint_constrainedFunctionTrigger_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionTrigger_BehaviorConstraint_constrainedFunctionTrigger_feature",
								"_UI_FunctionTrigger_type"),
						BehaviorPackage.Literals.FUNCTION_TRIGGER__BEHAVIOR_CONSTRAINT_CONSTRAINED_FUNCTION_TRIGGER,
						true, false, true, null, null, null));
	}

	/**
	 * This returns FunctionTrigger.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/FunctionTrigger"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionTrigger) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_FunctionTrigger_type")
				: getString("_UI_FunctionTrigger_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FunctionTrigger.class)) {
		case BehaviorPackage.FUNCTION_TRIGGER__TRIGGER_CONDITION:
		case BehaviorPackage.FUNCTION_TRIGGER__TRIGGER_POLICY:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
