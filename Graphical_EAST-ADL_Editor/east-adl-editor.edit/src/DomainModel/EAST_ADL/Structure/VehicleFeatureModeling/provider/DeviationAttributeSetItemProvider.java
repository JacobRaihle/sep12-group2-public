/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.provider;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.DeviationAttributeSet;
import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.DeviationAttributeSet} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DeviationAttributeSetItemProvider extends EAElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviationAttributeSetItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAllowChangeAttributePropertyDescriptor(object);
			addAllowChangeCardinalityPropertyDescriptor(object);
			addAllowChangeDescriptionPropertyDescriptor(object);
			addAllowChangeNamePropertyDescriptor(object);
			addAllowMovePropertyDescriptor(object);
			addAllowReductionPropertyDescriptor(object);
			addAllowRefinementPropertyDescriptor(object);
			addAllowRegroupingPropertyDescriptor(object);
			addAllowRemovalPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Allow Change Attribute feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowChangeAttributePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowChangeAttribute_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowChangeAttribute_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_ATTRIBUTE,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Change Cardinality feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowChangeCardinalityPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowChangeCardinality_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowChangeCardinality_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_CARDINALITY,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Change Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowChangeDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowChangeDescription_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowChangeDescription_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_DESCRIPTION,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Change Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowChangeNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowChangeName_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowChangeName_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_NAME,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Move feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowMovePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowMove_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowMove_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_MOVE,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Reduction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowReductionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowReduction_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowReduction_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_REDUCTION,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Refinement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowRefinementPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowRefinement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowRefinement_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_REFINEMENT,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Regrouping feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowRegroupingPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowRegrouping_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowRegrouping_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_REGROUPING,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Allow Removal feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllowRemovalPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DeviationAttributeSet_allowRemoval_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DeviationAttributeSet_allowRemoval_feature",
								"_UI_DeviationAttributeSet_type"),
						VehicleFeatureModelingPackage.Literals.DEVIATION_ATTRIBUTE_SET__ALLOW_REMOVAL,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns DeviationAttributeSet.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/DeviationAttributeSet"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DeviationAttributeSet) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_DeviationAttributeSet_type")
				: getString("_UI_DeviationAttributeSet_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DeviationAttributeSet.class)) {
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_ATTRIBUTE:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_CARDINALITY:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_DESCRIPTION:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_CHANGE_NAME:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_MOVE:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_REDUCTION:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_REFINEMENT:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_REGROUPING:
		case VehicleFeatureModelingPackage.DEVIATION_ATTRIBUTE_SET__ALLOW_REMOVAL:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
