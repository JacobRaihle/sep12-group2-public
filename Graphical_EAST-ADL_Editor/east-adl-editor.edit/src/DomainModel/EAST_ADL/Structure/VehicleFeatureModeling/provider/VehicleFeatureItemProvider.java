/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureModelingPackage;

import DomainModel.EAST_ADL.Structure.FeatureModeling.provider.FeatureItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeature;
import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingFactory;
import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeature} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VehicleFeatureItemProvider extends FeatureItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VehicleFeatureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIsCustomerVisiblePropertyDescriptor(object);
			addIsDesignVariabilityRationalePropertyDescriptor(object);
			addIsRemovedPropertyDescriptor(object);
			addItem_vehicleFeaturePropertyDescriptor(object);
			addBehaviorConstraint_relatedVehicleFeaturePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Is Customer Visible feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsCustomerVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VehicleFeature_isCustomerVisible_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VehicleFeature_isCustomerVisible_feature",
								"_UI_VehicleFeature_type"),
						VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__IS_CUSTOMER_VISIBLE,
						true, false, false,
						ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Is Design Variability Rationale feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsDesignVariabilityRationalePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VehicleFeature_isDesignVariabilityRationale_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VehicleFeature_isDesignVariabilityRationale_feature",
								"_UI_VehicleFeature_type"),
						VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__IS_DESIGN_VARIABILITY_RATIONALE,
						true, false, false,
						ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Is Removed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsRemovedPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VehicleFeature_isRemoved_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VehicleFeature_isRemoved_feature",
								"_UI_VehicleFeature_type"),
						VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__IS_REMOVED,
						true, false, false,
						ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Item vehicle Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addItem_vehicleFeaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VehicleFeature_Item_vehicleFeature_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VehicleFeature_Item_vehicleFeature_feature",
								"_UI_VehicleFeature_type"),
						VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__ITEM_VEHICLE_FEATURE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Behavior Constraint related Vehicle Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBehaviorConstraint_relatedVehicleFeaturePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VehicleFeature_BehaviorConstraint_relatedVehicleFeature_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VehicleFeature_BehaviorConstraint_relatedVehicleFeature_feature",
								"_UI_VehicleFeature_type"),
						VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__BEHAVIOR_CONSTRAINT_RELATED_VEHICLE_FEATURE,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__DEVIATION_ATTRIBUTE_SET);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns VehicleFeature.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/VehicleFeature"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((VehicleFeature) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_VehicleFeature_type")
				: getString("_UI_VehicleFeature_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(VehicleFeature.class)) {
		case VehicleFeatureModelingPackage.VEHICLE_FEATURE__IS_CUSTOMER_VISIBLE:
		case VehicleFeatureModelingPackage.VEHICLE_FEATURE__IS_DESIGN_VARIABILITY_RATIONALE:
		case VehicleFeatureModelingPackage.VEHICLE_FEATURE__IS_REMOVED:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case VehicleFeatureModelingPackage.VEHICLE_FEATURE__DEVIATION_ATTRIBUTE_SET:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						VehicleFeatureModelingPackage.Literals.VEHICLE_FEATURE__DEVIATION_ATTRIBUTE_SET,
						VehicleFeatureModelingFactory.eINSTANCE
								.createDeviationAttributeSet()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE
				|| childFeature == FeatureModelingPackage.Literals.FEATURE__CHILD_NODE
				|| childFeature == FeatureModelingPackage.Literals.FEATURE__FEATURE_PARAMETER
				|| childFeature == FeatureModelingPackage.Literals.FEATURE__REQUIRED_BINDING_TIME
				|| childFeature == FeatureModelingPackage.Literals.FEATURE__ACTUAL_BINDING_TIME;

		if (qualify) {
			return getString("_UI_CreateChild_text2", new Object[] {
					getTypeText(childObject), getFeatureText(childFeature),
					getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
