/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.provider;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;

import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.HardwareConnector_port;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef._instanceRefPackage;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.jface.resource.ColorDescriptor;

import org.eclipse.swt.graphics.RGB;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.HardwareConnector_port} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HardwareConnector_portItemProvider extends ItemProviderAdapter
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HardwareConnector_portItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addHardwareComponentPrototypePropertyDescriptor(object);
			addHardwarePinPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Hardware Component Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwareComponentPrototypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareConnector_port_hardwareComponentPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareConnector_port_hardwareComponentPrototype_feature",
								"_UI_HardwareConnector_port_type"),
						_instanceRefPackage.Literals.HARDWARE_CONNECTOR_PORT__HARDWARE_COMPONENT_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Hardware Pin feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwarePinPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareConnector_port_hardwarePin_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareConnector_port_hardwarePin_feature",
								"_UI_HardwareConnector_port_type"),
						_instanceRefPackage.Literals.HARDWARE_CONNECTOR_PORT__HARDWARE_PIN,
						true, false, true, null, null, null));
	}

	/**
	 * This returns HardwareConnector_port.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/HardwareConnector_port"));
	}

	/**
	 * Returns false if the port is not properly connected
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isConnectorSet(Object object) {

		HardwareConnector_port connectionPort = (HardwareConnector_port) object;
		return connectionPort.getHardwarePin() != null;

	}

	/**
	 * Controls the color of the label displayed in the tree view
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getForeground(Object object) {
		if (!isConnectorSet(object))
			return ColorDescriptor.createFrom(new RGB(255, 0, 0));
		return null;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		HardwareConnector_port connectionPort = (HardwareConnector_port) object;
		String label = "";

		HardwareComponentPrototype prototype = connectionPort
				.getHardwareComponentPrototype();

		HardwarePin port = connectionPort.getHardwarePin();
		if (port == null) {
			label = "Warning: Port not set";
		} else {
			String portLabel = port.getShortName();
			String prototypeLabel = prototype != null ? prototype
					.getShortName() + "." : "this.";
			label = prototypeLabel + portLabel;
		}
		return label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		fireNotifyChanged(new ViewerNotification(notification,
				notification.getNotifier(), false, true));
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
