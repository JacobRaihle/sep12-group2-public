/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentType;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingPackage;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HardwareComponentPrototypeItemProvider extends
		AllocationTargetItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource, IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HardwareComponentPrototypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpContextElementPropertyDescriptor(object);
			addAtpInstanceRef_atpTargetPropertyDescriptor(object);
			addAtpTypePropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addHardwareConnector_port_hardwareComponentPrototypePropertyDescriptor(object);
			addErrorModelPrototype_hwTarget_hardwareComponentPrototype_contextPropertyDescriptor(object);
			addErrorModelPrototype_hwTarget_hardwareComponentPrototypePropertyDescriptor(object);
			addFaultFailurePort_hwTarget_hardwareComponentPrototypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Context Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpContextElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_CONTEXT_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpTarget_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AtpPrototype_atpType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AtpPrototype_atpType_feature",
						"_UI_AtpPrototype_type"),
				AbstractStructurePackage.Literals.ATP_PROTOTYPE__ATP_TYPE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentPrototype_type_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentPrototype_type_feature",
								"_UI_HardwareComponentPrototype_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Hardware Connector port hardware Component Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwareConnector_port_hardwareComponentPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentPrototype_HardwareConnector_port_hardwareComponentPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentPrototype_HardwareConnector_port_hardwareComponentPrototype_feature",
								"_UI_HardwareComponentPrototype_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__HARDWARE_CONNECTOR_PORT_HARDWARE_COMPONENT_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Prototype hw Target hardware Component Prototype context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelPrototype_hwTarget_hardwareComponentPrototype_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentPrototype_ErrorModelPrototype_hwTarget_hardwareComponentPrototype_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentPrototype_ErrorModelPrototype_hwTarget_hardwareComponentPrototype_context_feature",
								"_UI_HardwareComponentPrototype_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__ERROR_MODEL_PROTOTYPE_HW_TARGET_HARDWARE_COMPONENT_PROTOTYPE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Prototype hw Target hardware Component Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelPrototype_hwTarget_hardwareComponentPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentPrototype_ErrorModelPrototype_hwTarget_hardwareComponentPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentPrototype_ErrorModelPrototype_hwTarget_hardwareComponentPrototype_feature",
								"_UI_HardwareComponentPrototype_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__ERROR_MODEL_PROTOTYPE_HW_TARGET_HARDWARE_COMPONENT_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Fault Failure Port hw Target hardware Component Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailurePort_hwTarget_hardwareComponentPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentPrototype_FaultFailurePort_hwTarget_hardwareComponentPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentPrototype_FaultFailurePort_hwTarget_hardwareComponentPrototype_feature",
								"_UI_HardwareComponentPrototype_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__FAULT_FAILURE_PORT_HW_TARGET_HARDWARE_COMPONENT_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * Overrides the getChildren method of function and hardware prototypes
	 * to include a virtual copy of its type and that type's ports.
	 * @generated
	 */

	@Override
	public Collection<?> getChildren(Object object) {
		Collection children = (Collection) super.getChildren(object);
		HardwareComponentPrototype proto = (HardwareComponentPrototype) object;
		HardwareComponentType type = proto.getType();
		if (type != null) {
			children.add(type);
			for (HardwarePin port : type.getPort()) {
				children.add(port);
			}
		}
		return children;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__DERIVED_PIN);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__DERIVED_PART);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns HardwareComponentPrototype.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/HardwareComponentPrototype"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((HardwareComponentPrototype) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_HardwareComponentPrototype_type")
				: getString("_UI_HardwareComponentPrototype_type") + " "
						+ label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(HardwareComponentPrototype.class)) {
		case HardwareModelingPackage.HARDWARE_COMPONENT_PROTOTYPE__DERIVED_PIN:
		case HardwareModelingPackage.HARDWARE_COMPONENT_PROTOTYPE__DERIVED_PART:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__DERIVED_PIN,
						HardwareModelingFactory.eINSTANCE
								.createHardwarePinProxy()));

		newChildDescriptors
				.add(createChildParameter(
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_PROTOTYPE__DERIVED_PART,
						HardwareModelingFactory.eINSTANCE
								.createHardwareComponentPrototype()));
	}

}
