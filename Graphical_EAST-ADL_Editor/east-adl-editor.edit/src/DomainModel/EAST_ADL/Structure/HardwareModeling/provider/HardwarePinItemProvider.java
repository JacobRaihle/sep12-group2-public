/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.AUTOSAR.SystemTemplate.SystemTemplateFactory;

import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelFactory;

import DomainModel.EAST_ADL.Environment.EnvironmentFactory;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.DatatypesFactory;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureModelingFactory;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingPackage;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;

import DomainModel.EAST_ADL.Structure.SystemModeling.SystemModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HardwarePinItemProvider extends EAElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HardwarePinItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpContextElementPropertyDescriptor(object);
			addAtpInstanceRef_atpTargetPropertyDescriptor(object);
			addAtpInstanceRef_atpBasePropertyDescriptor(object);
			addDirectionPropertyDescriptor(object);
			addImpedancePropertyDescriptor(object);
			addIsGroundPropertyDescriptor(object);
			addPowerPropertyDescriptor(object);
			addVoltagePropertyDescriptor(object);
			addHardwarePinGroup_portPropertyDescriptor(object);
			addHardwareConnector_port_hardwarePinPropertyDescriptor(object);
			addFaultFailurePort_hwTarget_hardwarePortPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Context Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpContextElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_CONTEXT_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpTarget_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpClassifier_AtpInstanceRef_atpBase_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpClassifier_AtpInstanceRef_atpBase_feature",
								"_UI_AtpClassifier_type"),
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_INSTANCE_REF_ATP_BASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Direction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_HardwarePin_direction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_HardwarePin_direction_feature",
						"_UI_HardwarePin_type"),
				HardwareModelingPackage.Literals.HARDWARE_PIN__DIRECTION, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Impedance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addImpedancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_HardwarePin_impedance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_HardwarePin_impedance_feature",
						"_UI_HardwarePin_type"),
				HardwareModelingPackage.Literals.HARDWARE_PIN__IMPEDANCE, true,
				false, false, ItemPropertyDescriptor.REAL_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Is Ground feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsGroundPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_HardwarePin_isGround_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_HardwarePin_isGround_feature",
						"_UI_HardwarePin_type"),
				HardwareModelingPackage.Literals.HARDWARE_PIN__IS_GROUND, true,
				false, false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Power feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPowerPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwarePin_power_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_HardwarePin_power_feature",
								"_UI_HardwarePin_type"),
						HardwareModelingPackage.Literals.HARDWARE_PIN__POWER,
						true, false, false,
						ItemPropertyDescriptor.REAL_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Voltage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVoltagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_HardwarePin_voltage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_HardwarePin_voltage_feature",
						"_UI_HardwarePin_type"),
				HardwareModelingPackage.Literals.HARDWARE_PIN__VOLTAGE, true,
				false, false, ItemPropertyDescriptor.REAL_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Hardware Pin Group port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwarePinGroup_portPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwarePin_HardwarePinGroup_port_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwarePin_HardwarePinGroup_port_feature",
								"_UI_HardwarePin_type"),
						HardwareModelingPackage.Literals.HARDWARE_PIN__HARDWARE_PIN_GROUP_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Hardware Connector port hardware Pin feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwareConnector_port_hardwarePinPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwarePin_HardwareConnector_port_hardwarePin_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwarePin_HardwareConnector_port_hardwarePin_feature",
								"_UI_HardwarePin_type"),
						HardwareModelingPackage.Literals.HARDWARE_PIN__HARDWARE_CONNECTOR_PORT_HARDWARE_PIN,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Fault Failure Port hw Target hardware Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailurePort_hwTarget_hardwarePortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwarePin_FaultFailurePort_hwTarget_hardwarePort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwarePin_FaultFailurePort_hwTarget_hardwarePort_feature",
								"_UI_HardwarePin_type"),
						HardwareModelingPackage.Literals.HARDWARE_PIN__FAULT_FAILURE_PORT_HW_TARGET_HARDWARE_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((HardwarePin) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_HardwarePin_type")
				: getString("_UI_HardwarePin_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(HardwarePin.class)) {
		case HardwareModelingPackage.HARDWARE_PIN__DIRECTION:
		case HardwareModelingPackage.HARDWARE_PIN__IMPEDANCE:
		case HardwareModelingPackage.HARDWARE_PIN__IS_GROUND:
		case HardwareModelingPackage.HARDWARE_PIN__POWER:
		case HardwareModelingPackage.HARDWARE_PIN__VOLTAGE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case HardwareModelingPackage.HARDWARE_PIN__ATP_FEATURE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createHardwareComponentPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwareConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createLogicalBus()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerOutHardwarePin()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						HardwareModelingFactory.eINSTANCE
								.createPowerInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwarePinProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createVehicleLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createSystemModel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createAnalysisLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createDesignLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createImplementationLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeatureModel()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						VehicleFeatureModelingFactory.eINSTANCE
								.createVehicleFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createAnalysisFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createDesignFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPowerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionInFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionOutFlowPort()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						FunctionModelingFactory.eINSTANCE
								.createFunctionInOutFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionClientPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionServerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPortProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				EnvironmentFactory.eINSTANCE.createClampConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createErrorModelPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFailureOutPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFaultInPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createInternalFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createProcessFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				DatatypesFactory.eINSTANCE.createEADatatypePrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemTemplateFactory.eINSTANCE.createSystem()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
