/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.AUTOSAR.SystemTemplate.SystemTemplateFactory;

import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelFactory;

import DomainModel.EAST_ADL.Environment.EnvironmentFactory;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.DatatypesFactory;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.ContextItemProvider;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureModelingFactory;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentType;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareConnector;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingPackage;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;
import DomainModel.EAST_ADL.Structure.SystemModeling.SystemModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import se.chalmers.ieee.eastadleditor.ConnectionList;
import se.chalmers.ieee.eastadleditor.PortList;
import se.chalmers.ieee.eastadleditor.PrototypeList;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HardwareComponentTypeItemProvider extends ContextItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {

	/**
	 * @generated
	 */
	private static HashMap<Object, ConnectionList> connectionLists = new HashMap<Object, ConnectionList>();
	/**
	 * @generated
	 */
	private static HashMap<Object, PrototypeList> partLists = new HashMap<Object, PrototypeList>();
	/**
	 * @generated
	 */
	private static HashMap<Object, PortList> portLists = new HashMap<Object, PortList>();

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HardwareComponentTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpBasePropertyDescriptor(object);
			addAtpPrototype_atpTypePropertyDescriptor(object);
			addHardwareFunctionType_hardwareComponentPropertyDescriptor(object);
			addHardwareComponentPrototype_typePropertyDescriptor(object);
			addErrorModelType_hwTargetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpClassifier_AtpInstanceRef_atpBase_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpClassifier_AtpInstanceRef_atpBase_feature",
								"_UI_AtpClassifier_type"),
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_INSTANCE_REF_ATP_BASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Prototype atp Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpPrototype_atpTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpType_AtpPrototype_atpType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_AtpType_AtpPrototype_atpType_feature",
								"_UI_AtpType_type"),
						AbstractStructurePackage.Literals.ATP_TYPE__ATP_PROTOTYPE_ATP_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Hardware Function Type hardware Component feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwareFunctionType_hardwareComponentPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentType_HardwareFunctionType_hardwareComponent_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentType_HardwareFunctionType_hardwareComponent_feature",
								"_UI_HardwareComponentType_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__HARDWARE_FUNCTION_TYPE_HARDWARE_COMPONENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Hardware Component Prototype type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHardwareComponentPrototype_typePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentType_HardwareComponentPrototype_type_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentType_HardwareComponentPrototype_type_feature",
								"_UI_HardwareComponentType_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__HARDWARE_COMPONENT_PROTOTYPE_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Type hw Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelType_hwTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_HardwareComponentType_ErrorModelType_hwTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_HardwareComponentType_ErrorModelType_hwTarget_feature",
								"_UI_HardwareComponentType_type"),
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__ERROR_MODEL_TYPE_HW_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__CONNECTOR);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PART);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT_GROUP);
			childrenFeatures
					.add(HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__BUS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * @generated
	 */
	@Override
	public Collection<?> getChildren(Object object) {
		HardwareComponentType function = (HardwareComponentType) object;
		Collection children = super.getChildren(object);
		Collection<HardwareConnector> connectors = function.getConnector();
		children.removeAll(connectors);
		Collection<HardwareComponentPrototype> parts = function.getPart();
		children.removeAll(parts);
		Collection<HardwarePin> ports = function.getPort();
		children.removeAll(ports);
		children.add(getPortList(function));
		children.add(getPartList(function));
		children.add(getConnectionList(function));
		return children;
	}

	/**
	 * @generated
	 */
	public ConnectionList getConnectionList(HardwareComponentType function) {
		if (!connectionLists.containsKey(function)) {
			connectionLists.put(function, new ConnectionList(adapterFactory,
					function));
		}
		return connectionLists.get(function);
	}

	/**
	 * @generated
	 */
	public PortList getPortList(HardwareComponentType function) {
		if (!portLists.containsKey(function)) {
			portLists.put(function, new PortList(adapterFactory, function));
		}
		return portLists.get(function);
	}

	/**
	 * @generated
	 */
	public PrototypeList getPartList(HardwareComponentType function) {
		if (!partLists.containsKey(function)) {
			partLists
					.put(function, new PrototypeList(adapterFactory, function));
		}
		return partLists.get(function);
	}

	/**
	 * This returns HardwareComponentType.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/HardwareComponentType"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((HardwareComponentType) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_HardwareComponentType_type")
				: getString("_UI_HardwareComponentType_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(HardwareComponentType.class)) {
		case HardwareModelingPackage.HARDWARE_COMPONENT_TYPE__ATP_FEATURE:
		case HardwareModelingPackage.HARDWARE_COMPONENT_TYPE__CONNECTOR:
		case HardwareModelingPackage.HARDWARE_COMPONENT_TYPE__PORT:
		case HardwareModelingPackage.HARDWARE_COMPONENT_TYPE__PART:
		case HardwareModelingPackage.HARDWARE_COMPONENT_TYPE__PORT_GROUP:
		case HardwareModelingPackage.HARDWARE_COMPONENT_TYPE__BUS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createHardwareComponentPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwareConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createLogicalBus()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerOutHardwarePin()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						HardwareModelingFactory.eINSTANCE
								.createPowerInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwarePinProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createVehicleLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createSystemModel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createAnalysisLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createDesignLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createImplementationLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeatureModel()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						VehicleFeatureModelingFactory.eINSTANCE
								.createVehicleFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createAnalysisFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createDesignFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPowerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionInFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionOutFlowPort()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						FunctionModelingFactory.eINSTANCE
								.createFunctionInOutFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionClientPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionServerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPortProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				EnvironmentFactory.eINSTANCE.createClampConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createErrorModelPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFailureOutPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFaultInPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createInternalFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createProcessFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				DatatypesFactory.eINSTANCE.createEADatatypePrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemTemplateFactory.eINSTANCE.createSystem()));

		newChildDescriptors
				.add(createChildParameter(
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__CONNECTOR,
						HardwareModelingFactory.eINSTANCE
								.createHardwareConnector()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE.createPowerInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE.createPowerOutHardwarePin()));

		newChildDescriptors
				.add(createChildParameter(
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
						HardwareModelingFactory.eINSTANCE
								.createPowerInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE.createIOInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE.createIOOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE.createIOInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT,
				HardwareModelingFactory.eINSTANCE.createHardwarePinProxy()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PART,
				HardwareModelingFactory.eINSTANCE
						.createHardwareComponentPrototype()));

		newChildDescriptors
				.add(createChildParameter(
						HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT_GROUP,
						HardwareModelingFactory.eINSTANCE
								.createHardwarePinGroup()));

		newChildDescriptors.add(createChildParameter(
				HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__BUS,
				HardwareModelingFactory.eINSTANCE.createLogicalBus()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE
				|| childFeature == HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PART
				|| childFeature == HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__CONNECTOR
				|| childFeature == HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__BUS
				|| childFeature == HardwareModelingPackage.Literals.HARDWARE_COMPONENT_TYPE__PORT;

		if (qualify) {
			return getString("_UI_CreateChild_text2", new Object[] {
					getTypeText(childObject), getFeatureText(childFeature),
					getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
