package DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.provider;

import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.FunctionConnector_port;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef._instanceRefPackage;
import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.swt.graphics.RGB;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.FunctionConnector_port} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionConnector_portItemProvider extends ItemProviderAdapter
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionConnector_portItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFunctionPrototypePropertyDescriptor(object);
			addFunctionPortPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionPrototypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionConnector_port_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionConnector_port_functionPrototype_feature",
								"_UI_FunctionConnector_port_type"),
						_instanceRefPackage.Literals.FUNCTION_CONNECTOR_PORT__FUNCTION_PROTOTYPE,
						true, false, true, null, null, null)

				{
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<Object> choiceOfValues = new ArrayList<Object>();

						FunctionConnector_port base = (FunctionConnector_port) object;
						FunctionType grandParent = base
								.getFunctionConnector_port()
								.getFunctionType_connector();
						FunctionPort port = base.getFunctionPort();

						if (grandParent instanceof AnalysisFunctionType) {
							AnalysisFunctionType aft = (AnalysisFunctionType) grandParent;
							if (port != null) {
								ArrayList<AnalysisFunctionPrototype> listOfPrototypes = new ArrayList<AnalysisFunctionPrototype>();
								listOfPrototypes.addAll(aft.getPart());
								for (AnalysisFunctionPrototype iprototype : listOfPrototypes) {
									if (iprototype.getType().getPort()
											.contains(port)) {
										choiceOfValues.add(iprototype);
									}
								}
							} else {
								choiceOfValues.addAll(aft.getPart());
							}
						} else if (grandParent instanceof DesignFunctionType) {
							choiceOfValues
									.addAll(((DesignFunctionType) grandParent)
											.getPart());
						}

						//Add the empty choice (this is the 'remove' option and initially chosen)
						choiceOfValues.add(null);

						return choiceOfValues;
					}
				}

				);
	}

	/**
	 * This adds a property descriptor for the Function Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionPortPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionConnector_port_functionPort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionConnector_port_functionPort_feature",
								"_UI_FunctionConnector_port_type"),
						_instanceRefPackage.Literals.FUNCTION_CONNECTOR_PORT__FUNCTION_PORT,
						true, false, true, null, null, null)

				{
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<Object> newList = new ArrayList<Object>();
						FunctionConnector_port base = (FunctionConnector_port) object;
						FunctionPrototype uncastPrototype = base
								.getFunctionPrototype();
						FunctionType grandParent = base
								.getFunctionConnector_port()
								.getFunctionType_connector();

						if (base.getFunctionPrototype() == null) {
							newList.addAll(getFtPorts(grandParent));

						} else if (uncastPrototype instanceof AnalysisFunctionPrototype) {
							newList.addAll(getAfpPorts((AnalysisFunctionPrototype) uncastPrototype));

						} else if (uncastPrototype instanceof DesignFunctionPrototype) {
							newList.addAll(getDfpPorts((DesignFunctionPrototype) uncastPrototype));
						}

						//Add the empty choice (this is the 'remove' option)
						newList.add(null);

						return newList;
					}
				}

				);
	}

	private List<Object> getFtPorts(FunctionType ft) {
		List<Object> list = new ArrayList<Object>();
		if (ft instanceof AnalysisFunctionType) {
			AnalysisFunctionType aft = (AnalysisFunctionType) ft;
			ArrayList<AnalysisFunctionPrototype> prototypeList = new ArrayList<AnalysisFunctionPrototype>();
			prototypeList.addAll(aft.getPart());
			for (AnalysisFunctionPrototype afp : prototypeList) {
				list.addAll(afp.getType().getPort());
			}
		} else if (ft instanceof DesignFunctionType) {
			DesignFunctionType dft = (DesignFunctionType) ft;
			ArrayList<DesignFunctionPrototype> prototypeList = new ArrayList<DesignFunctionPrototype>();
			prototypeList.addAll(dft.getPart());
			for (DesignFunctionPrototype dfp : prototypeList) {
				list.addAll(dfp.getType().getPort());
			}
		}
		list.addAll(ft.getPort());
		return list;
	}

	private List<Object> getAfpPorts(AnalysisFunctionPrototype afp) {
		List<Object> list = new ArrayList<Object>();
		list.addAll(afp.getType().getPort());
		return list;
	}

	private List<Object> getDfpPorts(DesignFunctionPrototype dfp) {
		List<Object> list = new ArrayList<Object>();
		list.addAll(dfp.getType().getPort());
		return list;
	}

	/**
	 * This returns FunctionConnector_port.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/FunctionConnector_port"));
	}

	/**
	 * Returns false if the port is not properly connected
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isConnectorSet(Object object) {

		FunctionConnector_port connectionPort = (FunctionConnector_port) object;
		return connectionPort.getFunctionPort() != null;

	}

	/**
	 * Controls the color of the label displayed in the tree view
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getForeground(Object object) {
		if (!isConnectorSet(object))
			return ColorDescriptor.createFrom(new RGB(255, 0, 0));
		return null;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		FunctionConnector_port connectionPort = (FunctionConnector_port) object;
		String label = "";

		FunctionPrototype prototype = connectionPort.getFunctionPrototype();

		FunctionPort port = connectionPort.getFunctionPort();
		if (port == null) {
			label = "Warning: Port not set";
		} else {
			String portLabel = port.getShortName();
			String prototypeLabel = prototype != null ? prototype
					.getShortName() + "." : "this.";
			label = prototypeLabel + portLabel;
		}
		return label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		fireNotifyChanged(new ViewerNotification(notification,
				notification.getNotifier(), false, true));
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}