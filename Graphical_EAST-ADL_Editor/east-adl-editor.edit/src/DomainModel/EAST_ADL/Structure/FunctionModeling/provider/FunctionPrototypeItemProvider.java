/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionPrototypeItemProvider extends EAElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPrototypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpContextElementPropertyDescriptor(object);
			addAtpInstanceRef_atpTargetPropertyDescriptor(object);
			addAtpTypePropertyDescriptor(object);
			addFunctionConnector_port_functionPrototypePropertyDescriptor(object);
			addFunctionTrigger_functionPrototypePropertyDescriptor(object);
			addClampConnector_port_functionPrototypePropertyDescriptor(object);
			addPrecedenceConstraint_preceding_functionPrototype_targetPropertyDescriptor(object);
			addPrecedenceConstraint_preceding_functionPrototype_contextPropertyDescriptor(object);
			addPrecedenceConstraint_successive_functionPrototype_contextPropertyDescriptor(object);
			addPrecedenceConstraint_successive_functionPrototype_targetPropertyDescriptor(object);
			addEventFunction_function_functionPrototype_contextPropertyDescriptor(object);
			addEventFunction_function_functionPrototype_targetPropertyDescriptor(object);
			addEventFunctionClientServerPort_port_functionPrototypePropertyDescriptor(object);
			addEventFunctionFlowPort_port_functionPrototypePropertyDescriptor(object);
			addErrorModelPrototype_functionTarget_functionPrototypePropertyDescriptor(object);
			addErrorModelPrototype_functionTarget_functionPrototype_contextPropertyDescriptor(object);
			addFaultFailurePort_functionTarget_functionPrototypePropertyDescriptor(object);
			addDerivedFromPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Context Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpContextElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_CONTEXT_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpTarget_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AtpPrototype_atpType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AtpPrototype_atpType_feature",
						"_UI_AtpPrototype_type"),
				AbstractStructurePackage.Literals.ATP_PROTOTYPE__ATP_TYPE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Connector port function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionConnector_port_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_FunctionConnector_port_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_FunctionConnector_port_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__FUNCTION_CONNECTOR_PORT_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Trigger function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionTrigger_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_FunctionTrigger_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_FunctionTrigger_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__FUNCTION_TRIGGER_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Clamp Connector port function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClampConnector_port_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_ClampConnector_port_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_ClampConnector_port_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__CLAMP_CONNECTOR_PORT_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Precedence Constraint preceding function Prototype target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrecedenceConstraint_preceding_functionPrototype_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_PrecedenceConstraint_preceding_functionPrototype_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_PrecedenceConstraint_preceding_functionPrototype_target_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__PRECEDENCE_CONSTRAINT_PRECEDING_FUNCTION_PROTOTYPE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Precedence Constraint preceding function Prototype context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrecedenceConstraint_preceding_functionPrototype_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_PrecedenceConstraint_preceding_functionPrototype_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_PrecedenceConstraint_preceding_functionPrototype_context_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__PRECEDENCE_CONSTRAINT_PRECEDING_FUNCTION_PROTOTYPE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Precedence Constraint successive function Prototype context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrecedenceConstraint_successive_functionPrototype_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_PrecedenceConstraint_successive_functionPrototype_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_PrecedenceConstraint_successive_functionPrototype_context_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__PRECEDENCE_CONSTRAINT_SUCCESSIVE_FUNCTION_PROTOTYPE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Precedence Constraint successive function Prototype target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrecedenceConstraint_successive_functionPrototype_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_PrecedenceConstraint_successive_functionPrototype_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_PrecedenceConstraint_successive_functionPrototype_target_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__PRECEDENCE_CONSTRAINT_SUCCESSIVE_FUNCTION_PROTOTYPE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Function function function Prototype context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventFunction_function_functionPrototype_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_EventFunction_function_functionPrototype_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_EventFunction_function_functionPrototype_context_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__EVENT_FUNCTION_FUNCTION_FUNCTION_PROTOTYPE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Function function function Prototype target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventFunction_function_functionPrototype_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_EventFunction_function_functionPrototype_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_EventFunction_function_functionPrototype_target_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__EVENT_FUNCTION_FUNCTION_FUNCTION_PROTOTYPE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Function Client Server Port port function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventFunctionClientServerPort_port_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_EventFunctionClientServerPort_port_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_EventFunctionClientServerPort_port_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__EVENT_FUNCTION_CLIENT_SERVER_PORT_PORT_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Function Flow Port port function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventFunctionFlowPort_port_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_EventFunctionFlowPort_port_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_EventFunctionFlowPort_port_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__EVENT_FUNCTION_FLOW_PORT_PORT_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Prototype function Target function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelPrototype_functionTarget_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_ErrorModelPrototype_functionTarget_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_ErrorModelPrototype_functionTarget_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__ERROR_MODEL_PROTOTYPE_FUNCTION_TARGET_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Prototype function Target function Prototype context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelPrototype_functionTarget_functionPrototype_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_ErrorModelPrototype_functionTarget_functionPrototype_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_ErrorModelPrototype_functionTarget_functionPrototype_context_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__ERROR_MODEL_PROTOTYPE_FUNCTION_TARGET_FUNCTION_PROTOTYPE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Fault Failure Port function Target function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailurePort_functionTarget_functionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_FaultFailurePort_functionTarget_functionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_FaultFailurePort_functionTarget_functionPrototype_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__FAULT_FAILURE_PORT_FUNCTION_TARGET_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Derived From feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedFromPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPrototype_derivedFrom_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_FunctionPrototype_derivedFrom_feature",
								"_UI_FunctionPrototype_type"),
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__DERIVED_FROM,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__DERIVED_PORTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionPrototype) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_FunctionPrototype_type")
				: getString("_UI_FunctionPrototype_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FunctionPrototype.class)) {
		case FunctionModelingPackage.FUNCTION_PROTOTYPE__DERIVED_PORTS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						FunctionModelingPackage.Literals.FUNCTION_PROTOTYPE__DERIVED_PORTS,
						FunctionModelingFactory.eINSTANCE
								.createFunctionPortProxy()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
