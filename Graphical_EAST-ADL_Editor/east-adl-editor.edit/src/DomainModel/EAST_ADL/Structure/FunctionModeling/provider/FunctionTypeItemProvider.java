/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.AUTOSAR.SystemTemplate.SystemTemplateFactory;

import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelFactory;

import DomainModel.EAST_ADL.Environment.EnvironmentFactory;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.DatatypesFactory;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.ContextItemProvider;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureModelingFactory;

import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.SystemModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import se.chalmers.ieee.eastadleditor.ConnectionList;
import se.chalmers.ieee.eastadleditor.PortList;
import se.chalmers.ieee.eastadleditor.PrototypeList;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionTypeItemProvider extends ContextItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	private static Map<Object, ConnectionList> connectionLists = new HashMap<Object, ConnectionList>();
	private static Map<Object, PrototypeList> partLists = new HashMap<Object, PrototypeList>();
	private Map<Object, PortList> portLists = new HashMap<Object, PortList>();

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpBasePropertyDescriptor(object);
			addAtpPrototype_atpTypePropertyDescriptor(object);
			addIsElementaryPropertyDescriptor(object);
			addFunctionBehavior_functionPropertyDescriptor(object);
			addFunctionTrigger_functionPropertyDescriptor(object);
			addEventFunction_functionTypePropertyDescriptor(object);
			addErrorModelType_targetPropertyDescriptor(object);
			addParameter_targetFunctionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpClassifier_AtpInstanceRef_atpBase_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpClassifier_AtpInstanceRef_atpBase_feature",
								"_UI_AtpClassifier_type"),
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_INSTANCE_REF_ATP_BASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Prototype atp Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpPrototype_atpTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpType_AtpPrototype_atpType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_AtpType_AtpPrototype_atpType_feature",
								"_UI_AtpType_type"),
						AbstractStructurePackage.Literals.ATP_TYPE__ATP_PROTOTYPE_ATP_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Is Elementary feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsElementaryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_FunctionType_isElementary_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_FunctionType_isElementary_feature",
						"_UI_FunctionType_type"),
				FunctionModelingPackage.Literals.FUNCTION_TYPE__IS_ELEMENTARY,
				true, false, false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Function Behavior function feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionBehavior_functionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionType_FunctionBehavior_function_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionType_FunctionBehavior_function_feature",
								"_UI_FunctionType_type"),
						FunctionModelingPackage.Literals.FUNCTION_TYPE__FUNCTION_BEHAVIOR_FUNCTION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Trigger function feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionTrigger_functionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionType_FunctionTrigger_function_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionType_FunctionTrigger_function_feature",
								"_UI_FunctionType_type"),
						FunctionModelingPackage.Literals.FUNCTION_TYPE__FUNCTION_TRIGGER_FUNCTION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Function function Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventFunction_functionTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionType_EventFunction_functionType_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionType_EventFunction_functionType_feature",
								"_UI_FunctionType_type"),
						FunctionModelingPackage.Literals.FUNCTION_TYPE__EVENT_FUNCTION_FUNCTION_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Type target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelType_targetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionType_ErrorModelType_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionType_ErrorModelType_target_feature",
								"_UI_FunctionType_type"),
						FunctionModelingPackage.Literals.FUNCTION_TYPE__ERROR_MODEL_TYPE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Parameter target Function feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameter_targetFunctionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionType_Parameter_targetFunction_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionType_Parameter_targetFunction_feature",
								"_UI_FunctionType_type"),
						FunctionModelingPackage.Literals.FUNCTION_TYPE__PARAMETER_TARGET_FUNCTION,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE);
			childrenFeatures
					.add(FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT);
			childrenFeatures
					.add(FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT_GROUP);
			childrenFeatures
					.add(FunctionModelingPackage.Literals.FUNCTION_TYPE__CONNECTOR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionType) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_FunctionType_type")
				: getString("_UI_FunctionType_type") + " " + label;
	}

	
	/**
	 * @generated
	 */
	public Collection<?> getChildrenGen(Object object) {
		return super.getChildren(object);
	}

	@Override
	/* TODO kludge, when porting to JET include the different versions
	 * in each of the subclasses instead.
	 */
	public Collection<?> getChildren(Object object) {
		FunctionType function = (FunctionType) object;
		Collection children = getChildrenGen(object);
		Collection<FunctionConnector> connectors = function.getConnector();
		children.removeAll(connectors);
		if (function instanceof DesignFunctionType) {
			Collection<DesignFunctionPrototype> parts = ((DesignFunctionType) function)
					.getPart();
			children.removeAll(parts);
		} else if (function instanceof AnalysisFunctionType) {
			Collection<AnalysisFunctionPrototype> parts = ((AnalysisFunctionType) function)
					.getPart();
			children.removeAll(parts);
		}
		Collection<FunctionPort> ports = function.getPort();
		children.removeAll(ports);
		//System.out.println("Removing " + connectors.size() + " connectors");
		children.add(getPortList(function));
		children.add(getPartList(function));
		children.add(getConnectionList(function));

		return children;
	}

	public ConnectionList getConnectionList(FunctionType function) {
		if (!connectionLists.containsKey(function)) {
			connectionLists.put(function, new ConnectionList(adapterFactory,
					function));
		}
		return connectionLists.get(function);
	}

	public PortList getPortList(FunctionType function) {
		if (!portLists.containsKey(function)) {
			portLists.put(function, new PortList(adapterFactory, function));
		}
		return portLists.get(function);
	}

	public PrototypeList getPartList(FunctionType function) {
		if (!partLists.containsKey(function)) {
			partLists
					.put(function, new PrototypeList(adapterFactory, function));
		}
		return partLists.get(function);
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FunctionType.class)) {
		case FunctionModelingPackage.FUNCTION_TYPE__IS_ELEMENTARY:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case FunctionModelingPackage.FUNCTION_TYPE__ATP_FEATURE:
		case FunctionModelingPackage.FUNCTION_TYPE__PORT:
		case FunctionModelingPackage.FUNCTION_TYPE__PORT_GROUP:
		case FunctionModelingPackage.FUNCTION_TYPE__CONNECTOR:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createAnalysisFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createDesignFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPowerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionInFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionOutFlowPort()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						FunctionModelingFactory.eINSTANCE
								.createFunctionInOutFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionClientPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionServerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPortProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createVehicleLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createSystemModel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createAnalysisLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createDesignLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createImplementationLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeatureModel()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						VehicleFeatureModelingFactory.eINSTANCE
								.createVehicleFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createHardwareComponentPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwareConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createLogicalBus()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerOutHardwarePin()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						HardwareModelingFactory.eINSTANCE
								.createPowerInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwarePinProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				EnvironmentFactory.eINSTANCE.createClampConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createErrorModelPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFailureOutPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFaultInPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createInternalFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createProcessFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				DatatypesFactory.eINSTANCE.createEADatatypePrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemTemplateFactory.eINSTANCE.createSystem()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionPowerPort()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionInFlowPort()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionOutFlowPort()));

		newChildDescriptors
				.add(createChildParameter(
						FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
						FunctionModelingFactory.eINSTANCE
								.createFunctionInOutFlowPort()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionClientPort()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionServerPort()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT,
				FunctionModelingFactory.eINSTANCE.createFunctionPortProxy()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT_GROUP,
				FunctionModelingFactory.eINSTANCE.createPortGroup()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_TYPE__CONNECTOR,
				FunctionModelingFactory.eINSTANCE.createFunctionConnector()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE
				|| childFeature == FunctionModelingPackage.Literals.FUNCTION_TYPE__CONNECTOR
				|| childFeature == FunctionModelingPackage.Literals.FUNCTION_TYPE__PORT;

		if (qualify) {
			return getString("_UI_CreateChild_text2", new Object[] {
					getTypeText(childObject), getFeatureText(childFeature),
					getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
