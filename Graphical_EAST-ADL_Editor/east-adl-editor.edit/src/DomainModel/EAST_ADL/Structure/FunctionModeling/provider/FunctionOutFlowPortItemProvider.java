package DomainModel.EAST_ADL.Structure.FunctionModeling.provider;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionOutFlowPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.FunctionConnector_port;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionOutFlowPort} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionOutFlowPortItemProvider extends
		FunctionFlowPortItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource, IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionOutFlowPortItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This returns FunctionOutFlowPort.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/FunctionOutFlowPort"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {

		String label = super.getText(object);

		FunctionOutFlowPort startPort = ((FunctionOutFlowPort) object);
		EList<FunctionConnector_port> connectorPortList = startPort
				.getFunctionConnector_port_functionPort();

		if (connectorPortList.isEmpty()) {
			return label + ": Warning no port set";
		}

		// Get the starting port in the function connector port
		FunctionConnector_port startConnectorPort = connectorPortList.get(0);

		// Get the parent of the starting port
		FunctionConnector startPortParent = (FunctionConnector) startConnectorPort
				.eContainer();
		if (startPortParent == null) {
			return "Error: parent not found";
		}

		// Get the other FunctionConnector_port
		EList<FunctionConnector_port> fCPortList = startPortParent.getPort();
		if (fCPortList.isEmpty()) {
			return "Error: no Function Connection port found in: "
					+ startPortParent.getShortName();
		}

		// Check if only 1 port exist in the function connector
		if (fCPortList.size() == 1) {
			return label + ": Warning no port set";
		}

		// Select the correct FunctionConnector_port
		FunctionConnector_port endConnectorPort = null;
		if (fCPortList.get(0).equals(startConnectorPort)) {
			endConnectorPort = fCPortList.get(1);
		} else {
			endConnectorPort = fCPortList.get(0);
		}

		// Check if we found the endConnector port
		if (endConnectorPort == null) {
			return "Error: no endConnectorPort found";
		}

		// Get the prototype name of the endConnecterPort
		FunctionPrototype prototype = endConnectorPort.getFunctionPrototype();
		if (prototype == null) {
			return label + " ( --> this."
					+ endConnectorPort.getFunctionPort().getShortName() + ")";
		}

		return label + " ( --> " + prototype.getShortName() + "."
				+ endConnectorPort.getFunctionPort().getShortName() + ")";

	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}