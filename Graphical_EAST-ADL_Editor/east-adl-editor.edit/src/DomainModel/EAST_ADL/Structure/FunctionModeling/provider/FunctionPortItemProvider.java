/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionPortItemProvider extends EAElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPortItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAtpInstanceRef_atpContextElementPropertyDescriptor(object);
			addAtpInstanceRef_atpTargetPropertyDescriptor(object);
			addAtpTypePropertyDescriptor(object);
			addPortGroup_portPropertyDescriptor(object);
			addFunctionConnector_port_functionPortPropertyDescriptor(object);
			addFunctionTrigger_portPropertyDescriptor(object);
			addClampConnector_port_functionPortPropertyDescriptor(object);
			addFaultFailurePort_functionTarget_functionPortPropertyDescriptor(object);
			addParameter_targetFunctionPortPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Context Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpContextElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_CONTEXT_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpInstanceRef_atpTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpTarget_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAtpTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_AtpPrototype_atpType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_AtpPrototype_atpType_feature",
						"_UI_AtpPrototype_type"),
				AbstractStructurePackage.Literals.ATP_PROTOTYPE__ATP_TYPE,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Port Group port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPortGroup_portPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPort_PortGroup_port_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_FunctionPort_PortGroup_port_feature",
								"_UI_FunctionPort_type"),
						FunctionModelingPackage.Literals.FUNCTION_PORT__PORT_GROUP_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Connector port function Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionConnector_port_functionPortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPort_FunctionConnector_port_functionPort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPort_FunctionConnector_port_functionPort_feature",
								"_UI_FunctionPort_type"),
						FunctionModelingPackage.Literals.FUNCTION_PORT__FUNCTION_CONNECTOR_PORT_FUNCTION_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Trigger port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionTrigger_portPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPort_FunctionTrigger_port_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPort_FunctionTrigger_port_feature",
								"_UI_FunctionPort_type"),
						FunctionModelingPackage.Literals.FUNCTION_PORT__FUNCTION_TRIGGER_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Clamp Connector port function Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClampConnector_port_functionPortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPort_ClampConnector_port_functionPort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPort_ClampConnector_port_functionPort_feature",
								"_UI_FunctionPort_type"),
						FunctionModelingPackage.Literals.FUNCTION_PORT__CLAMP_CONNECTOR_PORT_FUNCTION_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Fault Failure Port function Target function Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFaultFailurePort_functionTarget_functionPortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPort_FaultFailurePort_functionTarget_functionPort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPort_FaultFailurePort_functionTarget_functionPort_feature",
								"_UI_FunctionPort_type"),
						FunctionModelingPackage.Literals.FUNCTION_PORT__FAULT_FAILURE_PORT_FUNCTION_TARGET_FUNCTION_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Parameter target Function Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameter_targetFunctionPortPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_FunctionPort_Parameter_targetFunctionPort_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_FunctionPort_Parameter_targetFunctionPort_feature",
								"_UI_FunctionPort_type"),
						FunctionModelingPackage.Literals.FUNCTION_PORT__PARAMETER_TARGET_FUNCTION_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionPort) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_FunctionPort_type")
				: getString("_UI_FunctionPort_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
