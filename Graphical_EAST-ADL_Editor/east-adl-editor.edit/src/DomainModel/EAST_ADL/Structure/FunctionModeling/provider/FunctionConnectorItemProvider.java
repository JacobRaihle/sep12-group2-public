/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.provider;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructurePackage;

import DomainModel.AUTOSAR.SystemTemplate.SystemTemplateFactory;

import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelFactory;

import DomainModel.EAST_ADL.Environment.EnvironmentFactory;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.DatatypesFactory;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureModelingFactory;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;

import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef._instanceRefFactory;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.SystemModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingFactory;

import java.awt.Color;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a
 * {@link DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class FunctionConnectorItemProvider extends EAElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public FunctionConnectorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	public Object getParent(Object object) {
		Object function = super.getParent(object);
		FunctionTypeItemProvider functionItemProvider = (FunctionTypeItemProvider) adapterFactory
				.adapt(function, IEditingDomainItemProvider.class);

		return functionItemProvider != null ? functionItemProvider
				.getConnectionList((FunctionType) function) : null;
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFunctionAllocation_allocatedElement_allocateableElement_contextPropertyDescriptor(object);
			addFunctionAllocation_allocatedElement_allocateableElementPropertyDescriptor(object);
			addAtpInstanceRef_atpContextElementPropertyDescriptor(object);
			addAtpInstanceRef_atpTargetPropertyDescriptor(object);
			addAtpInstanceRef_atpBasePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Function Allocation allocated
	 * Element allocateable Element context feature. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFunctionAllocation_allocatedElement_allocateableElement_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_context_feature",
								"_UI_AllocateableElement_type"),
						FunctionModelingPackage.Literals.ALLOCATEABLE_ELEMENT__FUNCTION_ALLOCATION_ALLOCATED_ELEMENT_ALLOCATEABLE_ELEMENT_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Allocation allocated
	 * Element allocateable Element feature. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFunctionAllocation_allocatedElement_allocateableElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_feature",
								"_UI_AllocateableElement_type"),
						FunctionModelingPackage.Literals.ALLOCATEABLE_ELEMENT__FUNCTION_ALLOCATION_ALLOCATED_ELEMENT_ALLOCATEABLE_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Context
	 * Element feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAtpInstanceRef_atpContextElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpContextElement_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_CONTEXT_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Target
	 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAtpInstanceRef_atpTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpFeature_AtpInstanceRef_atpTarget_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpFeature_AtpInstanceRef_atpTarget_feature",
								"_UI_AtpFeature_type"),
						AbstractStructurePackage.Literals.ATP_FEATURE__ATP_INSTANCE_REF_ATP_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Atp Instance Ref atp Base
	 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAtpInstanceRef_atpBasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AtpClassifier_AtpInstanceRef_atpBase_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AtpClassifier_AtpInstanceRef_atpBase_feature",
								"_UI_AtpClassifier_type"),
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_INSTANCE_REF_ATP_BASE,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to
	 * deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in
	 * {@link #createCommand}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE);
			childrenFeatures
					.add(FunctionModelingPackage.Literals.FUNCTION_CONNECTOR__PORT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper
		// feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FunctionConnector.gif. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/FunctionConnector"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionConnector) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_FunctionConnector_type")
				: getString("_UI_FunctionConnector_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FunctionConnector.class)) {
		case FunctionModelingPackage.FUNCTION_CONNECTOR__ATP_FEATURE:
		case FunctionModelingPackage.FUNCTION_CONNECTOR__PORT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createAnalysisFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE
						.createDesignFunctionPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPowerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionInFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionOutFlowPort()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						FunctionModelingFactory.eINSTANCE
								.createFunctionInOutFlowPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionClientPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionServerPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FunctionModelingFactory.eINSTANCE.createFunctionPortProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createVehicleLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createSystemModel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createAnalysisLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createDesignLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemModelingFactory.eINSTANCE.createImplementationLevel()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				FeatureModelingFactory.eINSTANCE.createFeatureModel()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						VehicleFeatureModelingFactory.eINSTANCE
								.createVehicleFeature()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createHardwareComponentPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwareConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createLogicalBus()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createPowerOutHardwarePin()));

		newChildDescriptors
				.add(createChildParameter(
						AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
						HardwareModelingFactory.eINSTANCE
								.createPowerInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createIOInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE
						.createCommunicationInOutHardwarePin()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				HardwareModelingFactory.eINSTANCE.createHardwarePinProxy()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				EnvironmentFactory.eINSTANCE.createClampConnector()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createErrorModelPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFailureOutPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createFaultInPort()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createInternalFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				ErrorModelFactory.eINSTANCE.createProcessFaultPrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				DatatypesFactory.eINSTANCE.createEADatatypePrototype()));

		newChildDescriptors.add(createChildParameter(
				AbstractStructurePackage.Literals.ATP_CLASSIFIER__ATP_FEATURE,
				SystemTemplateFactory.eINSTANCE.createSystem()));

		newChildDescriptors.add(createChildParameter(
				FunctionModelingPackage.Literals.FUNCTION_CONNECTOR__PORT,
				_instanceRefFactory.eINSTANCE.createFunctionConnector_port()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
