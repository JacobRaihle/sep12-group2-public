/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingPackage;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionPrototype} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignFunctionPrototypeItemProvider extends
		FunctionPrototypeItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource, IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignFunctionPrototypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFunctionAllocation_allocatedElement_allocateableElement_contextPropertyDescriptor(object);
			addFunctionAllocation_allocatedElement_allocateableElementPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addExecutionTimeConstraint_targetDesignFunctionPrototypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Function Allocation allocated Element allocateable Element context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionAllocation_allocatedElement_allocateableElement_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_context_feature",
								"_UI_AllocateableElement_type"),
						FunctionModelingPackage.Literals.ALLOCATEABLE_ELEMENT__FUNCTION_ALLOCATION_ALLOCATED_ELEMENT_ALLOCATEABLE_ELEMENT_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Function Allocation allocated Element allocateable Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionAllocation_allocatedElement_allocateableElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_AllocateableElement_FunctionAllocation_allocatedElement_allocateableElement_feature",
								"_UI_AllocateableElement_type"),
						FunctionModelingPackage.Literals.ALLOCATEABLE_ELEMENT__FUNCTION_ALLOCATION_ALLOCATED_ELEMENT_ALLOCATEABLE_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DesignFunctionPrototype_type_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_DesignFunctionPrototype_type_feature",
								"_UI_DesignFunctionPrototype_type"),
						FunctionModelingPackage.Literals.DESIGN_FUNCTION_PROTOTYPE__TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Execution Time Constraint target Design Function Prototype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionTimeConstraint_targetDesignFunctionPrototypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_DesignFunctionPrototype_ExecutionTimeConstraint_targetDesignFunctionPrototype_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_DesignFunctionPrototype_ExecutionTimeConstraint_targetDesignFunctionPrototype_feature",
								"_UI_DesignFunctionPrototype_type"),
						FunctionModelingPackage.Literals.DESIGN_FUNCTION_PROTOTYPE__EXECUTION_TIME_CONSTRAINT_TARGET_DESIGN_FUNCTION_PROTOTYPE,
						true, false, true, null, null, null));
	}

	/**
	 * Overrides the getChildren method of function and hardware prototypes
	 * to include a virtual copy of its type and that type's ports.
	 * @generated
	 */

	@Override
	public Collection<?> getChildren(Object object) {
		Collection children = (Collection) super.getChildren(object);
		DesignFunctionPrototype proto = (DesignFunctionPrototype) object;
		DesignFunctionType type = proto.getType();
		if (type != null) {
			children.add(type);
			for (FunctionPort port : type.getPort()) {
				children.add(port);
			}
		}
		return children;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(FunctionModelingPackage.Literals.DESIGN_FUNCTION_PROTOTYPE__DERIVED_PART);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DesignFunctionPrototype.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/DesignFunctionPrototype"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DesignFunctionPrototype) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_DesignFunctionPrototype_type")
				: getString("_UI_DesignFunctionPrototype_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DesignFunctionPrototype.class)) {
		case FunctionModelingPackage.DESIGN_FUNCTION_PROTOTYPE__DERIVED_PART:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						FunctionModelingPackage.Literals.DESIGN_FUNCTION_PROTOTYPE__DERIVED_PART,
						FunctionModelingFactory.eINSTANCE
								.createDesignFunctionPrototype()));
	}

}
