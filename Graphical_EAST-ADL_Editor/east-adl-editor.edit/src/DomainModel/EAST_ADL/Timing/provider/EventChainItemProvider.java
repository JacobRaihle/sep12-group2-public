/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Timing.provider;

import DomainModel.EAST_ADL.Timing.EventChain;
import DomainModel.EAST_ADL.Timing.TimingPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Timing.EventChain} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EventChainItemProvider extends TimingDescriptionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventChainItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addResponsePropertyDescriptor(object);
			addStrandPropertyDescriptor(object);
			addStimulusPropertyDescriptor(object);
			addSegmentPropertyDescriptor(object);
			addEventChain_strandPropertyDescriptor(object);
			addEventChain_segmentPropertyDescriptor(object);
			addDelayConstraint_scopePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Response feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResponsePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_EventChain_response_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_EventChain_response_feature",
						"_UI_EventChain_type"),
				TimingPackage.Literals.EVENT_CHAIN__RESPONSE, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Strand feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrandPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EventChain_strand_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_EventChain_strand_feature",
								"_UI_EventChain_type"),
						TimingPackage.Literals.EVENT_CHAIN__STRAND, true,
						false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Stimulus feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStimulusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_EventChain_stimulus_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_EventChain_stimulus_feature",
						"_UI_EventChain_type"),
				TimingPackage.Literals.EVENT_CHAIN__STIMULUS, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Segment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSegmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EventChain_segment_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_EventChain_segment_feature",
								"_UI_EventChain_type"),
						TimingPackage.Literals.EVENT_CHAIN__SEGMENT, true,
						false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Chain strand feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventChain_strandPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_EventChain_EventChain_strand_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_EventChain_EventChain_strand_feature",
						"_UI_EventChain_type"),
				TimingPackage.Literals.EVENT_CHAIN__EVENT_CHAIN_STRAND, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Event Chain segment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventChain_segmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_EventChain_EventChain_segment_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_EventChain_EventChain_segment_feature",
						"_UI_EventChain_type"),
				TimingPackage.Literals.EVENT_CHAIN__EVENT_CHAIN_SEGMENT, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Delay Constraint scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDelayConstraint_scopePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_EventChain_DelayConstraint_scope_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_EventChain_DelayConstraint_scope_feature",
						"_UI_EventChain_type"),
				TimingPackage.Literals.EVENT_CHAIN__DELAY_CONSTRAINT_SCOPE,
				true, false, true, null, null, null));
	}

	/**
	 * This returns EventChain.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/EventChain"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((EventChain) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_EventChain_type")
				: getString("_UI_EventChain_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
