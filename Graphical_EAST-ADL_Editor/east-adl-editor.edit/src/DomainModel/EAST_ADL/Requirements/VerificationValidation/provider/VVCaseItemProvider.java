/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements.VerificationValidation.provider;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.TraceableSpecificationItemProvider;

import DomainModel.EAST_ADL.Requirements.VerificationValidation.VVCase;
import DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidationFactory;
import DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidationPackage;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVCase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VVCaseItemProvider extends TraceableSpecificationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VVCaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addConcreteVVCasePropertyDescriptor(object);
			addVvTargetPropertyDescriptor(object);
			addAbstractVVCasePropertyDescriptor(object);
			addVvSubjectPropertyDescriptor(object);
			addVerify_verifiedByCasePropertyDescriptor(object);
			addVVCase_concreteVVCasePropertyDescriptor(object);
			addVVCase_abstractVVCasePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Concrete VV Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcreteVVCasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVCase_concreteVVCase_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVCase_concreteVVCase_feature",
								"_UI_VVCase_type"),
						VerificationValidationPackage.Literals.VV_CASE__CONCRETE_VV_CASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Vv Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVvTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_VVCase_vvTarget_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_VVCase_vvTarget_feature", "_UI_VVCase_type"),
				VerificationValidationPackage.Literals.VV_CASE__VV_TARGET,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Abstract VV Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractVVCasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVCase_abstractVVCase_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVCase_abstractVVCase_feature",
								"_UI_VVCase_type"),
						VerificationValidationPackage.Literals.VV_CASE__ABSTRACT_VV_CASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Vv Subject feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVvSubjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_VVCase_vvSubject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_VVCase_vvSubject_feature", "_UI_VVCase_type"),
				VerificationValidationPackage.Literals.VV_CASE__VV_SUBJECT,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Verify verified By Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVerify_verifiedByCasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVCase_Verify_verifiedByCase_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVCase_Verify_verifiedByCase_feature",
								"_UI_VVCase_type"),
						VerificationValidationPackage.Literals.VV_CASE__VERIFY_VERIFIED_BY_CASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Case concrete VV Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVCase_concreteVVCasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVCase_VVCase_concreteVVCase_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVCase_VVCase_concreteVVCase_feature",
								"_UI_VVCase_type"),
						VerificationValidationPackage.Literals.VV_CASE__VV_CASE_CONCRETE_VV_CASE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Case abstract VV Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVCase_abstractVVCasePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVCase_VVCase_abstractVVCase_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVCase_VVCase_abstractVVCase_feature",
								"_UI_VVCase_type"),
						VerificationValidationPackage.Literals.VV_CASE__VV_CASE_ABSTRACT_VV_CASE,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(VerificationValidationPackage.Literals.VV_CASE__VV_PROCEDURE);
			childrenFeatures
					.add(VerificationValidationPackage.Literals.VV_CASE__VV_LOG);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns VVCase.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/VVCase"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((VVCase) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_VVCase_type")
				: getString("_UI_VVCase_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(VVCase.class)) {
		case VerificationValidationPackage.VV_CASE__VV_PROCEDURE:
		case VerificationValidationPackage.VV_CASE__VV_LOG:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				VerificationValidationPackage.Literals.VV_CASE__VV_PROCEDURE,
				VerificationValidationFactory.eINSTANCE.createVVProcedure()));

		newChildDescriptors.add(createChildParameter(
				VerificationValidationPackage.Literals.VV_CASE__VV_LOG,
				VerificationValidationFactory.eINSTANCE.createVVLog()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
