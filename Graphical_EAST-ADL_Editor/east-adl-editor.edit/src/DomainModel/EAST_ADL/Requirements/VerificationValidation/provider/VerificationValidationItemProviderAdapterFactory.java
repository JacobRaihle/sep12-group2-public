/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements.VerificationValidation.provider;

import DomainModel.EAST_ADL.Requirements.VerificationValidation.util.VerificationValidationAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VerificationValidationItemProviderAdapterFactory extends
		VerificationValidationAdapterFactory implements
		ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VerificationValidationItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
		supportedTypes.add(IItemColorProvider.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidation} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VerificationValidationItemProvider verificationValidationItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVerificationValidationAdapter() {
		if (verificationValidationItemProvider == null) {
			verificationValidationItemProvider = new VerificationValidationItemProvider(
					this);
		}

		return verificationValidationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.Verify} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VerifyItemProvider verifyItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.Verify}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVerifyAdapter() {
		if (verifyItemProvider == null) {
			verifyItemProvider = new VerifyItemProvider(this);
		}

		return verifyItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVActualOutcome} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVActualOutcomeItemProvider vvActualOutcomeItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVActualOutcome}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVActualOutcomeAdapter() {
		if (vvActualOutcomeItemProvider == null) {
			vvActualOutcomeItemProvider = new VVActualOutcomeItemProvider(this);
		}

		return vvActualOutcomeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVCase} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVCaseItemProvider vvCaseItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVCaseAdapter() {
		if (vvCaseItemProvider == null) {
			vvCaseItemProvider = new VVCaseItemProvider(this);
		}

		return vvCaseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVIntendedOutcome} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVIntendedOutcomeItemProvider vvIntendedOutcomeItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVIntendedOutcome}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVIntendedOutcomeAdapter() {
		if (vvIntendedOutcomeItemProvider == null) {
			vvIntendedOutcomeItemProvider = new VVIntendedOutcomeItemProvider(
					this);
		}

		return vvIntendedOutcomeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVLog} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVLogItemProvider vvLogItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVLog}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVLogAdapter() {
		if (vvLogItemProvider == null) {
			vvLogItemProvider = new VVLogItemProvider(this);
		}

		return vvLogItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVProcedure} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVProcedureItemProvider vvProcedureItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVProcedure}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVProcedureAdapter() {
		if (vvProcedureItemProvider == null) {
			vvProcedureItemProvider = new VVProcedureItemProvider(this);
		}

		return vvProcedureItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVStimuli} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVStimuliItemProvider vvStimuliItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVStimuli}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVStimuliAdapter() {
		if (vvStimuliItemProvider == null) {
			vvStimuliItemProvider = new VVStimuliItemProvider(this);
		}

		return vvStimuliItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVTarget} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VVTargetItemProvider vvTargetItemProvider;

	/**
	 * This creates an adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVTarget}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVVTargetAdapter() {
		if (vvTargetItemProvider == null) {
			vvTargetItemProvider = new VVTargetItemProvider(this);
		}

		return vvTargetItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory
				.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(
			ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>)
					|| (((Class<?>) type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (verificationValidationItemProvider != null)
			verificationValidationItemProvider.dispose();
		if (verifyItemProvider != null)
			verifyItemProvider.dispose();
		if (vvActualOutcomeItemProvider != null)
			vvActualOutcomeItemProvider.dispose();
		if (vvCaseItemProvider != null)
			vvCaseItemProvider.dispose();
		if (vvIntendedOutcomeItemProvider != null)
			vvIntendedOutcomeItemProvider.dispose();
		if (vvLogItemProvider != null)
			vvLogItemProvider.dispose();
		if (vvProcedureItemProvider != null)
			vvProcedureItemProvider.dispose();
		if (vvStimuliItemProvider != null)
			vvStimuliItemProvider.dispose();
		if (vvTargetItemProvider != null)
			vvTargetItemProvider.dispose();
	}

}
