/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements.VerificationValidation.provider;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.TraceableSpecificationItemProvider;

import DomainModel.EAST_ADL.Requirements.VerificationValidation.VVProcedure;
import DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidationFactory;
import DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidationPackage;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Requirements.VerificationValidation.VVProcedure} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class VVProcedureItemProvider extends TraceableSpecificationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VVProcedureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addConcreteVVProcedurePropertyDescriptor(object);
			addAbstractVVProcedurePropertyDescriptor(object);
			addVerify_verifiedByProcedurePropertyDescriptor(object);
			addVVLog_performedVVProcedurePropertyDescriptor(object);
			addVVProcedure_concreteVVProcedurePropertyDescriptor(object);
			addVVProcedure_abstractVVProcedurePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Concrete VV Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcreteVVProcedurePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVProcedure_concreteVVProcedure_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVProcedure_concreteVVProcedure_feature",
								"_UI_VVProcedure_type"),
						VerificationValidationPackage.Literals.VV_PROCEDURE__CONCRETE_VV_PROCEDURE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Abstract VV Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractVVProcedurePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVProcedure_abstractVVProcedure_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_VVProcedure_abstractVVProcedure_feature",
								"_UI_VVProcedure_type"),
						VerificationValidationPackage.Literals.VV_PROCEDURE__ABSTRACT_VV_PROCEDURE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Verify verified By Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVerify_verifiedByProcedurePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVProcedure_Verify_verifiedByProcedure_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VVProcedure_Verify_verifiedByProcedure_feature",
								"_UI_VVProcedure_type"),
						VerificationValidationPackage.Literals.VV_PROCEDURE__VERIFY_VERIFIED_BY_PROCEDURE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Log performed VV Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVLog_performedVVProcedurePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVProcedure_VVLog_performedVVProcedure_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VVProcedure_VVLog_performedVVProcedure_feature",
								"_UI_VVProcedure_type"),
						VerificationValidationPackage.Literals.VV_PROCEDURE__VV_LOG_PERFORMED_VV_PROCEDURE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Procedure concrete VV Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVProcedure_concreteVVProcedurePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVProcedure_VVProcedure_concreteVVProcedure_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VVProcedure_VVProcedure_concreteVVProcedure_feature",
								"_UI_VVProcedure_type"),
						VerificationValidationPackage.Literals.VV_PROCEDURE__VV_PROCEDURE_CONCRETE_VV_PROCEDURE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Procedure abstract VV Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVProcedure_abstractVVProcedurePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_VVProcedure_VVProcedure_abstractVVProcedure_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_VVProcedure_VVProcedure_abstractVVProcedure_feature",
								"_UI_VVProcedure_type"),
						VerificationValidationPackage.Literals.VV_PROCEDURE__VV_PROCEDURE_ABSTRACT_VV_PROCEDURE,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(VerificationValidationPackage.Literals.VV_PROCEDURE__VV_STIMULI);
			childrenFeatures
					.add(VerificationValidationPackage.Literals.VV_PROCEDURE__VV_INTENDED_OUTCOME);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns VVProcedure.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/VVProcedure"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((VVProcedure) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_VVProcedure_type")
				: getString("_UI_VVProcedure_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(VVProcedure.class)) {
		case VerificationValidationPackage.VV_PROCEDURE__VV_STIMULI:
		case VerificationValidationPackage.VV_PROCEDURE__VV_INTENDED_OUTCOME:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						VerificationValidationPackage.Literals.VV_PROCEDURE__VV_STIMULI,
						VerificationValidationFactory.eINSTANCE
								.createVVStimuli()));

		newChildDescriptors
				.add(createChildParameter(
						VerificationValidationPackage.Literals.VV_PROCEDURE__VV_INTENDED_OUTCOME,
						VerificationValidationFactory.eINSTANCE
								.createVVIntendedOutcome()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
