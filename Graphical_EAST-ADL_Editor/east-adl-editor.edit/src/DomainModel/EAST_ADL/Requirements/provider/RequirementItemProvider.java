/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements.provider;

import DomainModel.EAST_ADL.Requirements.Requirement;
import DomainModel.EAST_ADL.Requirements.RequirementsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Requirements.Requirement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RequirementItemProvider extends
		RequirementSpecificationObjectItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFormalismPropertyDescriptor(object);
			addUrlPropertyDescriptor(object);
			addModePropertyDescriptor(object);
			addDeriveRequirement_derivedPropertyDescriptor(object);
			addDeriveRequirement_derivedFromPropertyDescriptor(object);
			addRefine_refinedRequirementPropertyDescriptor(object);
			addSatisfy_satisfiedRequirementPropertyDescriptor(object);
			addRequirementsLink_sourcePropertyDescriptor(object);
			addRequirementsLink_targetPropertyDescriptor(object);
			addVerify_verifiedRequirementPropertyDescriptor(object);
			addFeatureFlaw_nonFulfilledRequirementPropertyDescriptor(object);
			addFunctionalSafetyConcept_functionalSafetyRequirementPropertyDescriptor(object);
			addSafetyGoal_requirementPropertyDescriptor(object);
			addTechnicalSafetyConcept_technicalSafetyRequirementPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Formalism feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFormalismPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Requirement_formalism_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Requirement_formalism_feature",
						"_UI_Requirement_type"),
				RequirementsPackage.Literals.REQUIREMENT__FORMALISM, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Url feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUrlPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Requirement_url_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Requirement_url_feature", "_UI_Requirement_type"),
				RequirementsPackage.Literals.REQUIREMENT__URL, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Mode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_mode_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Requirement_mode_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__MODE, true,
						false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Derive Requirement derived feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeriveRequirement_derivedPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_DeriveRequirement_derived_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_DeriveRequirement_derived_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__DERIVE_REQUIREMENT_DERIVED,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Derive Requirement derived From feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeriveRequirement_derivedFromPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_DeriveRequirement_derivedFrom_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_DeriveRequirement_derivedFrom_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__DERIVE_REQUIREMENT_DERIVED_FROM,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Refine refined Requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRefine_refinedRequirementPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_Refine_refinedRequirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_Refine_refinedRequirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__REFINE_REFINED_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Satisfy satisfied Requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSatisfy_satisfiedRequirementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_Satisfy_satisfiedRequirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_Satisfy_satisfiedRequirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__SATISFY_SATISFIED_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Requirements Link source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequirementsLink_sourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_RequirementsLink_source_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_RequirementsLink_source_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__REQUIREMENTS_LINK_SOURCE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Requirements Link target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequirementsLink_targetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_RequirementsLink_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_RequirementsLink_target_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__REQUIREMENTS_LINK_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Verify verified Requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVerify_verifiedRequirementPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_Verify_verifiedRequirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_Verify_verifiedRequirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__VERIFY_VERIFIED_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Feature Flaw non Fulfilled Requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeatureFlaw_nonFulfilledRequirementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_FeatureFlaw_nonFulfilledRequirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_FeatureFlaw_nonFulfilledRequirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__FEATURE_FLAW_NON_FULFILLED_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Functional Safety Concept functional Safety Requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionalSafetyConcept_functionalSafetyRequirementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_FunctionalSafetyConcept_functionalSafetyRequirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_FunctionalSafetyConcept_functionalSafetyRequirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__FUNCTIONAL_SAFETY_CONCEPT_FUNCTIONAL_SAFETY_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Safety Goal requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSafetyGoal_requirementPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_SafetyGoal_requirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_SafetyGoal_requirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__SAFETY_GOAL_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Technical Safety Concept technical Safety Requirement feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTechnicalSafetyConcept_technicalSafetyRequirementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Requirement_TechnicalSafetyConcept_technicalSafetyRequirement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Requirement_TechnicalSafetyConcept_technicalSafetyRequirement_feature",
								"_UI_Requirement_type"),
						RequirementsPackage.Literals.REQUIREMENT__TECHNICAL_SAFETY_CONCEPT_TECHNICAL_SAFETY_REQUIREMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This returns Requirement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Requirement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Requirement) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Requirement_type")
				: getString("_UI_Requirement_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Requirement.class)) {
		case RequirementsPackage.REQUIREMENT__FORMALISM:
		case RequirementsPackage.REQUIREMENT__URL:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
