/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Annexes.BehaviorConstraints.provider;

import DomainModel.EAST_ADL.Annexes.BehaviorConstraints.BehaviorConstraintsPackage;
import DomainModel.EAST_ADL.Annexes.BehaviorConstraints.ParameterCondition;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Annexes.BehaviorConstraints.ParameterCondition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ParameterConditionItemProvider extends EAElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterConditionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addExpressionPropertyDescriptor(object);
			addRepresentAnomalyPropertyDescriptor(object);
			addAppliedToConditionPropertyDescriptor(object);
			addAppliedToParameterPropertyDescriptor(object);
			addParameterCondition_appliedToConditionPropertyDescriptor(object);
			addState_denotePropertyDescriptor(object);
			addTransformation_postConditionPropertyDescriptor(object);
			addTransformation_preConditionPropertyDescriptor(object);
			addTransformation_invariantConditionPropertyDescriptor(object);
			addTransition_conditionSpecificationPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpressionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_expression_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_expression_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__EXPRESSION,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Represent Anomaly feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRepresentAnomalyPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_representAnomaly_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_representAnomaly_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__REPRESENT_ANOMALY,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Applied To Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAppliedToConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_appliedToCondition_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_appliedToCondition_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__APPLIED_TO_CONDITION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Applied To Parameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAppliedToParameterPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_appliedToParameter_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_appliedToParameter_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__APPLIED_TO_PARAMETER,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Parameter Condition applied To Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterCondition_appliedToConditionPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_ParameterCondition_appliedToCondition_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_ParameterCondition_appliedToCondition_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__PARAMETER_CONDITION_APPLIED_TO_CONDITION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the State denote feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addState_denotePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_State_denote_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_State_denote_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__STATE_DENOTE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transformation post Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformation_postConditionPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_Transformation_postCondition_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_Transformation_postCondition_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__TRANSFORMATION_POST_CONDITION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transformation pre Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformation_preConditionPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_Transformation_preCondition_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_Transformation_preCondition_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__TRANSFORMATION_PRE_CONDITION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transformation invariant Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformation_invariantConditionPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_Transformation_invariantCondition_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_Transformation_invariantCondition_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__TRANSFORMATION_INVARIANT_CONDITION,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transition condition Specification feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransition_conditionSpecificationPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ParameterCondition_Transition_conditionSpecification_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ParameterCondition_Transition_conditionSpecification_feature",
								"_UI_ParameterCondition_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER_CONDITION__TRANSITION_CONDITION_SPECIFICATION,
						true, false, true, null, null, null));
	}

	/**
	 * This returns ParameterCondition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/ParameterCondition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ParameterCondition) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ParameterCondition_type")
				: getString("_UI_ParameterCondition_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ParameterCondition.class)) {
		case BehaviorConstraintsPackage.PARAMETER_CONDITION__EXPRESSION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
