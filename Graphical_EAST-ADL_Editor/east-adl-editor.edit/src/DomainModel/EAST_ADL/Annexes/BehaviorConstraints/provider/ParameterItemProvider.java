/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Annexes.BehaviorConstraints.provider;

import DomainModel.EAST_ADL.Annexes.BehaviorConstraints.BehaviorConstraintsPackage;
import DomainModel.EAST_ADL.Annexes.BehaviorConstraints.Parameter;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAElementItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Annexes.BehaviorConstraints.Parameter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ParameterItemProvider extends EAElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTargetFunctionPortPropertyDescriptor(object);
			addTargetFunctionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addFlow_sinkParameterPropertyDescriptor(object);
			addFlow_sourceParameterPropertyDescriptor(object);
			addParameterCondition_appliedToParameterPropertyDescriptor(object);
			addTransformation_inOutPropertyDescriptor(object);
			addTransformation_inPropertyDescriptor(object);
			addTransformation_outPropertyDescriptor(object);
			addTransition_writePropertyDescriptor(object);
			addTransition_readPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Target Function Port feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetFunctionPortPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_targetFunctionPort_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_targetFunctionPort_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__TARGET_FUNCTION_PORT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Target Function feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetFunctionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Parameter_targetFunction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Parameter_targetFunction_feature",
						"_UI_Parameter_type"),
				BehaviorConstraintsPackage.Literals.PARAMETER__TARGET_FUNCTION,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Parameter_type_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Parameter_type_feature", "_UI_Parameter_type"),
				BehaviorConstraintsPackage.Literals.PARAMETER__TYPE, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Flow sink Parameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFlow_sinkParameterPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_Flow_sinkParameter_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_Flow_sinkParameter_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__FLOW_SINK_PARAMETER,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Flow source Parameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFlow_sourceParameterPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_Flow_sourceParameter_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_Flow_sourceParameter_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__FLOW_SOURCE_PARAMETER,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Parameter Condition applied To Parameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterCondition_appliedToParameterPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_ParameterCondition_appliedToParameter_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Parameter_ParameterCondition_appliedToParameter_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__PARAMETER_CONDITION_APPLIED_TO_PARAMETER,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transformation in Out feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformation_inOutPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_Transformation_inOut_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_Transformation_inOut_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__TRANSFORMATION_IN_OUT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transformation in feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformation_inPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_Transformation_in_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_Transformation_in_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__TRANSFORMATION_IN,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transformation out feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransformation_outPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_Transformation_out_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_Transformation_out_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__TRANSFORMATION_OUT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transition write feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransition_writePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Parameter_Transition_write_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_Parameter_Transition_write_feature",
								"_UI_Parameter_type"),
						BehaviorConstraintsPackage.Literals.PARAMETER__TRANSITION_WRITE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Transition read feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransition_readPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Parameter_Transition_read_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Parameter_Transition_read_feature",
						"_UI_Parameter_type"),
				BehaviorConstraintsPackage.Literals.PARAMETER__TRANSITION_READ,
				true, false, true, null, null, null));
	}

	/**
	 * This returns Parameter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Parameter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Parameter) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Parameter_type")
				: getString("_UI_Parameter_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
