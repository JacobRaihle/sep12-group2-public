/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Annexes.Needs.provider;

import DomainModel.EAST_ADL.Annexes.Needs.NeedsPackage;
import DomainModel.EAST_ADL.Annexes.Needs.Stakeholder;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.TraceableSpecificationItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Annexes.Needs.Stakeholder} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class StakeholderItemProvider extends TraceableSpecificationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StakeholderItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addResponsibilitiesPropertyDescriptor(object);
			addSuccessCriteriaPropertyDescriptor(object);
			addArchitecturalDescription_identifiesPropertyDescriptor(object);
			addVehicleSystem_hasPropertyDescriptor(object);
			addStakeholderNeed_stakeholderPropertyDescriptor(object);
			addProblemStatement_affectsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Responsibilities feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResponsibilitiesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Stakeholder_responsibilities_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Stakeholder_responsibilities_feature",
						"_UI_Stakeholder_type"),
				NeedsPackage.Literals.STAKEHOLDER__RESPONSIBILITIES, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Success Criteria feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuccessCriteriaPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Stakeholder_successCriteria_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Stakeholder_successCriteria_feature",
						"_UI_Stakeholder_type"),
				NeedsPackage.Literals.STAKEHOLDER__SUCCESS_CRITERIA, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Architectural Description identifies feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addArchitecturalDescription_identifiesPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Stakeholder_ArchitecturalDescription_identifies_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Stakeholder_ArchitecturalDescription_identifies_feature",
								"_UI_Stakeholder_type"),
						NeedsPackage.Literals.STAKEHOLDER__ARCHITECTURAL_DESCRIPTION_IDENTIFIES,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Vehicle System has feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVehicleSystem_hasPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Stakeholder_VehicleSystem_has_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Stakeholder_VehicleSystem_has_feature",
						"_UI_Stakeholder_type"),
				NeedsPackage.Literals.STAKEHOLDER__VEHICLE_SYSTEM_HAS, true,
				false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Stakeholder Need stakeholder feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStakeholderNeed_stakeholderPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Stakeholder_StakeholderNeed_stakeholder_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Stakeholder_StakeholderNeed_stakeholder_feature",
								"_UI_Stakeholder_type"),
						NeedsPackage.Literals.STAKEHOLDER__STAKEHOLDER_NEED_STAKEHOLDER,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Problem Statement affects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProblemStatement_affectsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Stakeholder_ProblemStatement_affects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Stakeholder_ProblemStatement_affects_feature",
						"_UI_Stakeholder_type"),
				NeedsPackage.Literals.STAKEHOLDER__PROBLEM_STATEMENT_AFFECTS,
				true, false, true, null, null, null));
	}

	/**
	 * This returns Stakeholder.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Stakeholder"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Stakeholder) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Stakeholder_type")
				: getString("_UI_Stakeholder_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Stakeholder.class)) {
		case NeedsPackage.STAKEHOLDER__RESPONSIBILITIES:
		case NeedsPackage.STAKEHOLDER__SUCCESS_CRITERIA:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
