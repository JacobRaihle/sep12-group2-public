/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Annexes.Needs.provider;

import DomainModel.EAST_ADL.Annexes.Needs.NeedsPackage;
import DomainModel.EAST_ADL.Annexes.Needs.ProductPositioning;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.TraceableSpecificationItemProvider;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Annexes.Needs.ProductPositioning} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ProductPositioningItemProvider extends
		TraceableSpecificationItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProductPositioningItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTargetCustomersPropertyDescriptor(object);
			addDrivingNeedsPropertyDescriptor(object);
			addKeyCapabilitiesPropertyDescriptor(object);
			addPrimaryCompetitiveAlternativePropertyDescriptor(object);
			addPrimaryDifferentiationPropertyDescriptor(object);
			addBusinessOpportunity_productPositioningPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Target Customers feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetCustomersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ProductPositioning_targetCustomers_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ProductPositioning_targetCustomers_feature",
						"_UI_ProductPositioning_type"),
				NeedsPackage.Literals.PRODUCT_POSITIONING__TARGET_CUSTOMERS,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Driving Needs feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDrivingNeedsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ProductPositioning_drivingNeeds_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ProductPositioning_drivingNeeds_feature",
						"_UI_ProductPositioning_type"),
				NeedsPackage.Literals.PRODUCT_POSITIONING__DRIVING_NEEDS, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Key Capabilities feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyCapabilitiesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_ProductPositioning_keyCapabilities_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_ProductPositioning_keyCapabilities_feature",
						"_UI_ProductPositioning_type"),
				NeedsPackage.Literals.PRODUCT_POSITIONING__KEY_CAPABILITIES,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Primary Competitive Alternative feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrimaryCompetitiveAlternativePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ProductPositioning_primaryCompetitiveAlternative_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ProductPositioning_primaryCompetitiveAlternative_feature",
								"_UI_ProductPositioning_type"),
						NeedsPackage.Literals.PRODUCT_POSITIONING__PRIMARY_COMPETITIVE_ALTERNATIVE,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Primary Differentiation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrimaryDifferentiationPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ProductPositioning_primaryDifferentiation_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ProductPositioning_primaryDifferentiation_feature",
								"_UI_ProductPositioning_type"),
						NeedsPackage.Literals.PRODUCT_POSITIONING__PRIMARY_DIFFERENTIATION,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Business Opportunity product Positioning feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusinessOpportunity_productPositioningPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_ProductPositioning_BusinessOpportunity_productPositioning_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_ProductPositioning_BusinessOpportunity_productPositioning_feature",
								"_UI_ProductPositioning_type"),
						NeedsPackage.Literals.PRODUCT_POSITIONING__BUSINESS_OPPORTUNITY_PRODUCT_POSITIONING,
						true, false, true, null, null, null));
	}

	/**
	 * This returns ProductPositioning.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/ProductPositioning"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ProductPositioning) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_ProductPositioning_type")
				: getString("_UI_ProductPositioning_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ProductPositioning.class)) {
		case NeedsPackage.PRODUCT_POSITIONING__TARGET_CUSTOMERS:
		case NeedsPackage.PRODUCT_POSITIONING__DRIVING_NEEDS:
		case NeedsPackage.PRODUCT_POSITIONING__KEY_CAPABILITIES:
		case NeedsPackage.PRODUCT_POSITIONING__PRIMARY_COMPETITIVE_ALTERNATIVE:
		case NeedsPackage.PRODUCT_POSITIONING__PRIMARY_DIFFERENTIATION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
