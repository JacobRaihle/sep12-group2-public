/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.UserAttributes.provider;

import DomainModel.EAST_ADL.Infrastructure.Elements.provider.EAPackageableElementItemProvider;

import DomainModel.EAST_ADL.Infrastructure.UserAttributes.UserAttributeElementType;
import DomainModel.EAST_ADL.Infrastructure.UserAttributes.UserAttributesFactory;
import DomainModel.EAST_ADL.Infrastructure.UserAttributes.UserAttributesPackage;

import DomainModel.EAST_ADL.Structure.SystemModeling.provider.DomainmodelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Infrastructure.UserAttributes.UserAttributeElementType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UserAttributeElementTypeItemProvider extends
		EAPackageableElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource, IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserAttributeElementTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValidForPropertyDescriptor(object);
			addExtendedElementTypePropertyDescriptor(object);
			addUserAttributeableElement_uaTypePropertyDescriptor(object);
			addUserAttributeElementType_extendedElementTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Valid For feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValidForPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_UserAttributeElementType_validFor_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_UserAttributeElementType_validFor_feature",
								"_UI_UserAttributeElementType_type"),
						UserAttributesPackage.Literals.USER_ATTRIBUTE_ELEMENT_TYPE__VALID_FOR,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Extended Element Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtendedElementTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_UserAttributeElementType_extendedElementType_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_UserAttributeElementType_extendedElementType_feature",
								"_UI_UserAttributeElementType_type"),
						UserAttributesPackage.Literals.USER_ATTRIBUTE_ELEMENT_TYPE__EXTENDED_ELEMENT_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the User Attributeable Element ua Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUserAttributeableElement_uaTypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_UserAttributeElementType_UserAttributeableElement_uaType_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_UserAttributeElementType_UserAttributeableElement_uaType_feature",
								"_UI_UserAttributeElementType_type"),
						UserAttributesPackage.Literals.USER_ATTRIBUTE_ELEMENT_TYPE__USER_ATTRIBUTEABLE_ELEMENT_UA_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the User Attribute Element Type extended Element Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUserAttributeElementType_extendedElementTypePropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_UserAttributeElementType_UserAttributeElementType_extendedElementType_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_UserAttributeElementType_UserAttributeElementType_extendedElementType_feature",
								"_UI_UserAttributeElementType_type"),
						UserAttributesPackage.Literals.USER_ATTRIBUTE_ELEMENT_TYPE__USER_ATTRIBUTE_ELEMENT_TYPE_EXTENDED_ELEMENT_TYPE,
						true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(UserAttributesPackage.Literals.USER_ATTRIBUTE_ELEMENT_TYPE__ATTRIBUTE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UserAttributeElementType.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(
				object,
				getResourceLocator().getImage(
						"full/obj16/UserAttributeElementType"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UserAttributeElementType) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_UserAttributeElementType_type")
				: getString("_UI_UserAttributeElementType_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UserAttributeElementType.class)) {
		case UserAttributesPackage.USER_ATTRIBUTE_ELEMENT_TYPE__VALID_FOR:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case UserAttributesPackage.USER_ATTRIBUTE_ELEMENT_TYPE__ATTRIBUTE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(
						UserAttributesPackage.Literals.USER_ATTRIBUTE_ELEMENT_TYPE__ATTRIBUTE,
						UserAttributesFactory.eINSTANCE
								.createUserAttributeDefinition()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DomainmodelEditPlugin.INSTANCE;
	}

}
