/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.Elements.provider;

import DomainModel.EAST_ADL.Annexes.BehaviorConstraints.BehaviorConstraintsFactory;

import DomainModel.EAST_ADL.Annexes.Needs.NeedsFactory;

import DomainModel.EAST_ADL.Behavior.BehaviorFactory;

import DomainModel.EAST_ADL.Dependability.DependabilityFactory;

import DomainModel.EAST_ADL.Dependability.ErrorModel.ErrorModelFactory;

import DomainModel.EAST_ADL.Dependability.SafetyCase.SafetyCaseFactory;

import DomainModel.EAST_ADL.Dependability.SafetyConstraints.SafetyConstraintsFactory;

import DomainModel.EAST_ADL.Dependability.SafetyRequirement.SafetyRequirementFactory;

import DomainModel.EAST_ADL.Environment.EnvironmentFactory;

import DomainModel.EAST_ADL.GenericConstraints.GenericConstraintsFactory;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.DatatypesFactory;

import DomainModel.EAST_ADL.Infrastructure.Elements.EAPackage;
import DomainModel.EAST_ADL.Infrastructure.Elements.ElementsFactory;
import DomainModel.EAST_ADL.Infrastructure.Elements.ElementsPackage;

import DomainModel.EAST_ADL.Infrastructure.UserAttributes.UserAttributesFactory;

import DomainModel.EAST_ADL.Interchange.InterchangeFactory;

import DomainModel.EAST_ADL.Requirements.RequirementsFactory;

import DomainModel.EAST_ADL.Requirements.UseCases.UseCasesFactory;

import DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidationFactory;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureModelingFactory;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;

import DomainModel.EAST_ADL.Structure.SystemModeling.SystemModelingFactory;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.VehicleFeatureModelingFactory;

import DomainModel.EAST_ADL.Timing.TimingFactory;

import DomainModel.EAST_ADL.Variability.VariabilityFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.EAST_ADL.Infrastructure.Elements.EAPackage} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EAPackageItemProvider extends EAElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAPackageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE);
			childrenFeatures.add(ElementsPackage.Literals.EA_PACKAGE__ELEMENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EAPackage.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/EAPackage"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((EAPackage) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_EAPackage_type")
				: getString("_UI_EAPackage_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EAPackage.class)) {
		case ElementsPackage.EA_PACKAGE__SUB_PACKAGE:
		case ElementsPackage.EA_PACKAGE__ELEMENT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
				ElementsFactory.eINSTANCE.createEAPackage()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
				ElementsFactory.eINSTANCE.createAnalysisTypePackage()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
				ElementsFactory.eINSTANCE.createDesignTypePackage()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
						ElementsFactory.eINSTANCE
								.createHardwareComponentTypePackage()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
				FunctionModelingFactory.eINSTANCE
						.createFunctionalDesignArchitecture()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
				FunctionModelingFactory.eINSTANCE
						.createFunctionalAnalysisArchitecture()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__SUB_PACKAGE,
				HardwareModelingFactory.eINSTANCE
						.createHardwareDesignArchitecture()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SystemModelingFactory.eINSTANCE.createVehicleLevel()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SystemModelingFactory.eINSTANCE.createSystemModel()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SystemModelingFactory.eINSTANCE.createAnalysisLevel()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SystemModelingFactory.eINSTANCE.createDesignLevel()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SystemModelingFactory.eINSTANCE.createImplementationLevel()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FeatureModelingFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FeatureModelingFactory.eINSTANCE.createFeatureGroup()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FeatureModelingFactory.eINSTANCE.createFeatureModel()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						VehicleFeatureModelingFactory.eINSTANCE
								.createVehicleFeature()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						FunctionModelingFactory.eINSTANCE
								.createAnalysisFunctionType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FunctionModelingFactory.eINSTANCE.createDesignFunctionType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FunctionModelingFactory.eINSTANCE
						.createBasicSoftwareFunctionType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FunctionModelingFactory.eINSTANCE.createFunctionalDevice()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FunctionModelingFactory.eINSTANCE
						.createFunctionClientServerInterface()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						FunctionModelingFactory.eINSTANCE
								.createHardwareFunctionType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				FunctionModelingFactory.eINSTANCE.createLocalDeviceManager()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						HardwareModelingFactory.eINSTANCE
								.createHardwareComponentType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				HardwareModelingFactory.eINSTANCE.createActuator()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				HardwareModelingFactory.eINSTANCE.createNode()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				HardwareModelingFactory.eINSTANCE.createPowerSupply()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				HardwareModelingFactory.eINSTANCE.createSensor()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				BehaviorFactory.eINSTANCE.createBehavior()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				BehaviorFactory.eINSTANCE.createModeGroup()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				BehaviorFactory.eINSTANCE.createFunctionBehavior()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VariabilityFactory.eINSTANCE.createReuseMetaInformation()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VariabilityFactory.eINSTANCE.createVariability()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				RequirementsFactory.eINSTANCE.createOperationalSituation()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				RequirementsFactory.eINSTANCE.createRequirementsModel()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				RequirementsFactory.eINSTANCE
						.createRequirementsRelatedInformation()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				RequirementsFactory.eINSTANCE.createRequirement()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				RequirementsFactory.eINSTANCE.createRequirementsContainer()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						RequirementsFactory.eINSTANCE
								.createRequirementsRelationGroup()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				RequirementsFactory.eINSTANCE.createQualityRequirement()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				UseCasesFactory.eINSTANCE.createActor()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				UseCasesFactory.eINSTANCE.createUseCase()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE
						.createVerificationValidation()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						VerificationValidationFactory.eINSTANCE
								.createVVActualOutcome()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE.createVVCase()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE
						.createVVIntendedOutcome()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE.createVVLog()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE.createVVProcedure()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE.createVVStimuli()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				VerificationValidationFactory.eINSTANCE.createVVTarget()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				EnvironmentFactory.eINSTANCE.createEnvironment()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				InterchangeFactory.eINSTANCE.createRIFExportArea()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				InterchangeFactory.eINSTANCE.createRIFImportArea()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				TimingFactory.eINSTANCE.createTiming()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DependabilityFactory.eINSTANCE.createDependability()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DependabilityFactory.eINSTANCE.createFeatureFlaw()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DependabilityFactory.eINSTANCE.createHazard()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DependabilityFactory.eINSTANCE.createHazardousEvent()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DependabilityFactory.eINSTANCE.createItem()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyConstraintsFactory.eINSTANCE.createFaultFailure()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyConstraintsFactory.eINSTANCE
						.createQuantitativeSafetyConstraint()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyConstraintsFactory.eINSTANCE.createSafetyConstraint()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				ErrorModelFactory.eINSTANCE.createErrorModelType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyRequirementFactory.eINSTANCE
						.createFunctionalSafetyConcept()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyRequirementFactory.eINSTANCE
						.createTechnicalSafetyConcept()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyCaseFactory.eINSTANCE.createClaim()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyCaseFactory.eINSTANCE.createGround()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyCaseFactory.eINSTANCE.createSafetyCase()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				SafetyCaseFactory.eINSTANCE.createWarrant()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				GenericConstraintsFactory.eINSTANCE.createGenericConstraint()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				GenericConstraintsFactory.eINSTANCE
						.createGenericConstraintSet()));

		newChildDescriptors
				.add(createChildParameter(
						ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
						GenericConstraintsFactory.eINSTANCE
								.createTakeRateConstraint()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createCompositeDatatype()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createEABoolean()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createEAFloat()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createEAInteger()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createEAString()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createEnumeration()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createEnumerationValueType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				DatatypesFactory.eINSTANCE.createRangeableValueType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				UserAttributesFactory.eINSTANCE
						.createUserAttributeElementType()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				BehaviorConstraintsFactory.eINSTANCE.createBehaviorAnnex()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				NeedsFactory.eINSTANCE.createStakeholder()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				NeedsFactory.eINSTANCE.createStakeholderNeed()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				NeedsFactory.eINSTANCE.createBusinessOpportunity()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				NeedsFactory.eINSTANCE.createProblemStatement()));

		newChildDescriptors.add(createChildParameter(
				ElementsPackage.Literals.EA_PACKAGE__ELEMENT,
				NeedsFactory.eINSTANCE.createProductPositioning()));
	}

}
