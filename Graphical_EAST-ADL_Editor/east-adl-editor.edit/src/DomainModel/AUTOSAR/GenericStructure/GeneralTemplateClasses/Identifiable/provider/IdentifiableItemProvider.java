/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.provider;

import DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.Identifiable;
import DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.IdentifiablePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.Identifiable} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class IdentifiableItemProvider extends ReferrableItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IItemColorProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifiableItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCategoryPropertyDescriptor(object);
			addUuidPropertyDescriptor(object);
			addConfigurableContainer_configurableElementPropertyDescriptor(object);
			addConfigurationDecision_targetPropertyDescriptor(object);
			addPrivateContent_privateElementPropertyDescriptor(object);
			addSelectionCriterion_sourcePropertyDescriptor(object);
			addVariableElement_optionalElementPropertyDescriptor(object);
			addRefine_refinedBy_identifiable_contextPropertyDescriptor(object);
			addRefine_refinedBy_identifiable_targetPropertyDescriptor(object);
			addSatisfy_satisfiedBy_identifiable_targetPropertyDescriptor(object);
			addSatisfy_satisfiedBy_identifiable_contextPropertyDescriptor(object);
			addVVCase_vvSubjectPropertyDescriptor(object);
			addVVTarget_elementPropertyDescriptor(object);
			addErrorModelPrototype_targetPropertyDescriptor(object);
			addGround_safetyEvidencePropertyDescriptor(object);
			addGenericConstraint_targetPropertyDescriptor(object);
			addTakeRateConstraint_sourcePropertyDescriptor(object);
			addRealization_realizedBy_identifiable_contextPropertyDescriptor(object);
			addRealization_realizedBy_identifiable_targetPropertyDescriptor(object);
			addUserAttributeableElement_attributedElementPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Category feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCategoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Identifiable_category_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Identifiable_category_feature",
						"_UI_Identifiable_type"),
				IdentifiablePackage.Literals.IDENTIFIABLE__CATEGORY, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Uuid feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUuidPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Identifiable_uuid_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Identifiable_uuid_feature",
						"_UI_Identifiable_type"),
				IdentifiablePackage.Literals.IDENTIFIABLE__UUID, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Configurable Container configurable Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConfigurableContainer_configurableElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_ConfigurableContainer_configurableElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_ConfigurableContainer_configurableElement_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__CONFIGURABLE_CONTAINER_CONFIGURABLE_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Configuration Decision target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConfigurationDecision_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_ConfigurationDecision_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_ConfigurationDecision_target_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__CONFIGURATION_DECISION_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Private Content private Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrivateContent_privateElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_PrivateContent_privateElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_PrivateContent_privateElement_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__PRIVATE_CONTENT_PRIVATE_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Selection Criterion source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionCriterion_sourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_SelectionCriterion_source_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_SelectionCriterion_source_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__SELECTION_CRITERION_SOURCE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Variable Element optional Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVariableElement_optionalElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_VariableElement_optionalElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_VariableElement_optionalElement_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__VARIABLE_ELEMENT_OPTIONAL_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Refine refined By identifiable context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRefine_refinedBy_identifiable_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Refine_refinedBy_identifiable_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Refine_refinedBy_identifiable_context_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__REFINE_REFINED_BY_IDENTIFIABLE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Refine refined By identifiable target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRefine_refinedBy_identifiable_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Refine_refinedBy_identifiable_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Refine_refinedBy_identifiable_target_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__REFINE_REFINED_BY_IDENTIFIABLE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Satisfy satisfied By identifiable target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSatisfy_satisfiedBy_identifiable_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Satisfy_satisfiedBy_identifiable_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Satisfy_satisfiedBy_identifiable_target_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__SATISFY_SATISFIED_BY_IDENTIFIABLE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Satisfy satisfied By identifiable context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSatisfy_satisfiedBy_identifiable_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Satisfy_satisfiedBy_identifiable_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Satisfy_satisfiedBy_identifiable_context_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__SATISFY_SATISFIED_BY_IDENTIFIABLE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Case vv Subject feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVCase_vvSubjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Identifiable_VVCase_vvSubject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Identifiable_VVCase_vvSubject_feature",
						"_UI_Identifiable_type"),
				IdentifiablePackage.Literals.IDENTIFIABLE__VV_CASE_VV_SUBJECT,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the VV Target element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVVTarget_elementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Identifiable_VVTarget_element_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_Identifiable_VVTarget_element_feature",
						"_UI_Identifiable_type"),
				IdentifiablePackage.Literals.IDENTIFIABLE__VV_TARGET_ELEMENT,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Error Model Prototype target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorModelPrototype_targetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_ErrorModelPrototype_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_ErrorModelPrototype_target_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__ERROR_MODEL_PROTOTYPE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Ground safety Evidence feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGround_safetyEvidencePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Ground_safetyEvidence_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Ground_safetyEvidence_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__GROUND_SAFETY_EVIDENCE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Generic Constraint target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGenericConstraint_targetPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_GenericConstraint_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_GenericConstraint_target_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__GENERIC_CONSTRAINT_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Take Rate Constraint source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTakeRateConstraint_sourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_TakeRateConstraint_source_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_TakeRateConstraint_source_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__TAKE_RATE_CONSTRAINT_SOURCE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Realization realized By identifiable context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRealization_realizedBy_identifiable_contextPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Realization_realizedBy_identifiable_context_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Realization_realizedBy_identifiable_context_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__REALIZATION_REALIZED_BY_IDENTIFIABLE_CONTEXT,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Realization realized By identifiable target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRealization_realizedBy_identifiable_targetPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_Realization_realizedBy_identifiable_target_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_Realization_realizedBy_identifiable_target_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__REALIZATION_REALIZED_BY_IDENTIFIABLE_TARGET,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the User Attributeable Element attributed Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUserAttributeableElement_attributedElementPropertyDescriptor(
			Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Identifiable_UserAttributeableElement_attributedElement_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_Identifiable_UserAttributeableElement_attributedElement_feature",
								"_UI_Identifiable_type"),
						IdentifiablePackage.Literals.IDENTIFIABLE__USER_ATTRIBUTEABLE_ELEMENT_ATTRIBUTED_ELEMENT,
						true, false, true, null, null, null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Identifiable) object).getShortName();
		return label == null || label.length() == 0 ? getString("_UI_Identifiable_type")
				: getString("_UI_Identifiable_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Identifiable.class)) {
		case IdentifiablePackage.IDENTIFIABLE__CATEGORY:
		case IdentifiablePackage.IDENTIFIABLE__UUID:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
