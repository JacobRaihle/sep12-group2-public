package test;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.swt.graphics.Color;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.css.RGBColor;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.FunctionConnector_port;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef._instanceRefFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.provider.FunctionConnector_portItemProvider;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.provider._instanceRefItemProviderAdapterFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionConnectorItemProvider;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionModelingItemProviderAdapterFactory;

public class FunctionConnectorTest {

	private FunctionModelingFactory factory = FunctionModelingFactory.eINSTANCE;
	private _instanceRefFactory irFactory = _instanceRefFactory.eINSTANCE;
	protected int notificationCount;
	private FunctionPort port;
	private FunctionPrototype proto;
	private FunctionConnector_port connectorPort;
	
	@Before
	public void setUp() {
		_instanceRefItemProviderAdapterFactory adapterFactory = new _instanceRefItemProviderAdapterFactory();
		FunctionConnector_portItemProvider provider = new FunctionConnector_portItemProvider(
				adapterFactory);
		connectorPort = irFactory.createFunctionConnector_port();
		connectorPort.eAdapters().add(provider);
		connectorPort.eAdapters().add(new AdapterImpl() {
			@Override
			public void notifyChanged(Notification msg) {
				super.notifyChanged(msg);
				notificationCount++;
			}
		});
		port = factory.createFunctionInFlowPort();
		port.setShortName("Port");
		proto = factory.createDesignFunctionPrototype();
		proto.setShortName("Prototype");
	}
	
	@Test
	public void testNotifiedOnPortSet() {
		// Confirm initial state
		assertEquals(0, notificationCount);
		connectorPort.setFunctionPort(port);
		// Confirm that notification was received
		assertEquals(1, notificationCount);
	}
	
	@Test
	public void testNotifiedOnPrototypeSet() {
		// Confirm initial state
		assertEquals(0, notificationCount);
		connectorPort.setFunctionPrototype(proto);
		// Confirm that notification was received
		assertEquals(1, notificationCount);
	}
	
	@Test
	public void testNotifiedOnPortNameChange() {
		// Confirm initial state
		assertEquals(0, notificationCount);
		port.setShortName("New port name");
		// Confirm that notification was received
		assertEquals(1, notificationCount);
	}
	
	@Test
	public void testNotifiedOnPrototypeNameChange() {
		// Confirm initial state
		assertEquals(0, notificationCount);
		proto.setShortName("New prototype name");
		// Confirm that notification was received
		assertEquals(1, notificationCount);
	}
	
	@Test
	public void testIsRedWhenNoPortSet() {
		_instanceRefItemProviderAdapterFactory aFactory = new _instanceRefItemProviderAdapterFactory();
		FunctionConnector_portItemProvider ip = new FunctionConnector_portItemProvider(aFactory);
		ColorDescriptor descriptor = (ColorDescriptor) ip.getForeground(connectorPort);
		Color color = descriptor.createColor(null);
		
		assertEquals(255, color.getRed());
		assertEquals(  0, color.getGreen());
		assertEquals(  0, color.getBlue());
		descriptor.destroyColor(null);	
	}
	
	@Test
	public void testIsBlackWhenPortSet() {
		connectorPort.setFunctionPort(port);
		_instanceRefItemProviderAdapterFactory aFactory = new _instanceRefItemProviderAdapterFactory();
		FunctionConnector_portItemProvider ip = new FunctionConnector_portItemProvider(aFactory);
		ColorDescriptor descriptor = (ColorDescriptor) ip.getForeground(connectorPort);
		assertEquals(null, descriptor);
	}
}
