package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.ieee.eastadleditor.ConnectionList;
import se.chalmers.ieee.eastadleditor.PortList;
import se.chalmers.ieee.eastadleditor.PrototypeList;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionalDevice;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.DesignFunctionTypeItemProvider;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionModelingItemProviderAdapterFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionTypeItemProvider;

/**
 * These tests assure that virtual children are added to the FunctionalDevice
 * The virtual children should be:
 *  * A "Ports" child containing only ports
 *  * A "Connections" child containing only connections
 *  * A "Parts" child containing only prototypes
 *  Other children can exist but should not be ports, connections or parts
 * @author Raihle
 *
 */
public class FunctionalDeviceVirtualChildrenTest extends AbstractTypeVirtualChildrenTest {
	//Types
	private FunctionalDevice device;
	
	private FunctionTypeItemProvider provider;
	
	@Before
	public void setup() {
		FunctionModelingItemProviderAdapterFactory adapterFactory = new FunctionModelingItemProviderAdapterFactory();
		provider = new DesignFunctionTypeItemProvider(adapterFactory);
		device = fmFactory.createFunctionalDevice();
		addConnectedPorts(device);
		device.getPart().add(fmFactory.createAnalysisFunctionPrototype());
	}
	
	@Test
	public void hasNoPortPartConnectionChildren() {
		Collection<?> children = provider.getChildren(device);
		for (Object child : children) {
			assertFalse(child instanceof FunctionPort);
			assertFalse(child instanceof FunctionPrototype);
			assertFalse(child instanceof FunctionConnector);
		}
	}
	
	@Test
	public void hasPortsChild() {
		try {
			PortList ports = getMatchingChild(provider, device, PortList.class);
			assertEquals("Ports", ports.getText(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no port child");
		}
	}
	
	@Test
	public void hasPrototypesChild() {
		try {
			PrototypeList parts = getMatchingChild(provider, device, PrototypeList.class);
			assertEquals("Parts", parts.getText(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no part child");
		}
	}
	
	@Test
	public void hasConnectionsChild() {
		try {
			ConnectionList parts = getMatchingChild(provider, device, ConnectionList.class);
			assertEquals("Connections", parts.getText(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no connection child");
		}
	}
	
	@Test
	public void portChildHasPorts() {
		try {
			PortList ports = getMatchingChild(provider, device, PortList.class);
			assertEquals(device.getPort(), ports.getChildren(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no port child");
		}
	}
	
	@Test
	public void partChildHasParts() {
		try {
			PrototypeList parts = getMatchingChild(provider, device, PrototypeList.class);
			assertEquals(device.getPart(), parts.getChildren(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no part child");
		}
	}
	
	@Test
	public void connectionChildHasConnections() {
		try {
			ConnectionList connections = getMatchingChild(provider, device, ConnectionList.class);
			assertEquals(device.getConnector(), connections.getChildren(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no connection child");
		}
	}
}
