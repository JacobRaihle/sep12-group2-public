package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.junit.Before;
import org.junit.Test;

import DomainModel.EAST_ADL.Infrastructure.Elements.presentation.ElementsActionBarContributor;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.HardwareConnector_port;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef._instanceRefFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.provider.HardwareConnector_portItemProvider;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.provider._instanceRefItemProviderAdapterFactory;

public class ElementsEditorTest {
	
	//FIXME the contributor needs an activeeditorpart in order to produce any actions.
	// Need to spoof that somehow, might be difficult.
	//TODO only asserts that there are no atp features on the main level
	// Still need to check that the sub menu exists if it should
	@Test
	public void atpFeaturesAreInSubMenu() {
		ElementsActionBarContributor c = new ElementsActionBarContributor();
		MenuManager manager = new MenuManager();
		manager.add(new Separator("additions"));
		c.contributeToMenu(manager);
		MenuManager subMenu = (MenuManager) manager.getItems()[1];
		for (IContributionItem item : subMenu.getItems()) {
			if (item instanceof MenuManager) {
				MenuManager subSubMenu = (MenuManager) item;
				System.out.println(subSubMenu.getMenuText());
				if (subSubMenu.getMenuText().equals("&New Child") ||
						subSubMenu.getMenuText().equals("N&ew Sibling")) {
					for (IContributionItem child : subSubMenu.getItems()) {
						System.out.println(child);
						if (child instanceof ActionContributionItem) {
							ActionContributionItem action = (ActionContributionItem) child;
							assertFalse(action.getAction().getText().startsWith("Atp"));
							System.out.println(action.getAction().getText());
						}
					}
				}
			}
		}
	}
	
	
}
