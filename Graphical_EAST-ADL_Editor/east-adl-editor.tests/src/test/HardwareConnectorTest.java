package test;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.swt.graphics.Color;
import org.junit.Before;
import org.junit.Test;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.HardwareConnector_port;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef._instanceRefFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.provider.HardwareConnector_portItemProvider;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.provider._instanceRefItemProviderAdapterFactory;

public class HardwareConnectorTest {

	private HardwareComponentPrototype hardwareComponentPrototype;
	private HardwareConnector_port connectorPort;
	private HardwarePin hardwarePin;
	private int notificationCount;

	@Before
	public void setUp() throws Exception {
		HardwareModelingFactory hardwareModelingFactory = HardwareModelingFactory.eINSTANCE;
		_instanceRefFactory instanceRefFactory = _instanceRefFactory.eINSTANCE;

		// Create Component Prototype
		hardwareComponentPrototype = hardwareModelingFactory
				.createHardwareComponentPrototype();
		hardwareComponentPrototype.setShortName("hardwareComponentPrototype");

		// Create Connection Port
		connectorPort = instanceRefFactory.createHardwareConnector_port();

		// Create Hardware Pin
		hardwarePin = hardwareModelingFactory
				.createCommunicationInHardwarePin();
		hardwarePin.setShortName("hardwarePin");
	}

	@Test
	public final void testGetTextHardwarePinIsSetAndPrototypeIsSet() {
		// SetUp
		connectorPort.setHardwareComponentPrototype(hardwareComponentPrototype);

		connectorPort.setHardwarePin(hardwarePin);

		_instanceRefItemProviderAdapterFactory adapterFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider provider = new HardwareConnector_portItemProvider(
				adapterFactory);

		// Test
		String expectedText = connectorPort.getHardwareComponentPrototype()
				.getShortName()
				+ "."
				+ connectorPort.getHardwarePin().getShortName();
		String actualText = provider.getText(connectorPort);
		assertEquals(expectedText, actualText);
	}

	@Test
	public final void testGetTextHardwarePinIsSetAndPrototypeIsNotSet() {
		// SetUp
		connectorPort.setHardwarePin(hardwarePin);

		_instanceRefItemProviderAdapterFactory adapterFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider provider = new HardwareConnector_portItemProvider(
				adapterFactory);

		// Test
		String expectedText = "this."
				+ connectorPort.getHardwarePin().getShortName();
		String actualText = provider.getText(connectorPort);
		assertEquals(expectedText, actualText);
	}

	@Test
	public final void testGetTextHardwarePinIsNotSetAndPrototypeIsSet() {
		// SetUp
		connectorPort.setHardwareComponentPrototype(hardwareComponentPrototype);

		_instanceRefItemProviderAdapterFactory adapterFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider provider = new HardwareConnector_portItemProvider(
				adapterFactory);

		// Test
		String expectedText = "Warning: Port not set";
		String actualText = provider.getText(connectorPort);
		assertEquals(expectedText, actualText);
	}

	@Test
	public final void testGetTextHardwarePinIsNotSetAndPrototypeIsNotSet() {
		// SetUp
		_instanceRefItemProviderAdapterFactory adapterFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider provider = new HardwareConnector_portItemProvider(
				adapterFactory);

		// Test
		String expectedText = "Warning: Port not set";
		String actualText = provider.getText(connectorPort);
		assertEquals(expectedText, actualText);
	}

	@Test
	public final void testNotifyChangedNotification() {
		_instanceRefItemProviderAdapterFactory adapterFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider provider = new HardwareConnector_portItemProvider(
				adapterFactory);
		connectorPort.eAdapters().add(provider);
		connectorPort.eAdapters().add(new AdapterImpl() {
			@Override
			public void notifyChanged(Notification msg) {
				super.notifyChanged(msg);
				notificationCount++;
			}
		});
		connectorPort.setHardwarePin(hardwarePin);
		assertEquals(1, notificationCount);
		connectorPort.setHardwareComponentPrototype(hardwareComponentPrototype);
		assertEquals(2, notificationCount);
		hardwarePin.setShortName("newName");
		assertEquals(3, notificationCount);
	}
	
	@Test
	public void testIsRedWhenNoPortSet() {
		_instanceRefItemProviderAdapterFactory aFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider ip = new HardwareConnector_portItemProvider(aFactory);
		ColorDescriptor descriptor = (ColorDescriptor) ip.getForeground(connectorPort);
		Color color = descriptor.createColor(null);
		
		assertEquals(255, color.getRed());
		assertEquals(  0, color.getGreen());
		assertEquals(  0, color.getBlue());
	}

	@Test
	public void testIsBlackWhenPortSet() {
		connectorPort.setHardwarePin(hardwarePin);
		_instanceRefItemProviderAdapterFactory aFactory = new _instanceRefItemProviderAdapterFactory();
		HardwareConnector_portItemProvider ip = new HardwareConnector_portItemProvider(aFactory);
		ColorDescriptor descriptor = (ColorDescriptor) ip.getForeground(connectorPort);
		assertEquals(null, descriptor);
	}
}
