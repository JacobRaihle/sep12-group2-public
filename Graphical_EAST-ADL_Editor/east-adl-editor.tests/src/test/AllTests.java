package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	FunctionConnectorTest.class, 
	PrototypeVirtualChildrenTest.class, 
	HardwareConnectorTest.class, 
	DesignTypeVirtualChildrenTest.class,
	AnalysisTypeVirtualChildrenTest.class,
	FunctionalDeviceVirtualChildrenTest.class,
	HardwareTypeVirtualChildrenTest.class,
	ElementsEditorTest.class
	
})
public class AllTests {

}
