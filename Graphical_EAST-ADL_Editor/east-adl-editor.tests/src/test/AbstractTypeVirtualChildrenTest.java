package test;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.edit.provider.ItemProviderAdapter;

import se.chalmers.ieee.eastadleditor.PortList;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.FunctionConnector_port;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef._instanceRefFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.DesignFunctionTypeItemProvider;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionModelingItemProviderAdapterFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionTypeItemProvider;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentType;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareConnector;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;
import DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef.HardwareConnector_port;
import DomainModel.EAST_ADL.Structure.HardwareModeling.provider.HardwareModelingItemProviderAdapterFactory;

/**
 * @author Raihle
 *
 */
public abstract class AbstractTypeVirtualChildrenTest {
	protected FunctionModelingFactory fmFactory = FunctionModelingFactory.eINSTANCE;
	protected _instanceRefFactory firFactory = _instanceRefFactory.eINSTANCE;
	protected HardwareModelingFactory hmFactory = HardwareModelingFactory.eINSTANCE;
	protected DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef._instanceRefFactory hirFactory = DomainModel.EAST_ADL.Structure.HardwareModeling._instanceRef._instanceRefFactory.eINSTANCE; 
	protected FunctionModelingItemProviderAdapterFactory fmAdapterFactory = new FunctionModelingItemProviderAdapterFactory();
	protected HardwareModelingItemProviderAdapterFactory hmAdapterFactory = new HardwareModelingItemProviderAdapterFactory();
	//FunctionTypeItemProvider provider = new DesignFunctionTypeItemProvider(adapterFactory);
	
	/**
	 * @param type the FunctionType to add ports to
	 * @return the ports added
	 */
	protected void addConnectedPorts(FunctionType type) {
		FunctionPort inPort = fmFactory.createFunctionInFlowPort();
		FunctionPort outPort = fmFactory.createFunctionOutFlowPort();
		FunctionConnector c = fmFactory.createFunctionConnector();
		FunctionConnector_port p1 = firFactory.createFunctionConnector_port();
		FunctionConnector_port p2 = firFactory.createFunctionConnector_port();
		
		type.getPort().add(inPort);
		type.getPort().add(outPort);
		p1.setFunctionPort(inPort);
		p2.setFunctionPort(outPort);
		c.getPort().add(p1);
		c.getPort().add(p2);
		type.getConnector().add(c);
	}
	
	/**
	 * @param type the HardwareComponentType to add ports to
	 * @return the ports added
	 */
	protected void addConnectedPorts(HardwareComponentType type) {
		HardwarePin inPort = hmFactory.createCommunicationInHardwarePin();
		HardwarePin outPort = hmFactory.createCommunicationOutHardwarePin();
		HardwareConnector c = hmFactory.createHardwareConnector();
		HardwareConnector_port p1 = hirFactory.createHardwareConnector_port();
		HardwareConnector_port p2 = hirFactory.createHardwareConnector_port();
		
		type.getPort().add(inPort);
		type.getPort().add(outPort);
		p1.setHardwarePin(inPort);
		p2.setHardwarePin(outPort);
		c.getPort().add(p1);
		c.getPort().add(p2);
		type.getConnector().add(c);
	}
	
	protected <E> E getMatchingChild(ItemProviderAdapter provider, Object object, Class<E> klass) {
		for (Object child : provider.getChildren(object)) {
			if (klass.isAssignableFrom(child.getClass())) {
				return (E) child;
			}
		}
		throw new IllegalArgumentException("Parent has no child of the given type");
	}
}
