package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.ieee.eastadleditor.ConnectionList;
import se.chalmers.ieee.eastadleditor.PortList;
import se.chalmers.ieee.eastadleditor.PrototypeList;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionConnector;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentType;
import DomainModel.EAST_ADL.Structure.HardwareModeling.provider.HardwareComponentTypeItemProvider;
import DomainModel.EAST_ADL.Structure.HardwareModeling.provider.HardwareModelingItemProviderAdapterFactory;

/**
 * These tests assure that virtual children are added to the HardwareComponentType
 * The virtual children should be:
 *  * A "Ports" child containing only ports
 *  * A "Connections" child containing only connections
 *  * A "Parts" child containing only prototypes
 *  Other children can exist but should not be ports, connections or parts
 * @author Raihle
 *
 */
public class HardwareTypeVirtualChildrenTest extends AbstractTypeVirtualChildrenTest {
	//Types
	private HardwareComponentType type;
	
	private HardwareComponentTypeItemProvider provider;
	
	@Before
	public void setup() {
		HardwareModelingItemProviderAdapterFactory adapterFactory = new HardwareModelingItemProviderAdapterFactory();
		provider = new HardwareComponentTypeItemProvider(adapterFactory);
		type = hmFactory.createHardwareComponentType();
		addConnectedPorts(type);
		type.getPart().add(hmFactory.createHardwareComponentPrototype());
	}
	
	@Test
	public void hasNoPortPartConnectionChildren() {
		Collection<?> children = provider.getChildren(type);
		for (Object child : children) {
			assertFalse(child instanceof FunctionPort);
			assertFalse(child instanceof FunctionPrototype);
			assertFalse(child instanceof FunctionConnector);
		}
	}
	
	@Test
	public void hasPortsChild() {
		try {
			PortList ports = getMatchingChild(provider, type, PortList.class);
			assertEquals("Ports", ports.getText(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no port child");
		}
	}
	
	@Test
	public void hasPrototypesChild() {
		try {
			PrototypeList parts = getMatchingChild(provider, type, PrototypeList.class);
			assertEquals("Parts", parts.getText(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no part child");
		}
	}
	
	@Test
	public void hasConnectionsChild() {
		try {
			ConnectionList parts = getMatchingChild(provider, type, ConnectionList.class);
			assertEquals("Connections", parts.getText(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no connection child");
		}
	}
	
	@Test
	public void portChildHasPorts() {
		try {
			PortList ports = getMatchingChild(provider, type, PortList.class);
			assertEquals(type.getPort(), ports.getChildren(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no port child");
		}
	}
	
	@Test
	public void partChildHasParts() {
		try {
			PrototypeList parts = getMatchingChild(provider, type, PrototypeList.class);
			assertEquals(type.getPart(), parts.getChildren(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no part child");
		}
	}
	
	@Test
	public void connectionChildHasConnections() {
		try {
			ConnectionList connections = getMatchingChild(provider, type, ConnectionList.class);
			assertEquals(type.getConnector(), connections.getChildren(null));
		} catch (IllegalArgumentException e) {
			fail("Type has no connection child");
		}
	}
}
