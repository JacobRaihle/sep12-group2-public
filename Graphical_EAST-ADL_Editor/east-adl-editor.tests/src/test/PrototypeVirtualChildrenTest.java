package test;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Random;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.junit.Before;
import org.junit.Test;

import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.AnalysisFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionPrototype;
import DomainModel.EAST_ADL.Structure.FunctionModeling.DesignFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionPort;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.AnalysisFunctionPrototypeItemProvider;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.DesignFunctionPrototypeItemProvider;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionModelingItemProviderAdapterFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.provider.FunctionPrototypeItemProvider;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentPrototype;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareComponentType;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;
import DomainModel.EAST_ADL.Structure.HardwareModeling.provider.HardwareComponentPrototypeItemProvider;
import DomainModel.EAST_ADL.Structure.HardwareModeling.provider.HardwareModelingItemProviderAdapterFactory;

/**
 * These tests assure that virtual children are added to the following elements:
 *  * AnalysisFunctionPrototype
 *  * DesignFunctionPrototype
 *  * HardwareComponentPrototype
 * The virtual children should show the following information:
 *  * The Type that the Prototype is instantiating
 *    * There is no need to test for Functional Devices as prototypes treat them the same as a normal Function Type
 *  * The Ports or Pins of that Type
 * @author Raihle
 *
 */
public class PrototypeVirtualChildrenTest {
	//Factories
	private FunctionModelingFactory fmFactory = FunctionModelingFactory.eINSTANCE;
	private HardwareModelingFactory hmFactory = HardwareModelingFactory.eINSTANCE;
	//Types
	private DesignFunctionType dType;
	private AnalysisFunctionType aType;
	private HardwareComponentType hType;
	//Prototypes
	private DesignFunctionPrototype dProto;
	private AnalysisFunctionPrototype aProto;
	private HardwareComponentPrototype hProto;
	
	@Before
	public void setup() {
		dType = fmFactory.createDesignFunctionType();
		addPorts(dType);
		dProto = fmFactory.createDesignFunctionPrototype();
		dProto.setType(dType);
		
		aType = fmFactory.createAnalysisFunctionType();
		addPorts(aType);
		aProto = fmFactory.createAnalysisFunctionPrototype();
		aProto.setType(aType);
		
		hType = hmFactory.createHardwareComponentType();
		addPorts(hType);
		hProto = hmFactory.createHardwareComponentPrototype();
		hProto.setType(hType);
	}
	
	/**
	 * @param type the FunctionType to add ports to
	 * @return the ports added
	 */
	private void addPorts(FunctionType type) {
		EList<FunctionPort> ports = type.getPort();
		int inPorts = new Random().nextInt(50) + 1;
		for (int i = 0; i < inPorts; i++) {
			FunctionPort port = fmFactory.createFunctionInFlowPort();
			ports.add(port);
		}
		int outPorts = new Random().nextInt(50) + 1;
		for (int i = 0; i < outPorts; i++) {
			FunctionPort port = fmFactory.createFunctionOutFlowPort();
			ports.add(port);
		}
	}
	
	/**
	 * @param type the HardwareComponentType to add ports to
	 * @return the ports added
	 */
	private void addPorts(HardwareComponentType type) {
		EList<HardwarePin> ports = type.getPort();
		int inPorts = new Random().nextInt(50) + 1;
		for (int i = 0; i < inPorts; i++) {
			HardwarePin port = hmFactory.createCommunicationInHardwarePin();
			ports.add(port);
		}
		int outPorts = new Random().nextInt(50) + 1;
		for (int i = 0; i < outPorts; i++) {
			HardwarePin port = hmFactory.createCommunicationOutHardwarePin();
			ports.add(port);
		}
	}
	
	private void confirmPorts(EList<? extends EObject> ports, ItemProviderAdapter provider, Object prototype) {
		Collection<?> protoChildren = provider.getChildren(prototype);
		for (EObject port : ports) {
			assertTrue(protoChildren.contains(port));
		}
	}
	
	@Test
	public void designPrototypeShowsPorts() {
		FunctionModelingItemProviderAdapterFactory adapterFactory = new FunctionModelingItemProviderAdapterFactory();
		FunctionPrototypeItemProvider provider = new DesignFunctionPrototypeItemProvider(adapterFactory);
		confirmPorts(dType.getPort(), provider, dProto);
	}
	
	@Test
	public void analysisPrototypeShowsPorts() {
		FunctionModelingItemProviderAdapterFactory adapterFactory = new FunctionModelingItemProviderAdapterFactory();
		FunctionPrototypeItemProvider provider = new AnalysisFunctionPrototypeItemProvider(adapterFactory);
		
		confirmPorts(aType.getPort(), provider, aProto);
	}
	
	@Test
	public void hardwarePrototypeShowsPorts() {
		HardwareModelingItemProviderAdapterFactory adapterFactory = new HardwareModelingItemProviderAdapterFactory();
		HardwareComponentPrototypeItemProvider provider = new HardwareComponentPrototypeItemProvider(adapterFactory);
		
		confirmPorts(hType.getPort(), provider, hProto);
	}
	
	@Test
	public void designPrototypeShowsType() {
		FunctionModelingItemProviderAdapterFactory adapterFactory = new FunctionModelingItemProviderAdapterFactory();
		FunctionPrototypeItemProvider provider = new DesignFunctionPrototypeItemProvider(adapterFactory);
		Collection<?> protoChildren = provider.getChildren(dProto);
		
		assertTrue(protoChildren.contains(dType));
	}
	
	@Test
	public void analysisPrototypeShowsType() {
		FunctionModelingItemProviderAdapterFactory adapterFactory = new FunctionModelingItemProviderAdapterFactory();
		AnalysisFunctionPrototypeItemProvider provider = new AnalysisFunctionPrototypeItemProvider(adapterFactory);
		Collection<?> protoChildren = provider.getChildren(aProto);
		
		assertTrue(protoChildren.contains(aType));
	}
	
	@Test
	public void hardwarePrototypeShowsType() {
		HardwareModelingItemProviderAdapterFactory adapterFactory = new HardwareModelingItemProviderAdapterFactory();
		HardwareComponentPrototypeItemProvider provider = new HardwareComponentPrototypeItemProvider(adapterFactory);
		Collection<?> protoChildren = provider.getChildren(hProto);
		
		assertTrue(protoChildren.contains(hType));
	}
}
