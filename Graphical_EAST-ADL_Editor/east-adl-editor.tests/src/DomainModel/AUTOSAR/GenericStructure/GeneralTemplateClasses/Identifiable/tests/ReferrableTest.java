/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.tests;

import DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.Referrable;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Referrable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ReferrableTest extends TestCase {

	/**
	 * The fixture for this Referrable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Referrable fixture = null;

	/**
	 * Constructs a new Referrable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferrableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Referrable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Referrable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Referrable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Referrable getFixture() {
		return fixture;
	}

} //ReferrableTest
