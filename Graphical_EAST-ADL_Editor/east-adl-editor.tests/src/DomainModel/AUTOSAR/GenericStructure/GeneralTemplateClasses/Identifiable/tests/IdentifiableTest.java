/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.tests;

import DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.Identifiable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Identifiable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class IdentifiableTest extends ReferrableTest {

	/**
	 * Constructs a new Identifiable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifiableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Identifiable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Identifiable getFixture() {
		return (Identifiable) fixture;
	}

} //IdentifiableTest
