/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.ARElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>AR Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ARElementTest extends PackageableElementTest {

	/**
	 * Constructs a new AR Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ARElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this AR Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ARElement getFixture() {
		return (ARElement) fixture;
	}

} //ARElementTest
