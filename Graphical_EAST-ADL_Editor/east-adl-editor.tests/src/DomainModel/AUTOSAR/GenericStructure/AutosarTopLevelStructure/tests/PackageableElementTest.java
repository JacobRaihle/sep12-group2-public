/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.PackageableElement;

import DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.tests.IdentifiableTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Packageable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class PackageableElementTest extends IdentifiableTest {

	/**
	 * Constructs a new Packageable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Packageable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PackageableElement getFixture() {
		return (PackageableElement) fixture;
	}

} //PackageableElementTest
