/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.AUTOSAR;
import DomainModel.AUTOSAR.GenericStructure.AutosarTopLevelStructure.AutosarTopLevelStructureFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>AUTOSAR</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AUTOSARTest extends TestCase {

	/**
	 * The fixture for this AUTOSAR test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AUTOSAR fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AUTOSARTest.class);
	}

	/**
	 * Constructs a new AUTOSAR test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AUTOSARTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this AUTOSAR test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(AUTOSAR fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this AUTOSAR test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AUTOSAR getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AutosarTopLevelStructureFactory.eINSTANCE.createAUTOSAR());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AUTOSARTest
