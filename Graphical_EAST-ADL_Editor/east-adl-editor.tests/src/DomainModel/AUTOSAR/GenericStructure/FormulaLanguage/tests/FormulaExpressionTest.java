/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.FormulaLanguage.tests;

import DomainModel.AUTOSAR.GenericStructure.FormulaLanguage.FormulaExpression;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Formula Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FormulaExpressionTest extends TestCase {

	/**
	 * The fixture for this Formula Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormulaExpression fixture = null;

	/**
	 * Constructs a new Formula Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormulaExpressionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Formula Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FormulaExpression fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Formula Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormulaExpression getFixture() {
		return fixture;
	}

} //FormulaExpressionTest
