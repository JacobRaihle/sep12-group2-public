/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Atp Instance Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#getAtpBase() <em>Atp Base</em>}</li>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#getAtpContextElement() <em>Atp Context Element</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AtpInstanceRefTest extends TestCase {

	/**
	 * The fixture for this Atp Instance Ref test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtpInstanceRef fixture = null;

	/**
	 * Constructs a new Atp Instance Ref test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtpInstanceRefTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Atp Instance Ref test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(AtpInstanceRef fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Atp Instance Ref test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtpInstanceRef getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#getAtpBase() <em>Atp Base</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#getAtpBase()
	 * @generated
	 */
	public void testGetAtpBase() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#setAtpBase(DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier) <em>Atp Base</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#setAtpBase(DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier)
	 * @generated
	 */
	public void testSetAtpBase() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#getAtpContextElement() <em>Atp Context Element</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpInstanceRef#getAtpContextElement()
	 * @generated
	 */
	public void testGetAtpContextElement() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AtpInstanceRefTest
