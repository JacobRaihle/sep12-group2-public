/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpStructureElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Atp Structure Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpFeature() <em>Atp Feature</em>}</li>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpInstanceRef_atpBase() <em>Atp Instance Ref atp Base</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AtpStructureElementTest extends AtpFeatureTest {

	/**
	 * Constructs a new Atp Structure Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtpStructureElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Atp Structure Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AtpStructureElement getFixture() {
		return (AtpStructureElement) fixture;
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpFeature() <em>Atp Feature</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpFeature()
	 * @generated
	 */
	public void testGetAtpFeature() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpInstanceRef_atpBase() <em>Atp Instance Ref atp Base</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpInstanceRef_atpBase()
	 * @generated
	 */
	public void testGetAtpInstanceRef_atpBase() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AtpStructureElementTest
