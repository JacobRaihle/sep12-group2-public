/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpPrototype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Atp Prototype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AtpPrototypeTest extends AtpFeatureTest {

	/**
	 * Constructs a new Atp Prototype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtpPrototypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Atp Prototype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AtpPrototype getFixture() {
		return (AtpPrototype) fixture;
	}

} //AtpPrototypeTest
