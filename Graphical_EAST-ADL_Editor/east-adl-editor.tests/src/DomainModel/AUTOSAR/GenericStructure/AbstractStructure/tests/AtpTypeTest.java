/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Atp Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AtpTypeTest extends AtpClassifierTest {

	/**
	 * Constructs a new Atp Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtpTypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Atp Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AtpType getFixture() {
		return (AtpType) fixture;
	}

} //AtpTypeTest
