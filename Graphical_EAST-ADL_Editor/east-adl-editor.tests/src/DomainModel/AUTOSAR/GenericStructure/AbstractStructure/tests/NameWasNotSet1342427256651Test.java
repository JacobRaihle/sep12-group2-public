/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AbstractStructureFactory;
import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.NameWasNotSet1342427256651;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Name Was Not Set1342427256651</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class NameWasNotSet1342427256651Test extends TestCase {

	/**
	 * The fixture for this Name Was Not Set1342427256651 test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NameWasNotSet1342427256651 fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(NameWasNotSet1342427256651Test.class);
	}

	/**
	 * Constructs a new Name Was Not Set1342427256651 test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameWasNotSet1342427256651Test(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Name Was Not Set1342427256651 test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(NameWasNotSet1342427256651 fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Name Was Not Set1342427256651 test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NameWasNotSet1342427256651 getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AbstractStructureFactory.eINSTANCE
				.createNameWasNotSet1342427256651());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //NameWasNotSet1342427256651Test
