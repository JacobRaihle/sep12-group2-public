/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements.VerificationValidation.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.TraceableSpecificationTest;

import DomainModel.EAST_ADL.Requirements.VerificationValidation.VVProcedure;
import DomainModel.EAST_ADL.Requirements.VerificationValidation.VerificationValidationFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>VV Procedure</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class VVProcedureTest extends TraceableSpecificationTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VVProcedureTest.class);
	}

	/**
	 * Constructs a new VV Procedure test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VVProcedureTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this VV Procedure test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected VVProcedure getFixture() {
		return (VVProcedure) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(VerificationValidationFactory.eINSTANCE.createVVProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //VVProcedureTest
