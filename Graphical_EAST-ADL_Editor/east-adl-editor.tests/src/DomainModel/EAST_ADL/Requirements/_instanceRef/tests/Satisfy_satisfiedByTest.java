/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements._instanceRef.tests;

import DomainModel.EAST_ADL.Requirements._instanceRef.Satisfy_satisfiedBy;
import DomainModel.EAST_ADL.Requirements._instanceRef._instanceRefFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Satisfy satisfied By</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class Satisfy_satisfiedByTest extends TestCase {

	/**
	 * The fixture for this Satisfy satisfied By test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Satisfy_satisfiedBy fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(Satisfy_satisfiedByTest.class);
	}

	/**
	 * Constructs a new Satisfy satisfied By test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Satisfy_satisfiedByTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Satisfy satisfied By test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Satisfy_satisfiedBy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Satisfy satisfied By test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Satisfy_satisfiedBy getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(_instanceRefFactory.eINSTANCE.createSatisfy_satisfiedBy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //Satisfy_satisfiedByTest
