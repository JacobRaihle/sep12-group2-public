/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Requirements.UseCases.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAElementTest;

import DomainModel.EAST_ADL.Requirements.UseCases.RedefinableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Redefinable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class RedefinableElementTest extends EAElementTest {

	/**
	 * Constructs a new Redefinable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RedefinableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Redefinable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected RedefinableElement getFixture() {
		return (RedefinableElement) fixture;
	}

} //RedefinableElementTest
