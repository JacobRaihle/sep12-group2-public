/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.SafetyConstraints._instanceRef.tests;

import DomainModel.EAST_ADL.Dependability.SafetyConstraints._instanceRef.FaultFailure_anomaly;
import DomainModel.EAST_ADL.Dependability.SafetyConstraints._instanceRef._instanceRefFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Fault Failure anomaly</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FaultFailure_anomalyTest extends TestCase {

	/**
	 * The fixture for this Fault Failure anomaly test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FaultFailure_anomaly fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FaultFailure_anomalyTest.class);
	}

	/**
	 * Constructs a new Fault Failure anomaly test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultFailure_anomalyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Fault Failure anomaly test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FaultFailure_anomaly fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Fault Failure anomaly test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FaultFailure_anomaly getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(_instanceRefFactory.eINSTANCE.createFaultFailure_anomaly());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FaultFailure_anomalyTest
