/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.SafetyRequirement.tests;

import DomainModel.EAST_ADL.Dependability.SafetyRequirement.SafetyRequirementFactory;
import DomainModel.EAST_ADL.Dependability.SafetyRequirement.TechnicalSafetyConcept;

import DomainModel.EAST_ADL.Requirements.tests.RequirementsContainerTest;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Technical Safety Concept</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TechnicalSafetyConceptTest extends RequirementsContainerTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TechnicalSafetyConceptTest.class);
	}

	/**
	 * Constructs a new Technical Safety Concept test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TechnicalSafetyConceptTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Technical Safety Concept test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TechnicalSafetyConcept getFixture() {
		return (TechnicalSafetyConcept) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SafetyRequirementFactory.eINSTANCE
				.createTechnicalSafetyConcept());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TechnicalSafetyConceptTest
