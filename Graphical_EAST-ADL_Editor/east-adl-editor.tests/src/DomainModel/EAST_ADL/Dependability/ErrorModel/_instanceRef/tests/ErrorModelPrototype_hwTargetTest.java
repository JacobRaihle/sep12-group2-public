/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef.tests;

import DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef.ErrorModelPrototype_hwTarget;
import DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef._instanceRefFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Error Model Prototype hw Target</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ErrorModelPrototype_hwTargetTest extends TestCase {

	/**
	 * The fixture for this Error Model Prototype hw Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ErrorModelPrototype_hwTarget fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ErrorModelPrototype_hwTargetTest.class);
	}

	/**
	 * Constructs a new Error Model Prototype hw Target test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorModelPrototype_hwTargetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Error Model Prototype hw Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ErrorModelPrototype_hwTarget fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Error Model Prototype hw Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ErrorModelPrototype_hwTarget getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(_instanceRefFactory.eINSTANCE
				.createErrorModelPrototype_hwTarget());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ErrorModelPrototype_hwTargetTest
