/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef.tests;

import DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef.FaultFailurePropagationLink_fromPort;
import DomainModel.EAST_ADL.Dependability.ErrorModel._instanceRef._instanceRefFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Fault Failure Propagation Link from Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FaultFailurePropagationLink_fromPortTest extends TestCase {

	/**
	 * The fixture for this Fault Failure Propagation Link from Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FaultFailurePropagationLink_fromPort fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FaultFailurePropagationLink_fromPortTest.class);
	}

	/**
	 * Constructs a new Fault Failure Propagation Link from Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultFailurePropagationLink_fromPortTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Fault Failure Propagation Link from Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FaultFailurePropagationLink_fromPort fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Fault Failure Propagation Link from Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FaultFailurePropagationLink_fromPort getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(_instanceRefFactory.eINSTANCE
				.createFaultFailurePropagationLink_fromPort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FaultFailurePropagationLink_fromPortTest
