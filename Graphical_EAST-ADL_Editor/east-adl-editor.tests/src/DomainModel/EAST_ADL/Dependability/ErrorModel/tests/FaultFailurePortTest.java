/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Dependability.ErrorModel.tests;

import DomainModel.EAST_ADL.Dependability.ErrorModel.FaultFailurePort;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Fault Failure Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FaultFailurePortTest extends AnomalyTest {

	/**
	 * Constructs a new Fault Failure Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultFailurePortTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Fault Failure Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FaultFailurePort getFixture() {
		return (FaultFailurePort) fixture;
	}

} //FaultFailurePortTest
