/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Variability.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAElementTest;

import DomainModel.EAST_ADL.Variability.ConfigurationDecisionModelEntry;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Configuration Decision Model Entry</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ConfigurationDecisionModelEntryTest extends EAElementTest {

	/**
	 * Constructs a new Configuration Decision Model Entry test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationDecisionModelEntryTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Configuration Decision Model Entry test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ConfigurationDecisionModelEntry getFixture() {
		return (ConfigurationDecisionModelEntry) fixture;
	}

} //ConfigurationDecisionModelEntryTest
