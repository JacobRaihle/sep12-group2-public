/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Timing.TimingConstraints.tests;

import DomainModel.EAST_ADL.Timing.TimingConstraints.DelayConstraint;

import DomainModel.EAST_ADL.Timing.tests.TimingConstraintTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Delay Constraint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DelayConstraintTest extends TimingConstraintTest {

	/**
	 * Constructs a new Delay Constraint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelayConstraintTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Delay Constraint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DelayConstraint getFixture() {
		return (DelayConstraint) fixture;
	}

} //DelayConstraintTest
