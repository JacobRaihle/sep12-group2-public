/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.GenericConstraints.tests;

import DomainModel.EAST_ADL.GenericConstraints.GenericConstraintSet;
import DomainModel.EAST_ADL.GenericConstraints.GenericConstraintsFactory;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.ContextTest;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Generic Constraint Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class GenericConstraintSetTest extends ContextTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(GenericConstraintSetTest.class);
	}

	/**
	 * Constructs a new Generic Constraint Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericConstraintSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Generic Constraint Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected GenericConstraintSet getFixture() {
		return (GenericConstraintSet) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(GenericConstraintsFactory.eINSTANCE
				.createGenericConstraintSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //GenericConstraintSetTest
