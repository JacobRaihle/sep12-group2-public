/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.Elements.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.TraceableSpecification;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Traceable Specification</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class TraceableSpecificationTest extends
		EAPackageableElementTest {

	/**
	 * Constructs a new Traceable Specification test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TraceableSpecificationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Traceable Specification test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TraceableSpecification getFixture() {
		return (TraceableSpecification) fixture;
	}

} //TraceableSpecificationTest
