/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.Elements.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.EAPackageableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EA Packageable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class EAPackageableElementTest extends EAElementTest {

	/**
	 * Constructs a new EA Packageable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAPackageableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this EA Packageable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EAPackageableElement getFixture() {
		return (EAPackageableElement) fixture;
	}

} //EAPackageableElementTest
