/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.Elements.tests;

import DomainModel.AUTOSAR.GenericStructure.GeneralTemplateClasses.Identifiable.tests.IdentifiableTest;

import DomainModel.EAST_ADL.Infrastructure.Elements.EAElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EA Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class EAElementTest extends IdentifiableTest {

	/**
	 * Constructs a new EA Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this EA Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EAElement getFixture() {
		return (EAElement) fixture;
	}

} //EAElementTest
