/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.Datatypes.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Datatypes</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class DatatypesTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new DatatypesTests("Datatypes Tests");
		suite.addTestSuite(CompositeDatatypeTest.class);
		suite.addTestSuite(EABooleanTest.class);
		suite.addTestSuite(EADatatypePrototypeTest.class);
		suite.addTestSuite(EAFloatTest.class);
		suite.addTestSuite(EAIntegerTest.class);
		suite.addTestSuite(EAStringTest.class);
		suite.addTestSuite(EnumerationTest.class);
		suite.addTestSuite(EnumerationValueTypeTest.class);
		suite.addTestSuite(RangeableValueTypeTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatatypesTests(String name) {
		super(name);
	}

} //DatatypesTests
