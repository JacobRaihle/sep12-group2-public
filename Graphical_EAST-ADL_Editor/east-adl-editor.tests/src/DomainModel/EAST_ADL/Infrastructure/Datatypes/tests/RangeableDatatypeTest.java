/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Infrastructure.Datatypes.tests;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.RangeableDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Rangeable Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class RangeableDatatypeTest extends EADatatypeTest {

	/**
	 * Constructs a new Rangeable Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeableDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Rangeable Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected RangeableDatatype getFixture() {
		return (RangeableDatatype) fixture;
	}

} //RangeableDatatypeTest
