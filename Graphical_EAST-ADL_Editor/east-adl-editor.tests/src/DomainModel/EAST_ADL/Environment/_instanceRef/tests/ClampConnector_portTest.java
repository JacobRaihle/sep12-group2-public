/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Environment._instanceRef.tests;

import DomainModel.EAST_ADL.Environment._instanceRef.ClampConnector_port;
import DomainModel.EAST_ADL.Environment._instanceRef._instanceRefFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Clamp Connector port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClampConnector_portTest extends TestCase {

	/**
	 * The fixture for this Clamp Connector port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClampConnector_port fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ClampConnector_portTest.class);
	}

	/**
	 * Constructs a new Clamp Connector port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClampConnector_portTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Clamp Connector port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ClampConnector_port fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Clamp Connector port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClampConnector_port getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(_instanceRefFactory.eINSTANCE.createClampConnector_port());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ClampConnector_portTest
