/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Annexes.BehaviorConstraints.tests;

import DomainModel.EAST_ADL.Annexes.BehaviorConstraints.BehaviorConstraint;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAElementTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Behavior Constraint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BehaviorConstraintTest extends EAElementTest {

	/**
	 * Constructs a new Behavior Constraint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BehaviorConstraintTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Behavior Constraint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BehaviorConstraint getFixture() {
		return (BehaviorConstraint) fixture;
	}

} //BehaviorConstraintTest
