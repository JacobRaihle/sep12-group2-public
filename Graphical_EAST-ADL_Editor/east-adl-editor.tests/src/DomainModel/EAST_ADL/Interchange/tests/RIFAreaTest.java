/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Interchange.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.ContextTest;

import DomainModel.EAST_ADL.Interchange.RIFArea;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>RIF Area</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class RIFAreaTest extends ContextTest {

	/**
	 * Constructs a new RIF Area test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RIFAreaTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this RIF Area test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected RIFArea getFixture() {
		return (RIFArea) fixture;
	}

} //RIFAreaTest
