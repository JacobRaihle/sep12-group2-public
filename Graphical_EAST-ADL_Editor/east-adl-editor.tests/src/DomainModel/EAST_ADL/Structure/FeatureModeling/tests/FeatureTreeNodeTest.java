/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FeatureModeling.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.ContextTest;

import DomainModel.EAST_ADL.Structure.FeatureModeling.FeatureTreeNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Feature Tree Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FeatureTreeNodeTest extends ContextTest {

	/**
	 * Constructs a new Feature Tree Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureTreeNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Feature Tree Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FeatureTreeNode getFixture() {
		return (FeatureTreeNode) fixture;
	}

} //FeatureTreeNodeTest
