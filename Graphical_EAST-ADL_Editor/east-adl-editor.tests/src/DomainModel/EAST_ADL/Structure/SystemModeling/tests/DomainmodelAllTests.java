/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.SystemModeling.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests.AbstractStructureTests;

import DomainModel.AUTOSAR.SystemTemplate.tests.SystemTemplateTests;

import DomainModel.EAST_ADL.Dependability.ErrorModel.tests.ErrorModelTests;

import DomainModel.EAST_ADL.Environment.tests.EnvironmentTests;

import DomainModel.EAST_ADL.Infrastructure.Datatypes.tests.DatatypesTests;

import DomainModel.EAST_ADL.Structure.FeatureModeling.tests.FeatureModelingTests;

import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.tests._instanceRefTests;

import DomainModel.EAST_ADL.Structure.FunctionModeling.tests.FunctionModelingTests;

import DomainModel.EAST_ADL.Structure.HardwareModeling.tests.HardwareModelingTests;

import DomainModel.EAST_ADL.Structure.VehicleFeatureModeling.tests.VehicleFeatureModelingTests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Domainmodel</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class DomainmodelAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new DomainmodelAllTests("Domainmodel Tests");
		suite.addTest(SystemModelingTests.suite());
		suite.addTest(FeatureModelingTests.suite());
		suite.addTest(VehicleFeatureModelingTests.suite());
		suite.addTest(FunctionModelingTests.suite());
		suite.addTest(_instanceRefTests.suite());
		suite.addTest(HardwareModelingTests.suite());
		suite.addTest(EnvironmentTests.suite());
		suite.addTest(ErrorModelTests.suite());
		suite.addTest(DatatypesTests.suite());
		suite.addTest(AbstractStructureTests.suite());
		suite.addTest(SystemTemplateTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainmodelAllTests(String name) {
		super(name);
	}

} //DomainmodelAllTests
