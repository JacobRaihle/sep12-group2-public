/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAElementTest;

import DomainModel.EAST_ADL.Structure.HardwareModeling.AllocationTarget;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Allocation Target</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AllocationTargetTest extends EAElementTest {

	/**
	 * Constructs a new Allocation Target test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationTargetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Allocation Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AllocationTarget getFixture() {
		return (AllocationTarget) fixture;
	}

} //AllocationTargetTest
