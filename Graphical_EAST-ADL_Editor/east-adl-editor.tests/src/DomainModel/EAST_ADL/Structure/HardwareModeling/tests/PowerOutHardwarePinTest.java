/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.tests;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwareModelingFactory;
import DomainModel.EAST_ADL.Structure.HardwareModeling.PowerOutHardwarePin;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Power Out Hardware Pin</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PowerOutHardwarePinTest extends PowerHardwarePinTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PowerOutHardwarePinTest.class);
	}

	/**
	 * Constructs a new Power Out Hardware Pin test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerOutHardwarePinTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Power Out Hardware Pin test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PowerOutHardwarePin getFixture() {
		return (PowerOutHardwarePin) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(HardwareModelingFactory.eINSTANCE
				.createPowerOutHardwarePin());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PowerOutHardwarePinTest
