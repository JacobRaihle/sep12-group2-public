/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAElementTest;

import DomainModel.EAST_ADL.Structure.HardwareModeling.HardwarePin;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Hardware Pin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#getAtpClassifier_atpFeature() <em>Atp Classifier atp Feature</em>}</li>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#getAtpInstanceRef_atpContextElement() <em>Atp Instance Ref atp Context Element</em>}</li>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpFeature() <em>Atp Feature</em>}</li>
 *   <li>{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpInstanceRef_atpBase() <em>Atp Instance Ref atp Base</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class HardwarePinTest extends EAElementTest {

	/**
	 * Constructs a new Hardware Pin test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HardwarePinTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Hardware Pin test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected HardwarePin getFixture() {
		return (HardwarePin) fixture;
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#getAtpClassifier_atpFeature() <em>Atp Classifier atp Feature</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#getAtpClassifier_atpFeature()
	 * @generated
	 */
	public void testGetAtpClassifier_atpFeature() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#setAtpClassifier_atpFeature(DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier) <em>Atp Classifier atp Feature</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#setAtpClassifier_atpFeature(DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier)
	 * @generated
	 */
	public void testSetAtpClassifier_atpFeature() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#getAtpInstanceRef_atpContextElement() <em>Atp Instance Ref atp Context Element</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpFeature#getAtpInstanceRef_atpContextElement()
	 * @generated
	 */
	public void testGetAtpInstanceRef_atpContextElement() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpFeature() <em>Atp Feature</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpFeature()
	 * @generated
	 */
	public void testGetAtpFeature() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpInstanceRef_atpBase() <em>Atp Instance Ref atp Base</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DomainModel.AUTOSAR.GenericStructure.AbstractStructure.AtpClassifier#getAtpInstanceRef_atpBase()
	 * @generated
	 */
	public void testGetAtpInstanceRef_atpBase() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //HardwarePinTest
