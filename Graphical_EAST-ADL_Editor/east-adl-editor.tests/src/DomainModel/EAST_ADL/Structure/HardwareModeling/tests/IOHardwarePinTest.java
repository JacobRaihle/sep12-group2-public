/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.tests;

import DomainModel.EAST_ADL.Structure.HardwareModeling.IOHardwarePin;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IO Hardware Pin</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class IOHardwarePinTest extends HardwarePinTest {

	/**
	 * Constructs a new IO Hardware Pin test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IOHardwarePinTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this IO Hardware Pin test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IOHardwarePin getFixture() {
		return (IOHardwarePin) fixture;
	}

} //IOHardwarePinTest
