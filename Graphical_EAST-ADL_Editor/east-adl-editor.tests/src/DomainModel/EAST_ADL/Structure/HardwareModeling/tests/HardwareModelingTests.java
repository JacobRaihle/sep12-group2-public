/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>HardwareModeling</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class HardwareModelingTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new HardwareModelingTests("HardwareModeling Tests");
		suite.addTestSuite(ActuatorTest.class);
		suite.addTestSuite(HardwareComponentPrototypeTest.class);
		suite.addTestSuite(HardwareComponentTypeTest.class);
		suite.addTestSuite(HardwareConnectorTest.class);
		suite.addTestSuite(LogicalBusTest.class);
		suite.addTestSuite(NodeTest.class);
		suite.addTestSuite(PowerSupplyTest.class);
		suite.addTestSuite(SensorTest.class);
		suite.addTestSuite(PowerInHardwarePinTest.class);
		suite.addTestSuite(PowerOutHardwarePinTest.class);
		suite.addTestSuite(PowerInOutHardwarePinTest.class);
		suite.addTestSuite(IOInHardwarePinTest.class);
		suite.addTestSuite(IOOutHardwarePinTest.class);
		suite.addTestSuite(IOInOutHardwarePinTest.class);
		suite.addTestSuite(CommunicationInHardwarePinTest.class);
		suite.addTestSuite(CommunicationOutHardwarePinTest.class);
		suite.addTestSuite(CommunicationInOutHardwarePinTest.class);
		suite.addTestSuite(HardwarePinProxyTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HardwareModelingTests(String name) {
		super(name);
	}

} //HardwareModelingTests
