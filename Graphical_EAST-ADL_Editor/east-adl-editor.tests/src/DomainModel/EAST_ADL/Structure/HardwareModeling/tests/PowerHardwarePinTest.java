/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.HardwareModeling.tests;

import DomainModel.EAST_ADL.Structure.HardwareModeling.PowerHardwarePin;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Power Hardware Pin</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class PowerHardwarePinTest extends HardwarePinTest {

	/**
	 * Constructs a new Power Hardware Pin test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerHardwarePinTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Power Hardware Pin test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PowerHardwarePin getFixture() {
		return (PowerHardwarePin) fixture;
	}

} //PowerHardwarePinTest
