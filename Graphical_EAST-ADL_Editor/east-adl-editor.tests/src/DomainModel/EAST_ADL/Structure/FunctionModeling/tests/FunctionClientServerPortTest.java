/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.tests;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionClientServerPort;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Function Client Server Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FunctionClientServerPortTest extends FunctionPortTest {

	/**
	 * Constructs a new Function Client Server Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionClientServerPortTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Function Client Server Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FunctionClientServerPort getFixture() {
		return (FunctionClientServerPort) fixture;
	}

} //FunctionClientServerPortTest
