/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>FunctionModeling</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionModelingTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new FunctionModelingTests("FunctionModeling Tests");
		suite.addTestSuite(AnalysisFunctionPrototypeTest.class);
		suite.addTestSuite(AnalysisFunctionTypeTest.class);
		suite.addTestSuite(BasicSoftwareFunctionTypeTest.class);
		suite.addTestSuite(DesignFunctionPrototypeTest.class);
		suite.addTestSuite(DesignFunctionTypeTest.class);
		suite.addTestSuite(FunctionalDeviceTest.class);
		suite.addTestSuite(FunctionClientServerInterfaceTest.class);
		suite.addTestSuite(FunctionConnectorTest.class);
		suite.addTestSuite(FunctionPowerPortTest.class);
		suite.addTestSuite(HardwareFunctionTypeTest.class);
		suite.addTestSuite(LocalDeviceManagerTest.class);
		suite.addTestSuite(FunctionInFlowPortTest.class);
		suite.addTestSuite(FunctionOutFlowPortTest.class);
		suite.addTestSuite(FunctionInOutFlowPortTest.class);
		suite.addTestSuite(FunctionClientPortTest.class);
		suite.addTestSuite(FunctionServerPortTest.class);
		suite.addTestSuite(FunctionPortProxyTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionModelingTests(String name) {
		super(name);
	}

} //FunctionModelingTests
