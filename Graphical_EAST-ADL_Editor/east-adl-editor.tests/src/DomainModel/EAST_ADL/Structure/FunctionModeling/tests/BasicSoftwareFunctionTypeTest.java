/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.tests;

import DomainModel.EAST_ADL.Structure.FunctionModeling.BasicSoftwareFunctionType;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Basic Software Function Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BasicSoftwareFunctionTypeTest extends DesignFunctionTypeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BasicSoftwareFunctionTypeTest.class);
	}

	/**
	 * Constructs a new Basic Software Function Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicSoftwareFunctionTypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Basic Software Function Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BasicSoftwareFunctionType getFixture() {
		return (BasicSoftwareFunctionType) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FunctionModelingFactory.eINSTANCE
				.createBasicSoftwareFunctionType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BasicSoftwareFunctionTypeTest
