/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAElementTest;

import DomainModel.EAST_ADL.Structure.FunctionModeling.AllocateableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Allocateable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AllocateableElementTest extends EAElementTest {

	/**
	 * Constructs a new Allocateable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocateableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Allocateable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AllocateableElement getFixture() {
		return (AllocateableElement) fixture;
	}

} //AllocateableElementTest
