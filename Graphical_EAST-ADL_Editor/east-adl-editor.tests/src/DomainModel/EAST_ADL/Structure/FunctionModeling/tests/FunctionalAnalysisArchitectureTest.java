/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling.tests;

import DomainModel.EAST_ADL.Infrastructure.Elements.tests.EAPackageTest;

import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionModelingFactory;
import DomainModel.EAST_ADL.Structure.FunctionModeling.FunctionalAnalysisArchitecture;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Functional Analysis Architecture</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionalAnalysisArchitectureTest extends EAPackageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FunctionalAnalysisArchitectureTest.class);
	}

	/**
	 * Constructs a new Functional Analysis Architecture test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalAnalysisArchitectureTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Functional Analysis Architecture test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FunctionalAnalysisArchitecture getFixture() {
		return (FunctionalAnalysisArchitecture) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FunctionModelingFactory.eINSTANCE
				.createFunctionalAnalysisArchitecture());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FunctionalAnalysisArchitectureTest
