/**
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 * 	Continental AG - Initial API and implementation
 */
package DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.tests;

import DomainModel.AUTOSAR.GenericStructure.AbstractStructure.tests.AtpInstanceRefTest;

import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef.FunctionAllocation_allocatedElement;
import DomainModel.EAST_ADL.Structure.FunctionModeling._instanceRef._instanceRefFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Function Allocation allocated Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionAllocation_allocatedElementTest extends AtpInstanceRefTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FunctionAllocation_allocatedElementTest.class);
	}

	/**
	 * Constructs a new Function Allocation allocated Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionAllocation_allocatedElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Function Allocation allocated Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FunctionAllocation_allocatedElement getFixture() {
		return (FunctionAllocation_allocatedElement) fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(_instanceRefFactory.eINSTANCE
				.createFunctionAllocation_allocatedElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FunctionAllocation_allocatedElementTest
